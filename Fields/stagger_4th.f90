! $Id$
! vim: nowrap
!----------------------------------------------------------------------
MODULE pic_stagger
  implicit none

  real    :: aa, bb                                                    ! stagger operator prefactors
  real    :: ax3, ay3, az3                                             ! do.
  real    :: bx3, by3, bz3                                             ! do.
  real    :: cx3, cy3, cz3                                             ! do.
  real    :: lp0, lpm1, lpm2, lpp1, lpp2                               ! LaPlace prefactors

  integer                  :: order=3                                  ! first order for now - default

CONTAINS
!----------------------------------------------------------------------
FUNCTION ddxup (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxup
  integer                                            :: i, j, k

     ddxup = (  ax3*(f(i+1 ,j,k)-f(i   ,j,k)) &
              + bx3*(f(i+2 ,j,k)-f(i-1 ,j,k))  )

END FUNCTION ddxup
!-----------------------------------------------------------------------
FUNCTION ddxdn(f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxdn
  integer                                            :: mx, my, mz
  integer                                            :: i, j, k

     ddxdn = (  ax3*(f(i   ,j,k)-f(i-1 ,j,k)) &
              + bx3*(f(i+1 ,j,k)-f(i-2 ,j,k))  )

END FUNCTION ddxdn
!-----------------------------------------------------------------------
FUNCTION ddyup (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddyup
  integer                                            :: i, j, k

     ddyup = (  ay3*(f(i,j+1 ,k)-f(i,j   ,k)) &
              + by3*(f(i,j+2 ,k)-f(i,j-1 ,k))  )

END FUNCTION ddyup
!-----------------------------------------------------------------------
FUNCTION ddydn (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddydn
  integer                                            :: i, j, k

     ddydn = (  ay3*(f(i,j   ,k)-f(i,j-1 ,k)) &
              + by3*(f(i,j+1 ,k)-f(i,j-2 ,k))  )

END FUNCTION ddydn
!-----------------------------------------------------------------------
FUNCTION ddzup (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzup
  integer                                            :: i, j, k

    ddzup  = (  az3*(f(i,j,k+1 )-f(i,j,k   )) &
              + bz3*(f(i,j,k+2 )-f(i,j,k-1 ))  )

END FUNCTION ddzup
!-----------------------------------------------------------------------
FUNCTION ddzdn (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzdn
  integer                                            :: i, j, k

    ddzdn  = (  az3*(f(i,j,k   )-f(i,j,k-1 )) &
              + bz3*(f(i,j,k+1 )-f(i,j,k-2 ))  )

END FUNCTION ddzdn
!-----------------------------------------------------------------------
FUNCTION ddxdna(f) result(h)
  USE grid_m, only : g
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
  do k=g%lb(3),g%ub(3)-1; do j=g%lb(2),g%ub(2)-1; do i=g%lb(1),g%ub(1)-1
    h(i,j,k) = ddxdn(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddxdna
!-----------------------------------------------------------------------
FUNCTION ddxupa(f) result(h)
  USE grid_m, only : g
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
  do k=g%lb(3),g%ub(3)-1; do j=g%lb(2),g%ub(2)-1; do i=g%lb(1),g%ub(1)-1
    h(i,j,k) = ddxup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddxupa
!-----------------------------------------------------------------------
FUNCTION ddydna(f) result(h)
  USE grid_m, only : g
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
  do k=g%lb(3),g%ub(3)-1; do j=g%lb(2),g%ub(2)-1; do i=g%lb(1),g%ub(1)-1
    h(i,j,k) = ddydn(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddydna
!-----------------------------------------------------------------------
FUNCTION ddyupa(f) result(h)
  USE grid_m, only : g
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
  do k=g%lb(3),g%ub(3)-1; do j=g%lb(2),g%ub(2)-1; do i=g%lb(1),g%ub(1)-1
    h(i,j,k) = ddyup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddyupa
!-----------------------------------------------------------------------
FUNCTION ddzdna(f) result(h)
  USE grid_m, only : g
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
  do k=g%lb(3),g%ub(3)-1; do j=g%lb(2),g%ub(2)-1; do i=g%lb(1),g%ub(1)-1
    h(i,j,k) = ddzdn(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddzdna
!-----------------------------------------------------------------------
FUNCTION ddzupa(f) result(h)
  USE grid_m, only : g
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
  do k=g%lb(3),g%ub(3)-1; do j=g%lb(2),g%ub(2)-1; do i=g%lb(1),g%ub(1)-1
    h(i,j,k) = ddzup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddzupa
!-----------------------------------------------------------------------
FUNCTION d2dx2cen(f,i,j,k)
  USE grid_m,           only : g, odxq
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dx2cen
  integer                                  :: i,j,k

    d2dx2cen = (  lpm2 * f(i-2,j,k) &
                + lpm1 * f(i-1,j,k) &
                +  lp0 * f(i  ,j,k) &
                + lpp1 * f(i+1,j,k) &
                + lpp2 * f(i+2,j,k)  ) * odxq

END FUNCTION d2dx2cen
!-----------------------------------------------------------------------
FUNCTION d2dx2cena(f) result(h)
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
  do k=g%lb(3),g%ub(3)-1; do j=g%lb(2),g%ub(2)-1; do i=g%lb(1),g%ub(1)-1
    h(i,j,k) = d2dx2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dx2cena
!------------------------
FUNCTION d2dy2cena(f) result(h)
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
  do k=g%lb(3),g%ub(3)-1; do j=g%lb(2),g%ub(2)-1; do i=g%lb(1),g%ub(1)-1
    h(i,j,k) = d2dy2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dy2cena
!------------------------
FUNCTION d2dz2cena(f) result(h)
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
  do k=g%lb(3),g%ub(3)-1; do j=g%lb(2),g%ub(2)-1; do i=g%lb(1),g%ub(1)-1
    h(i,j,k) = d2dz2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dz2cena
!------------------------
FUNCTION d2dy2cen(f,i,j,k)
  USE grid_m,           only : g, odyq
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dy2cen
  integer                                  :: i,j,k

    d2dy2cen = (  lpm2 * f(i,j-2,k) &
                + lpm1 * f(i,j-1,k) &
                +  lp0 * f(i,j  ,k) &
                + lpp1 * f(i,j+1,k) &
                + lpp2 * f(i,j+2,k)  ) * odyq

END FUNCTION d2dy2cen
!------------------------

FUNCTION d2dz2cen(f,i,j,k)
  USE grid_m,           only : g, odzq
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dz2cen
  integer                                  :: i,j,k

    d2dz2cen = (  lpm2 * f(i,j,k-2) &
                + lpm1 * f(i,j,k-1) &
                +  lp0 * f(i,j,k  ) &
                + lpp1 * f(i,j,k+1) &
                + lpp2 * f(i,j,k+2)  ) * odzq

END FUNCTION d2dz2cen
!------------------------
FUNCTION lpx(f,i,j,k)
  USE grid_m,           only : g
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpx
    lpx = (  lpm2 * f(i-2,j,k) &
           + lpm1 * f(i-1,j,k) &
           + lpp1 * f(i+1,j,k) &
           + lpp2 * f(i+2,j,k)  )
END FUNCTION lpx
!------------------------
FUNCTION lpy(f,i,j,k)
  USE grid_m,           only : g
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpy
    lpy = (  lpm2 * f(i,j-2,k) &
           + lpm1 * f(i,j-1,k) &
           + lpp1 * f(i,j+1,k) &
           + lpp2 * f(i,j+2,k)  )
END FUNCTION lpy
!------------------------
FUNCTION lpz(f,i,j,k)
  USE grid_m,           only : g
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpz
    lpz = (  lpm2 * f(i,j,k-2) &
           + lpm1 * f(i,j,k-1) &
           + lpp1 * f(i,j,k+1) &
           + lpp2 * f(i,j,k+2)  )
END FUNCTION lpz
!------------------------

END MODULE pic_stagger
!-------------------------------------------------------------------------------
  subroutine test_stagger
!
!  Check the correctness of the stagger operators
!
  use params
  use grid_m, only : g, bx, by, bz, ex, ey, ez
  use pic_stagger
  use units, only : c
  implicit none
  integer i,j,k, jx,jy,jz, mx, my, mz
  real epsx, epsy, epsz, eps1, eps2, eps3, fx, fy, fz, dx, dy, dz, pi
  logical ok

  character(len=70):: id='$Id$'
  if (id .ne. ' ') print *,id; id=' '

  mx=g%gn(1); my=g%gn(2); mz=g%gn(3); dx=g%ds(1); dy=g%ds(2); dz=g%ds(3)

  pi = c%pi
  fx = 2.*pi/(mx*dx)
  fy = 2.*pi/(my*dy)
  fz = 2.*pi/(mz*dz)
  ok = .true.

  do k=g%lb(3),g%ub(3)-1
  do j=g%lb(2),g%ub(2)-1
  do i=g%lb(1),g%ub(1)-1
    jx = i-g%lb(1); jy = j-g%lb(2); jz = k-g%lb(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx-0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy-0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz-0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  bx = ddxdna(bx); epsx=maxval(abs(bx-ex)); if (g%gn(1).eq.1) epsx=0.
  by = ddydna(by); epsy=maxval(abs(by-ey)); if (g%gn(2).eq.1) epsy=0.
  bz = ddzdna(bz); epsz=maxval(abs(bz-ez)); if (g%gn(3).eq.1) epsz=0.
  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  print *,' ddn     :', epsx, eps1, epsy, eps2, epsz, eps3, ok

  do k=g%lb(3),g%ub(3)-1
  do j=g%lb(2),g%ub(2)-1
  do i=g%lb(1),g%ub(1)-1
    jx = i-g%lb(1); jy = j-g%lb(2); jz = k-g%lb(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx+0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy+0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz+0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  bx = ddxupa(bx); epsx=maxval(abs(bx-ex)); if (g%gn(1).eq.1) epsx=0.
  by = ddyupa(by); epsy=maxval(abs(by-ey)); if (g%gn(2).eq.1) epsy=0.
  bz = ddzupa(bz); epsz=maxval(abs(bz-ez)); if (g%gn(3).eq.1) epsz=0.
  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  print *,' dup     :', epsx, eps1, epsy, eps2, epsz, eps3, ok

  do k=g%lb(3),g%ub(3)-1
  do j=g%lb(2),g%ub(2)-1
  do i=g%lb(1),g%ub(1)-1
    jx = i-g%lb(1); jy = j-g%lb(2); jz = k-g%lb(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=-sin(2.*pi*real(jx+0.5)/real(mx))*fx**2
    ey(i,j,k)=-sin(2.*pi*real(jy+0.5)/real(my))*fy**2
    ez(i,j,k)=-sin(2.*pi*real(jz+0.5)/real(mz))*fz**2
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  bx = d2dx2cena(bx); epsx=maxval(abs(bx-ex)); if (g%gn(1).eq.1) epsx=0.
  by = d2dy2cena(by); epsy=maxval(abs(by-ey)); if (g%gn(2).eq.1) epsy=0.
  bz = d2dz2cena(bz); epsz=maxval(abs(bz-ez)); if (g%gn(3).eq.1) epsz=0.
  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  if (.not. ok .and. rank < 10) print *,' d2d[xyz]:', epsx, eps1, epsy, eps2, epsz, eps3, ok

  if (ok) then
    print *,'the stagger routines appear to be working correctly'
  else
    call warning('stagger_test','THERE APPEARS TO BE A PROBLEM WITH THE STAGGER ROUTINES')
  end if

  end subroutine
!----------------------------------------------------------------------

SUBROUTINE compute_stagger_prefactors                                  ! compute prefactors for the stagger operators, they depend on order in finite diff.
USE grid_m,           only : g
USE pic_stagger,    only : aa,  bb,            &
                           ax3, ay3, az3,      &
                           bx3, by3, bz3,      &
                           lp0, lpm1, lpm2,    &
                           lpp1, lpp2,         &
                           order
  implicit none

  integer :: i
  real    :: dx, dy, dz

    dx = g%ds(1)
    dy = g%ds(2)
    dz = g%ds(3)
  
    bb = -1./16.
    aa = .5-bb
    bx3=-1./24.          ; by3=-1./24.          ; bz3=-1./24.
    ax3=(1. - 3.*bx3)/dx ; ay3=(1. - 3.*by3)/dy ; az3=(1. - 3.*bz3)/dz
    bx3=bx3/dx           ; by3=by3/dy           ; bz3=bz3/dz

    lp0  = - 30./12.
    lpm1 = + 16./12.
    lpp1 = + 16./12.
    lpm2 = -  1./12.
    lpp2 = -  1./12.

  call test_stagger
  
END SUBROUTINE compute_stagger_prefactors
!----------------------------------------------------------------------


!==========================================================================================================================
!CONVENTIONS:
!
!
!
!
!GRID:	i-3		i-2		i-1		i		i+1		i+2		i+3		i+1
!
!							ddxdn[i,j,k](i-3,i-2,i-1,i,i+1,i+2) / (i-2,i-1,i,i+1) / (i-1,i)
!							|
!		----------------------------------------|----------------------------------------
!		|		|		|		|		|		|
!X	O	X	O	X	O	X	O	X	O	X	O	X	O	X	O
!			|		|		|		|		|		|
!			----------------------------------------|----------------------------------------
!								|
!								ddxup[i,j,k](i-2,i-1,i,i+1,i+2,i+3) / (i-1,i,i+1,i+2) / (i,i+1)
!
!==========================================================================================================================


!From the coefficients: 
!    bz3=-1./24.
!    az3=(1. - 3.*bz3)/dz
!    bz3=bz3/dz
!
! we get for example that 
!    ddzup  = (  az3*(f(i,j,k+1 )-f(i,j,k   )) &
!              + bz3*(f(i,j,k+2 )-f(i,j,k-1 ))  )
!
! can be rewritten as
!    ddzup  = ( -f[+3/2] + 27*f[+1/2] - 27*f[-1/2]  + 1*f[-3/2] )/(24*dz).
!
! which is the correct representation on HALF-CENTERED grids. 
!
!We should, however, use the corresponding CENTERED LaPlace 
! operator to keep things in line, since we're finding d^2/d[Q]^2
! at it's 'own' point.
