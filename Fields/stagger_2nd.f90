! $Id$
! vim: nowrap
!----------------------------------------------------------------------
MODULE pic_stagger
  USE params, only: mpi
  USE grid_m, only : g, odxq, odyq, odzq
  implicit none

  real    :: aa
  real    :: ax3, ay3, az3                                              ! do.
  real    :: bx3, by3, bz3                                              ! do.
  real    :: cx3, cy3, cz3                                              ! do.
  real    :: lp0, lpm1, lpp1

  integer                  :: order=1                                  ! second order for now - default

CONTAINS
!----------------------------------------------------------------------
FUNCTION xup (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: xup
  integer                                            :: i, j, k

     xup = aa*(f(i+1,j,k)+f(i  ,j,k))
END FUNCTION xup
!----------------------------------------------------------------------
FUNCTION yup (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: yup
  integer                                            :: i, j, k

     yup = aa*(f(i,j+1,k)+f(i,j  ,k))
END FUNCTION yup
!----------------------------------------------------------------------
FUNCTION zup (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: zup
  integer                                            :: i, j, k

     zup = aa*(f(i,j,k+1)+f(i,j,k  ))
END FUNCTION zup
!----------------------------------------------------------------------
FUNCTION xdn (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: xdn
  integer                                            :: i, j, k

     xdn = aa*(f(i-1,j,k)+f(i  ,j,k))
END FUNCTION xdn
!----------------------------------------------------------------------
FUNCTION ydn (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ydn
  integer                                            :: i, j, k

     ydn = aa*(f(i,j-1,k)+f(i,j  ,k))
END FUNCTION ydn
!----------------------------------------------------------------------
FUNCTION zdn (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: zdn
  integer                                            :: i, j, k

     zdn = aa*(f(i,j,k-1)+f(i,j,k  ))
END FUNCTION zdn
!----------------------------------------------------------------------
FUNCTION ddxup (f,i,j,k)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxup
  integer                                            :: i, j, k
  ddxup = (  ax3*(f(i+1 ,j,k)-f(i   ,j,k))  )
END FUNCTION ddxup
!-----------------------------------------------------------------------
FUNCTION ddxdn(f,i,j,k)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxdn
  integer                                            :: mx, my, mz
  integer                                            :: i, j, k
  ddxdn = (  ax3*(f(i   ,j,k)-f(i-1 ,j,k))  )
END FUNCTION ddxdn
!-----------------------------------------------------------------------
FUNCTION ddyup (f,i,j,k)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddyup
  integer                                            :: i, j, k
  ddyup = (  ay3*(f(i,j+1 ,k)-f(i,j   ,k))  )
END FUNCTION ddyup
!-----------------------------------------------------------------------
FUNCTION ddydn (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddydn
  integer                                            :: i, j, k
  ddydn = (  ay3*(f(i,j   ,k)-f(i,j-1 ,k))  )
END FUNCTION ddydn
!-----------------------------------------------------------------------
FUNCTION ddzup (f,i,j,k)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzup
  integer                                            :: i, j, k
  ddzup  = (  az3*(f(i,j,k+1 )-f(i,j,k   ))  )
END FUNCTION ddzup
!-----------------------------------------------------------------------
FUNCTION ddzdn (f,i,j,k)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzdn
  integer                                            :: i, j, k
  ddzdn  = (  az3*(f(i,j,k   )-f(i,j,k-1 ))  )
END FUNCTION ddzdn
!-----------------------------------------------------------------------
FUNCTION ddxdna(f) result(h)
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(1) = max(lb(1),2)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddxdn(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddxdna
!-----------------------------------------------------------------------
FUNCTION xupa(f) result(h)
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  ub(1) = min(ub(1),g%n(1)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = xup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION xupa
!-----------------------------------------------------------------------
FUNCTION ddxupa(f) result(h)
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  ub(1) = min(ub(1),g%n(1)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddxup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddxupa
!-----------------------------------------------------------------------
FUNCTION ddydna(f) result(h)
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(2) = max(lb(2),2)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddydn(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddydna
!-----------------------------------------------------------------------
FUNCTION yupa(f) result(h)
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  ub(2) = min(ub(2),g%n(2)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = yup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION yupa
!-----------------------------------------------------------------------
FUNCTION ddyupa(f) result(h)
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  ub(2) = min(ub(2),g%n(2)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddyup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddyupa
!-----------------------------------------------------------------------
FUNCTION ddzdna(f) result(h)
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(3) = max(lb(3),2)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddzdn(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddzdna
!-----------------------------------------------------------------------
FUNCTION zupa(f) result(h)
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  ub(3) = min(ub(3),g%n(3)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = zup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION zupa
!-----------------------------------------------------------------------
FUNCTION ddzupa(f) result(h)
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  ub(3) = min(ub(3),g%n(3)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddzup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddzupa
!-----------------------------------------------------------------------
FUNCTION d2dx2cen(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dx2cen
  integer                                  :: i,j,k
  d2dx2cen = (  lpm1 * f(i-1,j,k) &
              +  lp0 * f(i  ,j,k) &
              + lpp1 * f(i+1,j,k)  ) * odxq
END FUNCTION d2dx2cen
!-----------------------------------------------------------------------
FUNCTION d2dx2cena(f) result(h)
  USE params, only : mpi
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,ub(3)
  ub = merge(g%ub, g%ub-1, mpi%ub)
  do k=g%lb(3),ub(3); do j=g%lb(2),ub(2); do i=g%lb(1),ub(1)
    h(i,j,k) = d2dx2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dx2cena
!------------------------
FUNCTION d2dy2cena(f) result(h)
  USE params, only : mpi
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,ub(3)
  ub = merge(g%ub, g%ub-1, mpi%ub)
  do k=g%lb(3),ub(3); do j=g%lb(2),ub(2); do i=g%lb(1),ub(1)
    h(i,j,k) = d2dy2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dy2cena
!------------------------
FUNCTION d2dz2cena(f) result(h)
  USE params, only : mpi
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,ub(3)
  ub = merge(g%ub, g%ub-1, mpi%ub)
  do k=g%lb(3),ub(3); do j=g%lb(2),ub(2); do i=g%lb(1),ub(1)
    h(i,j,k) = d2dz2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dz2cena
!------------------------
FUNCTION d2dy2cen(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dy2cen
  integer                                  :: i,j,k
  d2dy2cen = (  lpm1 * f(i,j-1,k) &
              +  lp0 * f(i,j  ,k) &
              + lpp1 * f(i,j+1,k)  ) * odyq
END FUNCTION d2dy2cen
!------------------------
FUNCTION d2dz2cen(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dz2cen
  integer                                  :: i,j,k
  d2dz2cen = (  lpm1 * f(i,j,k-1) &
              +  lp0 * f(i,j,k  ) &
              + lpp1 * f(i,j,k+1)  ) * odzq
END FUNCTION d2dz2cen
!------------------------
FUNCTION laplace(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: laplace
  integer                                  :: i,j,k
   laplace  = (  lpm1 * f(i-1,j,k) &
               +  lp0 * f(i  ,j,k) &
               + lpp1 * f(i+1,j,k)  ) * odxq &
            + (  lpm1 * f(i,j,k-1) &
               +  lp0 * f(i,j,k  ) &
               + lpp1 * f(i,j,k+1)  ) * odzq &
            + (  lpm1 * f(i,j-1,k) &
               +  lp0 * f(i,j  ,k) &
               + lpp1 * f(i,j+1,k)  ) * odyq
END FUNCTION laplace
!-----------------------------------------------------------------------
FUNCTION lpx(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpx
    lpx = (  lpm1 * f(i-1,j,k) &
           + lpp1 * f(i+1,j,k)  )
END FUNCTION lpx
!------------------------
FUNCTION lpy(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpy
      lpy = (  lpm1 * f(i,j+1,k) &
             + lpp1 * f(i,j-1,k)  )
END FUNCTION lpy
!------------------------
FUNCTION lpz(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpz
      lpz = (  lpm1 * f(i,j,k-1) &
             + lpp1 * f(i,j,k+1)  )
END FUNCTION lpz

!-----------------------------------------------------------------------
FUNCTION curlxdn(e,f) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = ay3*(e(i,j  ,k)-e(i,j-1,k)) &
             - az3*(f(i,j,k  )-f(i,j,k-1))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlxdn
!-----------------------------------------------------------------------
FUNCTION curlydn(e,f) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = az3*(e(i,j,k  )-e(i,j,k-1)) &
             - ax3*(f(i  ,j,k)-f(i-1,j,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlydn
!-----------------------------------------------------------------------
FUNCTION curlzdn(e,f) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = ax3*(e(i  ,j,k)-e(i-1,j,k)) &
             - ay3*(f(i,j,  k)-f(i,j-1,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlzdn
!-----------------------------------------------------------------------
FUNCTION curlxup(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = ay3*(e(i,j+1,k)-e(i,j  ,k)) &
             - az3*(f(i,j,k+1)-f(i,j,k  ))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlxup
!-----------------------------------------------------------------------
FUNCTION curlyup(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = az3*(e(i,j,k+1)-e(i,j,k  )) &
             - ax3*(f(i+1,j,k)-f(i  ,j,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlyup
!-----------------------------------------------------------------------
FUNCTION curlzup(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = ax3*(e(i+1,j,k)-e(i  ,j,k)) &
             - ay3*(f(i,j+1,k)-f(i,j  ,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlzup

END MODULE pic_stagger
!-------------------------------------------------------------------------------
  subroutine test_stagger
!
!  Check the correctness of the stagger operators
!
  use params
  use grid_m, only : g, bx, by, bz, ex, ey, ez
  use pic_stagger
  use units, only : c
  implicit none
  integer i,j,k, jx,jy,jz, mx, my, mz, ii(3)
  real epsx, epsy, epsz, eps1, eps2, eps3, fx, fy, fz, dx, dy, dz, pi
  logical ok, all_mpi
  real, allocatable, dimension(:,:,:):: dif

  character(len=mid):: id='$Id$'
  call print_id (id)

  mx=g%gn(1); my=g%gn(2); mz=g%gn(3); dx=g%ds(1); dy=g%ds(2); dz=g%ds(3)

  pi = c%pi
  fx = 2.*pi/(mx*dx)
  fy = 2.*pi/(my*dy)
  fz = 2.*pi/(mz*dz)
  ok = .true.

  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i+mpi%offset(1); jy = j+mpi%offset(2); jz = k+mpi%offset(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx-0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy-0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz-0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)

  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  allocate (dif(g%n(1),g%n(2),g%n(3)))

  bx = ddxdna(bx); dif=abs(bx-ex)
  epsx=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(1).eq.1) epsx=0.
  call overlap(dif)
  !do while (epsx > eps1)
  !  ii=maxloc(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))+g%lb-1
  !  print*,ii,bx(ii(1),ii(2),ii(3)),ex(ii(1),ii(2),ii(3))
  !  dif(ii(1),ii(2),ii(3))=0.
  !  epsx=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  !end do

  by = ddydna(by); dif=abs(by-ey)
  epsy=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(2).eq.1) epsy=0.
  call overlap(dif)
  !do while (epsy > eps2)
  !  ii=maxloc(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))+g%lb-1
  !  print*,ii,by(ii(1),ii(2),ii(3)),ey(ii(1),ii(2),ii(3))
  !  dif(ii(1),ii(2),ii(3))=0.
  !  epsy=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  !end do

  bz = ddzdna(bz); dif=abs(bz-ez)
  epsz=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(3).eq.1) epsz=0.
  call overlap(dif)
  !do while (epsz > eps3)
  !  ii=maxloc(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))+g%lb-1
  !  print*,ii,bz(ii(1),ii(2),ii(3)),ez(ii(1),ii(2),ii(3))
  !  dif(ii(1),ii(2),ii(3))=0.
  !  epsz=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  !end do

  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  if (.not. ok) print'(a,i5,1p,6g12.4,l4)',' ddn     :', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i+mpi%offset(1); jy = j+mpi%offset(2); jz = k+mpi%offset(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx+0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy+0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz+0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)

  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)

  bx = ddxupa(bx); dif=abs(bx-ex)
  epsx=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(1).eq.1) epsx=0.
  call overlap(dif)
  !do while (epsx > eps1)
  !  ii=maxloc(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))+g%lb-1
  !  print*,'ddxup',i,bx(ii(1),ii(2),ii(3)),ex(ii(1),ii(2),ii(3))
  !  dif(ii(1),ii(2),ii(3))=0.
  !  epsx=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  !end do

  by = ddyupa(by); dif=abs(by-ey)
  epsy=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(2).eq.1) epsy=0.
  call overlap(dif)
  !do while (epsy > eps2)
  !  ii=maxloc(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))+g%lb-1
  !  print*,'ddyup',i,bz(ii(1),ii(2),ii(3)),ez(ii(1),ii(2),ii(3))
  !  dif(ii(1),ii(2),ii(3))=0.
  !  epsy=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  !end do

  bz = ddzupa(bz); dif=abs(bz-ez)
  epsz=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(3).eq.1) epsz=0.
  call overlap(dif)
  !do while (epsz > eps3)
  !  ii=maxloc(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))+g%lb-1
  !  print*,'ddzup',ii,bz(ii(1),ii(2),ii(3)),ez(ii(1),ii(2),ii(3))
  !  dif(ii(1),ii(2),ii(3))=0.
  !  epsz=maxval(dif(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  !end do

  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  if (.not. ok) print'(a,i5,1p,6g12.4,l4)',' dup     :', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1); jy = j-g%lb(2)+mpi%offset(2); jz = k-g%lb(3)+mpi%offset(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=-sin(2.*pi*real(jx)/real(mx))*fx**2
    ey(i,j,k)=-sin(2.*pi*real(jy)/real(my))*fy**2
    ez(i,j,k)=-sin(2.*pi*real(jz)/real(mz))*fz**2
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  bx = d2dx2cena(bx); bx=abs(bx-ex)
  epsx=maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(1).eq.1) epsx=0.
  by = d2dy2cena(by); by=abs(by-ey)
  epsy=maxval(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(2).eq.1) epsy=0.
  bz = d2dz2cena(bz); bz=abs(bz-ez)
  epsz=maxval(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(3).eq.1) epsz=0.
  eps1 = 2.*max(fx**4*dx**2/(3.*2.*1.),2.e-6*fx/dx)
  eps2 = 2.*max(fy**4*dy**2/(3.*2.*1.),2.e-6*fx/dy)
  eps3 = 2.*max(fz**4*dz**2/(3.*2.*1.),2.e-6*fx/dz)
  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  if (.not. ok .and. rank < 10) print'(a,i5,1p,6g12.4,l4)',' d2d[xyz]:', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  ok = all_mpi(ok)
  if (ok) then
    if (master) print *,'the stagger routines appear to be working correctly'
  else
    call warning('stagger_test','THERE APPEARS TO BE A PROBLEM WITH THE STAGGER ROUTINES')
  end if

  deallocate(dif)
  end subroutine
!----------------------------------------------------------------------

SUBROUTINE compute_stagger_prefactors                                  ! compute prefactors for the stagger operators, they depend on order in finite diff.
USE grid_m,           only : g
USE pic_stagger,    only : aa,                    &
                           ax3,  ay3,  az3,       &
                           lp0, lpm1, lpp1
  implicit none
  integer :: i
  real    :: dx, dy, dz

  dx = g%ds(1); dy = g%ds(2); dz = g%ds(3)
  aa =.5; ax3=1./dx; ay3=1./dy; az3=1./dz    !stagger

  lp0  = - 2.                                !LaPlace
  lpm1 = + 1.
  lpp1 = + 1.

  call test_stagger
  
END SUBROUTINE compute_stagger_prefactors
!----------------------------------------------------------------------


!==========================================================================================================================
!CONVENTIONS:
!
!
!
!
!GRID:	i-3		i-2		i-1		i		i+1		i+2		i+3		i+1
!
!							ddxdn[i,j,k](i-3,i-2,i-1,i,i+1,i+2) / (i-2,i-1,i,i+1) / (i-1,i)
!							|
!		----------------------------------------|----------------------------------------
!		|		|		|		|		|		|
!X	O	X	O	X	O	X	O	X	O	X	O	X	O	X	O
!			|		|		|		|		|		|
!			----------------------------------------|----------------------------------------
!								|
!								ddxup[i,j,k](i-2,i-1,i,i+1,i+2,i+3) / (i-1,i,i+1,i+2) / (i,i+1)
!
!==========================================================================================================================


!From the coefficients: 
!    bz3=-1./24.
!    az3=(1. - 3.*bz3)/dz
!    bz3=bz3/dz
!
! we get for example that 
!    ddzup  = (  az3*(f(i,j,k+1 )-f(i,j,k   )) &
!              + bz3*(f(i,j,k+2 )-f(i,j,k-1 ))  )
!
! can be rewritten as
!    ddzup  = ( -f[+3/2] + 27*f[+1/2] - 27*f[-1/2]  + 1*f[-3/2] )/(24*dz).
!
! which is the correct representation on HALF-CENTERED grids. 
!
!We should, however, use the corresponding CENTERED LaPlace 
! operator to keep things in line, since we're finding d^2/d[Q]^2
! at it's 'own' point.
