! Io/dump.f90 $Id$
! vim: nowrap
!=======================================================================
MODULE dumps
  USE params, only: dump_unit, trace, stdout, stdall, rank, &
    do_dump, do_compare,time, nodes, master, nodes, datadir, mid, mpi
  USE debug, only: verbose, maxprint, tolerance, relative_tolerance
  implicit none
!.......................................................................
  !private
    integer, parameter:: llab=16, lfile=80
    logical:: file_is_open=.false., subdomain=.false., allok=.true.
    character(len=lfile) :: thread_file, file
    character(len=lfile) :: active_file, prefix=''
    integer dim(3), gdim(3), offset
  public:: dump,dump_set,allok
    INTERFACE dump
      MODULE PROCEDURE dump1d,dump2d,dump3d
    END INTERFACE
CONTAINS

!=======================================================================
! Set a non-default pre-fix for dump file names
!=======================================================================
SUBROUTINE dump_prefix (name)
  implicit none
  character(len=*) name
  prefix = trim(name)//'-'
END SUBROUTINE dump_prefix

!=======================================================================
! If a dump file is open, leave it open.  If a different file is open,
! close it and open the requested file
!-----------------------------------------------------------------------
SUBROUTINE dump_set (file_in)
  implicit none
  character(len=*) file_in
  logical file_exists
  character(len=5) rr
  integer idot,isp
  logical debug
  character(len=mid),save :: &
    id = 'Io/dump.f90 $Id$'
!.......................................................................
  call print_id (id)
  debug = verbose > 0 .and. stdout > 0
  !---------------------------------------------------------------------
  ! make sure the file name has a .dmp extension
  file = file_in
  if (index(file,'.') < 1) file=trim(file)//'.dmp'
  !---------------------------------------------------------------------
  ! if not doing dump or compare, just remember the name
  if (.not. (do_dump .or. do_compare)) then
    active_file = file
    return
  end if
  !---------------------------------------------------------------------
  ! if another file is open, close it and 
  if (file_is_open) then
    if (trim(file)==trim(active_file)) then
      if(debug) write(stdout,*) 'dump_set: keeping dump files ' &
        //trim(active_file)
      return
    else
      if(debug) write(stdout,*) 'dump_set: closing file ' &
        //trim(active_file)
      close(dump_unit)
      file_is_open=.false.
    end if
  end if
  !---------------------------------------------------------------------
  ! open thread-specific file
  write(rr,'(1h-,i4.4)') rank
  idot=index(file,'.')
  isp=len(file)
  thread_file=trim(datadir)//'/'//trim(prefix)// &
              file(1:idot-1)//rr//file(idot:isp)
  if (do_compare) then
    if (file_exists(thread_file)) then
      if(debug) write(stdout,*) &
        'dump_set: opening existing file ', trim(thread_file)
      open(dump_unit,file=thread_file,form='unformatted')
    else
      if (master) write(stdout,*) &
        'dump_set: comparison file does not exist:', trim(thread_file)
    end if
  else if (do_dump) then
    if (file_exists(file)) then
      open(dump_unit,file=thread_file,form='unformatted',status='unknown')
      if(debug) write(stdout,*) 'dump_set: writing to ', trim(thread_file)
    end if
  end if
  !---------------------------------------------------------------------
  ! remember open file
  subdomain = (nodes > 1)
  file_is_open = .true.
  active_file = file
END SUBROUTINE

!=======================================================================
! Search for a record that agrees with respect to dimensions, time, and
! dump label.
!-----------------------------------------------------------------------
LOGICAL FUNCTION seek_dump (label, dim)
  USE grid_m, only: g
  implicit none
  character(len=llab) label,label1
  integer nodes1, rank1, dim(3), llab1
  real time1,void
  logical debug
!.......................................................................
  debug = verbose > 1 .and. stdout > 0
  if (debug) write(stdout,*) 'seek_dump: searching for ',trim(label)
  debug = verbose > 2 .and. stdout > 0
  do while(.true.)
    read(dump_unit,err=9,end=9) nodes1,rank1,time1,llab1
    read(dump_unit) label1
    read(dump_unit) dim, gdim, offset
    if (label1.eq.label) then
      if (time1==real(time)) then
        if (maxval(g%gn-gdim)==0) then
          if(debug) write(stdout,*) 'found it'
          seek_dump=.true.
          return
        else
          if(debug) write(stdout,*) 'not the same dimensions'
        end if
      else
        if(debug) write(stdout,*) 'not the same time'
      end if
    else
      if(debug) write(stdout,*) 'not the same label'
    end if
    read(dump_unit) void
  end do
9 rewind(dump_unit)
  seek_dump=.false.
  return
END FUNCTION

!=======================================================================
SUBROUTINE dump1d (f, label)
  USE grid_m, only: g
  implicit none
  real f(:)
  character(len=llab) label
  real, allocatable, dimension(:) :: f2
  logical file_exists, flag
!.......................................................................
  flag = file_exists(active_file)
  if (verbose > 1) call stat (g%n, f, label, active_file, flag)
  dim(2:3) = 0; dim(1:1) = shape(f)
  if (do_compare) then
    if(seek_dump(label,dim)) then
      allocate(f2(dim(1)))
      read(dump_unit) f2
      call compare1d (dim, f, f2, label)
      deallocate (f2)
    end if
  else if (do_dump) then
    if (flag) then
      write(dump_unit) nodes, rank, real(time), llab
      write(dump_unit) label
      write(dump_unit) dim, g%gn, g%lb, g%ub, mpi%offset
      write(dump_unit) f
    end if
  end if
END SUBROUTINE
!=======================================================================
SUBROUTINE dump2d (f, label)
  USE grid_m, only: g
  implicit none
  real f(:,:)
  character(len=llab) label
  real, allocatable, dimension(:,:) :: f2
  logical file_exists, flag
!-----------------------------------------------------------------------
  flag = file_exists(active_file)
  if (verbose > 1) call stat (g%n, f, label, active_file, flag)
  dim(3) = 0; dim(1:2) = shape(f)
  if (do_compare) then
    if(seek_dump(label,dim)) then
      allocate (f2(dim(1), dim(2)))
      read(dump_unit) f2
      call compare2d (dim, f, f2, label)
      deallocate (f2)
    end if
  else if (do_dump) then
    if (flag) then
      write(dump_unit) nodes, rank, real(time), llab
      write(dump_unit) label
      write(dump_unit) dim, g%gn, g%lb, g%ub, mpi%offset
      write(dump_unit) f
    end if
  end if
END SUBROUTINE

!=======================================================================
SUBROUTINE dump3d (f, lab)
  USE grid_m, only: g
  implicit none
  real f(g%n(1),g%n(2),g%n(3))
  character(len=*) lab
  character(len=llab) label
  real, allocatable, dimension(:,:,:) :: f2, scr
  logical file_exists, is_nan, flag
  integer i,j,k,nan,lb(3)
!.......................................................................
  label = lab
  flag = file_exists(active_file)
  if (verbose > 1) call stat (g%n, f, label, active_file, flag)
  if (do_compare) then
    dim=shape(f)
    if(seek_dump(label,dim)) then
      allocate(f2(g%n(1),g%n(2),g%n(3)))
      if (subdomain) then
        allocate (scr(dim(1),dim(2),dim(3)))
        offset = 1 + g%offset + g%lb(3)
        if (verbose>2) write(stdall,'(4(i4,a))') &
          rank,' reading',dim(3),' using',g%n(3),' offset',offset
        read(dump_unit) scr
        f2 = scr(:,:,offset:offset+g%n(3)-1)
        deallocate (scr)
      else
        read(dump_unit) f2
      end if
      call compare3d(g%n,f,f2,label)
      deallocate(f2)
    end if
  else if (do_dump) then
    dim = shape(f)
    nan = 0
    do k=1,dim(3); do j=1,dim(2); do i=1,dim(1)
      if (is_nan(f(i,j,k))) then
        nan=nan+1
        if (nan < 9) print'(1x,a,4i5,2x,a)', &
          trim(label),rank,i,j,k,'NaN'
      end if
    end do; end do; end do
    call mpi_sum_integer (nan)
    if (nan > 0) then
      if (master) print*,'NaNs found in dump:', nan
       call finalize_mpi (.true.)
    end if
    lb = merge(g%lb+1, g%lb, mpi%lb)
    if (flag) then
      write(dump_unit) nodes, rank, real(time), llab
      write(dump_unit) label
      write(dump_unit) g%ub-lb, g%gn, g%lb, g%ub, mpi%offset
      write(dump_unit) f(lb(1):g%ub(1)-1,lb(2):g%ub(2)-1,lb(3):g%ub(3)-1)
      if (master .and. (verbose == 1)) then
        write(stdall,'(1x,a,1p,g14.6)') &
          'dump: '//trim(active_file)//':'//trim(label), real(time)
      end if
    end if
  end if
END SUBROUTINE

!=======================================================================
SUBROUTINE compare1d(dim,f,f2,label)
  implicit none
  integer dim(1)
  real, dimension(dim(1)):: f,f2
  character(len=llab) label
  integer i,nok
!.......................................................................
  nok=0
  do i=1,dim(1)
    if (f2(i).ne.f(i)) then
      nok=nok+1
      if (nok.le.maxprint) then
        write(stdall,*) label,rank,i,f(i),f2(i)
      end if
    end if
  end do
  if (nok==0) then
    if (master) write(stdall,*) label,' OK'
  else
    write(stdall,*) label,nok/real(dim(1)),' NOK'
    allok = .false.
  end if
END SUBROUTINE

!=======================================================================
SUBROUTINE compare2d(dim,f,f2,label)
  implicit none
  integer dim(2)
  real, dimension(dim(1),dim(2)):: f,f2
  character(len=llab) label
  integer i,j,nok
!.......................................................................
  nok=0
  do j=1,dim(2)
  do i=1,dim(1)
    if (f2(i,j).ne.f(i,j)) then
      nok=nok+1
      if (nok.le.maxprint) then
        write(stdall,*) label,rank,i,j,f(i,j),f2(i,j)
      end if
    end if
  end do
  end do
  if (nok==0) then
    if (master) write(stdall,*) label,' OK'
  else
    write(stdall,*) label,nok/real(product(dim)),' NOK'
    allok = .false.
  end if
END SUBROUTINE
!=======================================================================
SUBROUTINE compare3d(dim,f,f2,label)
  !USE params, only: rank, nodes
  USE grid_m, only: g
  implicit none
  integer dim(3)
  real, dimension(dim(1),dim(2),dim(3)):: f,f2
  character(len=llab) label
  integer i,j,k,nok,isp,node
  logical is_nan
!.......................................................................
  nok=0
  isp=len(label)
  do while(isp > 1) 
    if (label(isp:isp).ne.' ') exit
    isp=isp-1
  end do
  call barrier
  do node=0,nodes-1
    if (node==rank) then
      do k=1,g%n(3)
      do j=1,g%n(2)
      do i=1,g%n(1)
        if (abs(f2(i,j,k)-f(i,j,k)) > tolerance) then
          nok=nok+1
          if (nok.le.maxprint) then
            write(stdall,'(a,4i6,2g15.7,3x,a)') &                       ! report a difference
              ' tolerance: '//label(1:isp),rank,i,j,k, &
              f(i,j,k),f2(i,j,k),trim(active_file)
            call flush (stdall)
          end if
        else if (abs(f2(i,j,k)-f(i,j,k)) > relative_tolerance*f2(i,j,k)) then
          nok=nok+1
          if (nok.le.maxprint) then
            write(stdall,'(a,4i6,2g15.7,3x,a)') &                       ! report a difference
              ' relative_tolerance: '//label(1:isp),rank,i,j,k, &
              f(i,j,k),f2(i,j,k),trim(active_file)
            call flush (stdall)
          end if
        else if (is_nan(f(i,j,k))) then
          nok=nok+1
          if (nok.le.maxprint) then
            write(stdall,'(a,4i6,2g15.7,3x,a)') &                       ! report NaN
              ' is_nan: '//label(1:isp),rank,i,j,k, &
              f(i,j,k),f2(i,j,k),trim(active_file)
            call flush (stdall)
          end if
        end if
      end do
      end do
      end do
      if (nok==0) then
        if (verbose > 1) write(stdall,'(1x,a,2x,a,g13.4,a,i4)') &
          trim(active_file),trim(label),real(time),' OK'
      else
        write(stdall,*) label,nok/real(product(dim)),' NOK'
        allok = .false.
        call sleep(1)
      end if
      call flush (stdall)
    end if
    call barrier
  end do
END SUBROUTINE

!=======================================================================
! Compute and print the average, min, max, and rms of the array f
!-----------------------------------------------------------------------
SUBROUTINE stat (dim, f, label, file, flag)
  USE grid_m, only: g
  implicit none
  integer:: dim(3)
  real:: f(dim(1),dim(2),dim(3))
  character(len=llab):: label
  character(len=*):: file
  logical:: flag
  real(kind=8):: sum(3)
  real:: aver, rms, fmin, fmax
  integer:: ix, iy, iz, i, loc(3)
!.......................................................................
  sum = 0
  fmin = 1e30
  fmax = -fmin
  do iz=g%lb(3),g%ub(3)-1
  do iy=g%lb(2),g%ub(2)-1
  do ix=g%lb(1),g%ub(1)-1
    sum(1) = sum(1) + f(ix,iy,iz)
    sum(2) = sum(2) + f(ix,iy,iz)**2
    sum(3) = sum(3) + 1.
    fmin = merge(f(ix,iy,iz), fmin, f(ix,iy,iz) < fmin)
    fmax = merge(f(ix,iy,iz), fmax, f(ix,iy,iz) > fmax)
  end do; end do; end do
  call mpi_sum_real8s(3,sum)
  aver = sum(1)/sum(3)
  rms  = sqrt(sum(2)/sum(3) - aver**2)
  call mpi_max_real (fmax)
  call mpi_min_real (fmin)
  i = index(active_file,'.')-1
  if (master) print'(1x,a,a5,a,1p,4g11.3,2x,a)', &
    trim(prefix)//active_file(1:i),trim(label), &
    ' aver, rms, min, max =', &
    aver, rms, fmin, fmax, merge('w', ' ',flag)
  if (nodes==1 .and. verbose>2) then
    loc = minloc(f) 
    print*,'min =', f(loc(1),loc(2),loc(3)), ' at', loc
    loc = maxloc(f) 
    print*,'max =', f(loc(1),loc(2),loc(3)), ' at', loc
  end if
END SUBROUTINE
!-----------------------------------------------------------------------
END MODULE

!=======================================================================
SUBROUTINE finalize_compare
  USE params
  USE dumps
  implicit none
!.......................................................................
  if (do_compare) then
    if (allok) then 
      write(stdall,'(1x,a,i4)') 'compare: OK from rank',rank 
    else
      write(stdall,'(1x,a,i4)') 'compare: NOK from rank',rank 
    end if
  end if
END
