! species.f90 $Id$
! vim: nowrap
!=======================================================================
MODULE species
!
! Unfortunately it is not possible to define truly dynamic types,
! where the dimension of an element is dynamically defined.  One is
! thus forced to for example allocate space for all particles separately,
! and then let the species type contain pointers into the global array.
! This is perhaps also the only practical way to allow different 
! instances of a type to have different size elements.
!
! In the case of the cellvectors one might instead choose to say "there
! is really a maximum number of species (for example electrons+positrons
! +neutrons+protons+heavy_ions+photons = 6 = mspecies), and only let the 
! actual number of species be a variable (nspecies).  One then achieves
! being able to have all the relevant data for a cell in one cacheline.
!
! However, the typical operation for an interaction is to run through
! just one or two species in a cell, so to have the particles in separate
! cache lines is not bad.  What is most important is to keep all the data
! associated with a particle, including momentum increment, in one cache
! line.  Since we only need the dv as a working array we only need these
! larger particle types while dealing with each cell.  It is doubtful,
! however, if the extra speed gain from having dv along with the particle
! is larger than the cost of copying the rest of the particle data.  So,
! at least to keep things simple to begin with, we may choose to allocate
! dp separately.
!
  USE params
  implicit none
!.......................................................................
! Basic particle type. The length of a particle measured in integers 
! is needed for MPI, if we ever adjust the content of a particle, then
! it is ! important to readjust the particle length too.
!
! Remember to change the derived type for sending particles in init_mpi too.
#if PREC==4
  integer, parameter :: particle_length = mdim + mcoord + 2 + 4         ! single precision
#else
#if PREC==8
  integer, parameter :: particle_length = (mdim + mcoord + 2)*2 + 4     ! double precision
#else
  integer, parameter :: particle_length = (mdim + mcoord + 2)*4 + 4     ! quad precision
#endif
#endif
  type :: particle                                                      ! particle data type
#ifdef CUDA
    SEQUENCE
#endif
    real r(mdim)                                                        ! positions inside cell, normalised to [0,1)
    real e                                                              ! energy
    real p(mcoord)                                                      ! momenta
    real w                                                              ! weight
    integer(kind=2) q(mdim)                                             ! cell number; 3D: up to 65k per direction; 2D: up to 16.7M
    integer(kind=2) bits                                                ! bits, that can be used for signaling stuff
    integer*8 i                                                         ! sort index
  end type
!........................................................................
  type :: particle2                                                     ! particle data type
    real r(mdim)                                                        ! positions
    real p(mcoord)                                                      ! momenta
    real dp(mcoord)                                                     ! momentum increments
    real e                                                              ! energy
    real w                                                              ! weight
    integer*8 i                                                         ! sort index
  end type
!........................................................................
  type :: field                                                         ! species on mesh point
    real d, v(mcoord)                                                   ! density, velocity
    real vth                                                            ! estimated thermal velocity
    integer i                                                           ! starting index
    integer n                                                           ! number of particles
  end type
!........................................................................
  type field_type_deposit
    real d, v(mcoord)
  end type
!........................................................................
  real, allocatable, dimension(:,:,:,:,:) :: vthb                       ! thermal velo array, for boundary conditions,
                                                                        ! vthb[nx,ny,lower.or.upper,vth.or.vavz.or.vthz,sp]
!........................................................................
  type :: spec                                                          ! species data type
    integer*8 tnp                                                       ! total nr of particles
    integer*8 imax                                                      ! highest part id in use
    real mass                                                           ! mass
    real charge                                                         ! charge, in units of elm_q => charge=Z
    real lifetime                                                       ! life time
    real temperature                                                    ! temperature
    real ini_weight                                                     ! intial weight for the particles
    real min_weight                                                     ! particles with less weight are killed 
    real max_weight                                                     ! particles with larger weight are split
    integer mp                                                          ! max particles
    integer np                                                          ! number of particles
    integer nmax                                                        ! trigger to kill
    integer nmin                                                        ! trigger do duplicate
    integer ntarget                                                     ! trigger to kill
    ! OBS It is (maybe) nice to have the particle pointer with a 128 byte alignment
    type(particle), allocatable, dimension(:)         :: particle       ! particle pointers
    integer*8 lp                                                        ! theoretical max for particles. Set at 2^28 = 256 (2^10)^2 = 256 Mpart
    integer nmaxcell                                                    ! max nr of parts in a cell
    character(len=mtxt) name                                            ! name
  end type
!........................................................................
  type :: spec_io                                                       ! species data type
    sequence                                                            ! keep the order!
    integer*8 tnp                                                       ! total nr of particles
    integer*8 imax                                                      ! highest part id in use
    real(kind=4) mass                                                   ! mass
    real(kind=4) charge                                                 ! charge, in units of elm_q => charge=Z
    real(kind=4) lifetime                                               ! life time
    real(kind=4) temperature                                            ! temperature
    real(kind=4) min_weight                                             ! particles with less weight are killed 
    integer mp                                                          ! max particles
    integer np                                                          ! number of particles
    integer nmax                                                        ! trigger to kill
    integer nmin                                                        ! trigger do duplicate
    integer ntarget                                                     ! trigger to kill
    character(len=mtxt) name                                            ! name
  end type
!........................................................................
  type :: cellvector                                                    ! cell vector data type
    integer mp                                                          ! max particles
    integer np                                                          ! number of particles
    integer inp                                                         ! initial number of part
    integer ix                                                          ! in which x-cube are we?
    integer iy                                                          ! in which y-pencil are we?
    integer iz                                                          ! in which z-slice are we?
    type(particle), pointer, dimension(:)   :: particle                 ! particle pointer
    real,           pointer, dimension(:,:) :: dp                       ! momentum change
  end type
!........................................................................
  type(field)   , allocatable, target, dimension(:,:,:,:) :: fields     ! species on mesh
  type(spec)    , allocatable, target, dimension(:)   :: sp             ! species as particles
  type(field_type_deposit), allocatable, dimension(:,:,:,:,:) :: lfields! suming up fields on mesh

  ! variables local to the module
  integer*8,      allocatable, dimension(:)   :: ip_offset

  real, parameter :: kilobyte = 1024.**1
  real, parameter :: megabyte = 1024.**2
  real, parameter :: gigabyte = 1024.**3
  integer*8       :: n_tomerge=0, n_merged=0, n_split=0, n_npmin=0, &
                     n_npmax=0, n_zero=0
  integer         :: npmin=999, npmax=0
  logical         :: split_overflow=.false.
  real            :: merge_tol = 0.20
!
CONTAINS

!=======================================================================
FUNCTION strupcase(string)
!> \brief function to convert string to upper case-
  implicit none
  character(len=*), intent(in) :: string
  character(len=len(string))   :: strupcase
  integer :: j
  do j = 1,len(string)
    if(string(j:j) >= "a" .and. string(j:j) <= "z") then
         strupcase(j:j) = achar(iachar(string(j:j)) - 32)
    else
         strupcase(j:j) = string(j:j)
    end if
  end do
END FUNCTION strupcase

!=======================================================================
FUNCTION get_SpeciesNr(name) result(isp)
  implicit none
  character(len=mtxt), intent(in) :: name
  integer                         :: isp
  if (len_trim(name) .eq. 0) then
    isp = -1
    return
  endif
  do isp=1,nspecies
    if (strupcase(name) .eq. strupcase(sp(isp)%name)) return
  enddo
  isp = -1
END FUNCTION get_SpeciesNr

!=======================================================================
END MODULE

!=======================================================================
MODULE species_mod
  USE species
  implicit none
  integer :: verbose  = 0
  private
  public :: sp, nspecies, merge_tol, verbose
END MODULE species_mod

!=======================================================================
SUBROUTINE read_species
  USE species_mod
  USE grid_m,   only : g, ngrid
  USE units,  only : c, u
  USE params, only : writefac_param=>writefac, master, &
     stdin, stdout, out_namelists, params_unit, mpi, mtxt, &
     do_simple_renormalize, do_weightchange
  implicit none
  character(len=mtxt) name
  real    :: mass, charge, lifetime, temperature, weight, &
             min_new_weight, min_weight, max_weight
  integer :: npercell, minpercell, maxpercell, isp, ninterior, i, sz
  integer(kind=8) :: writefac
  
  namelist /species/ name, mass, charge, npercell, minpercell, &
    maxpercell, lifetime, temperature, weight, min_weight, max_weight, &
    merge_tol, writefac, verbose
!.......................................................................
  rewind (stdin)

  ninterior = 1
  do i=1,3
    sz = (g%ub(i) - g%lb(i))
    if (mpi%lb(i)) sz = sz + (g%lb(i) - 1)
    if (mpi%ub(i)) sz = sz + (g%n(i) - g%ub(i) + 1)
    ninterior = ninterior * sz    
  enddo
  
!-----------------------------------------------------------------------
! Species reading, counting, etc.
!-----------------------------------------------------------------------
  do isp=1,nspecies
    name     = sp(isp)%name
    mass     = sp(isp)%mass / c%mu
    charge   = sp(isp)%charge / c%elm_q
    lifetime = sp(isp)%lifetime * u%t
    npercell = sp(isp)%ntarget
    maxpercell = 0
    minpercell = 0
    temperature = sp(isp)%temperature                                   ! initial temparature
    weight   = sp(isp)%ini_weight
    min_weight = sp(isp)%min_weight
    max_weight = sp(isp)%max_weight
    writefac = writefac_param(isp)
    read (stdin,species)                                                ! read species params
    if (name(1:6) .eq. 'photon'  .and. charge .ne. 0) then 
      call warning('init_species','Photon had charge!') 
      charge = 0.
    endif
    if (name(1:6) .eq. 'photon'  .and. mass   .ne. 0) then 
      call warning('init_species','Photon had mass!')
      mass = 0.
    endif
    if (name(1:7) .eq. 'neutron' .and. charge .ne. 0) then 
      call warning('init_species','Neutron had charge!')
      charge = 0.
    endif

    if (minpercell == 0) minpercell = 2*npercell/3                      ! minimum number
    if (maxpercell == 0) maxpercell = 2*npercell                        ! room for more
    if (npercell == 0) npercell = maxpercell/2                          ! if max given
    if (out_namelists) write (stdout,species)                           ! print species params
    if (master)        write (params_unit,species)                      ! write to params file
    sp(isp)%mass = mass*c%mu                                            ! mass
    sp(isp)%charge = charge*c%elm_q                                     ! charge
    sp(isp)%lifetime = lifetime/u%t                                     ! nondim lifetime
    sp(isp)%name = name                                     
    sp(isp)%temperature = temperature                                   ! initial temparature
    if (sp(isp)%mp == 0) sp(isp)%mp = maxpercell*ninterior              ! max particles
   !if (sp(isp)%np == 0) sp(isp)%np = npercell*ninterior                ! actual particles
    sp(isp)%ntarget = npercell                                          ! target number
    sp(isp)%nmin = minpercell                                           ! threshhold to duplicate
    sp(isp)%nmax = (npercell+maxpercell)/2                              ! threshhold to kill
    sp(isp)%ini_weight = weight                                         ! initial weight of the particles
    sp(isp)%min_weight = min_weight                                     ! minimum part.weight survival acceptance
    sp(isp)%max_weight = max_weight                                     ! maximum part.weight before split
    do_simple_renormalize = do_simple_renormalize .or. (min_weight > 0.)
    do_weightchange = do_weightchange .or. do_simple_renormalize
    writefac_param(isp) = writefac
  end do

END SUBROUTINE read_species

!=======================================================================
SUBROUTINE write_species
  USE params, ONLY: data_unit, nspecies, mid, stdout
  USE species, ONLY: sp, spec_io
  implicit none
  type(spec_io), dimension(nspecies) :: sp_io
  character(len=mid) :: &
    id = "species.f90 $Id$"
  integer, parameter:: io_format=1                                      ! increment whenever changing write_species
!.......................................................................
  write (data_unit) io_format,mid
  write (data_unit) id
  write (data_unit) 1
  sp_io%tnp         = sp%tnp
  sp_io%imax        = sp%imax
  sp_io%mass        = sp%mass
  sp_io%charge      = sp%charge
  sp_io%lifetime    = sp%lifetime
  sp_io%temperature = sp%temperature
  sp_io%min_weight  = sp%min_weight
  sp_io%mp          = sp%mp
  sp_io%np          = sp%np
  sp_io%nmax        = sp%nmax
  sp_io%nmin        = sp%nmin
  sp_io%ntarget     = sp%ntarget
 !sp_io%nmaxcell    = sp%nmaxcell
  sp_io%name        = sp%name
 !write (data_unit) sizeof(sp_io)/sizeof(1.),3
  write (data_unit) nspecies*18,3
  write (data_unit) sp_io
  write (data_unit) sp_io
END SUBROUTINE write_species

!=======================================================================
SUBROUTINE read_species_header
  USE params, ONLY: data_unit, nspecies, mid, stdout
  USE species, ONLY: sp, spec_io
  USE header
  implicit none
  type(spec_io), dimension(nspecies) :: sp_io
  character(len=mid) :: &
    id = "species.f90 $Id$"
  integer            :: io_format=1                                     ! inc whenever changing write_species
  integer            :: l,t
!.......................................................................
  call check_header_format(io_format, id, 'read_species_header')
  read (data_unit) l,t
  read (data_unit) sp_io
  read (data_unit) sp_io ! read twice because we have ints and reals
  if (any(sp_io%mass   .ne. sp%mass)) &
    call error('read_species_header','Masses have changed')
  if (any(sp_io%charge .ne. sp%charge)) &
    call error('read_species_header','Charges have changed')
  !sp_io%tnp         = sp%tnp
  !sp_io%imax        = sp%imax
  !sp_io%mass        = sp%mass
  !sp_io%charge      = sp%charge
  !sp_io%lifetime    = sp%lifetime
  !sp_io%temperature = sp%temperature
  !sp_io%min_weight  = sp%min_weight
  !sp_io%mp          = sp%mp
  !sp_io%np          = sp%np
  !sp_io%nmax        = sp%nmax
  !sp_io%nmin        = sp%nmin
  !sp_io%ntarget     = sp%ntarget
  !!sp_io%nmaxcell    = sp%nmaxcell
  !sp_io%name        = sp%name
END SUBROUTINE read_species_header

!=======================================================================
SUBROUTINE init_species
  USE species_mod, only: verbose
  USE species, only : sp, fields, vthb, particle_length, ip_offset, &
                      gigabyte
  USE params,  only : mpi, mcoord, mdim, nspecies, rank, master, &
                      GBpercore, nodes, hl, do_simple_renormalize, &
                      stdall,  mid, stdout, writefac_global, &
                      writefac, nomp, out_id
  USE units,   only : c, elm
  USE grid_m,    only : g, ngrid, nx, ny, nz
  implicit none
  real    :: memleft, dsknode, memnode, mem4particles
  integer, allocatable, dimension(:,:) :: gnp, gmp
  logical :: noprio, ploc_stat

  integer(kind=8) :: bytesleft
  integer :: partbytes, partbytesslow, partbytesfast
  integer :: partexpand, ninterior, nprio, i, sz, maxpercell
  integer :: ispexpand
  real r0(mdim)
  integer   :: ix, iy, iz, isp, lb, ub, alloc_stat, alloc_stat_global, &
    alloc_try, dealloc_stat
  integer(kind=8) :: ip, ip0, ip1
  character(len=mid) :: text
  character(len=mid) :: &
    id = "species.f90 $Id$"
!.......................................................................
  call print_id(id)

  call species_communicators                                            ! species communicators 
  if (nspecies==0) return

  if (.not. allocated(sp) .and. nspecies > 0) then                      ! allocate species
    allocate (sp(nspecies), ip_offset(nspecies), writefac(nspecies))
  else
    call warning('init_species', &
     "You are changing fundamental parameters. Don't change too much!")
  endif

  sp%charge = 0.
  sp%mass = 0.
  sp%temperature = 0.                                                   ! initial temparature
  sp%lifetime = 0.
  sp%ntarget = 30
  sp%ini_weight   = 1.
  sp%min_weight = 0.
  sp%max_weight = 100.
  sp%np = 0
  sp%mp = 0
  sp%nmaxcell = 0
  writefac = writefac_global
  do_simple_renormalize = .false. 

  call read_species

!-----------------------------------------------------------------------
! Allocate fields
!-----------------------------------------------------------------------
  if (master) then
    if(.not.out_id) print'(1x,a)', hl
    print'(a,f5.2,a)', ' Using up to', GBpercore, ' GB/core'
    if(verbose>0) print*,'Allocating fields'
  end if
  allocate (fields(nx,ny,nz,nspecies))                                  ! allocate species fields
  allocate (vthb(nx,ny,2,3,nspecies))                                   ! do boundary thermal velocities

!-----------------------------------------------------------------------
! Estimate effective nr of grid points for setting sp%lp.
! PREC is the number of bytes per float.
!-----------------------------------------------------------------------
  ngrid = product(g%n)                                                  ! local number of grids
  memnode = (2+mcoord)*nspecies*ngrid*PREC/gigabyte                     ! GB due to interpolated fields
  memnode = memnode + 20.*ngrid*PREC/gigabyte                           ! GB due to E-M fields
  if (nspecies > 0) then
    memnode = memnode + real(maxval(sp%mp))*8/gigabyte                  ! GB due to sort & other scratch
    mem4particles = GBpercore*nomp - memnode                            ! GB available for particles proper
  endif

!-----------------------------------------------------------------------
! Allocate particles, after checking that we have enough memory
!-----------------------------------------------------------------------
  if (nspecies > 0) then
    if (master .and. verbose>0) &
      print*,'Allocating particles'

    sp%lp = (mem4particles*gigabyte*sp%mp)/(sum(sp%mp)*particle_length*4.)
    do isp=1,nspecies
      if (sp(isp)%mp > sp(isp)%lp) then
        if (master) write(stdout,'(3x,a,2i8,a,i2,a,f8.2)') &
          'GBpercore set too low. ',sp(isp)%lp/1024, sp(isp)%mp/1024, &
          ' kparticles needed for species ', isp,'.  Setting lp=mp'
        sp(isp)%lp = sp(isp)%mp
      endif
      if (master .and. verbose>0) &
        print'(a,i4,2f10.2)' ,' Species, lp, mp [kp/proc] =', &
          isp, sp(isp)%lp/1024., sp(isp)%mp/1024.
      if (allocated(sp(isp)%particle)) deallocate(sp(isp)%particle)     ! be sure to start clean
      allocate (sp(isp)%particle(sp(isp)%lp),STAT=alloc_stat)           ! allocate theoretical max
    end do

    if (master) then
      write(stdout,'(a,i8,a,i8)') ' Allocated virtual space for ', &
         sp(1)%lp/1024, ' kparticles / species / process'
      write(stdout,*) hl
    end if
  endif

!-----------------------------------------------------------------------
! Compute particle index offsets from global particle numbers
!-----------------------------------------------------------------------
  allocate(gnp(nspecies,nodes),gmp(nspecies,nodes))
  call global_nr_of_particles(gnp,gmp)                                  ! get global particle cnts
  ip_offset = 0                                                         ! offset for part id nrs
  if (rank .gt. 0) ip_offset = sum(int(gmp(:,1:rank),kind=8),dim=2)     ! 0 if rank=0, else sum of 
                                                                        ! max parts in lower ranks
  do isp=1,nspecies
    sp(isp)%imax = 0
    do i=1,nodes
      sp(isp)%imax = sp(isp)%imax + int(gmp(isp,i),kind=8)              ! max id in use
    enddo
  enddo
  deallocate(gnp,gmp)

!-----------------------------------------------------------------------
! Initialize fields and particles to zero, to encourage memory affinity
!-----------------------------------------------------------------------
  do isp=1,nspecies
    if (trim(sp(isp)%name) .eq. "electron") then
      c%mc     = sp(isp)%mass * c%c 
      c%omc    = 1/c%mc
      c%mc2    = c%mc * c%c
      c%omc2   = 1/c%mc2
      c%sigma_thom = ((c%elm_q/c%mc)*(elm%ke*c%elm_q/c%c))**2           ! sigma-Thompson, system-free units
      c%sigma_comp = c%sigma_thom*(3./4.)                               ! sigma-Compton, system-free units
    endif
    if (sp(isp)%mp .eq. 0) then
      write(text,'(a,i2)') 'no particles, species', isp
      call warning('init_species',text)
    end if
    do ip=1,sp(isp)%mp
      sp(isp)%particle(ip)%r = 0.                                       ! touch 
      sp(isp)%particle(ip)%e = 0.                                       ! touch
      sp(isp)%particle(ip)%p = 0.                                       ! touch 
      sp(isp)%particle(ip)%w = 0.                                       ! touch
      sp(isp)%particle(ip)%q = 0                                        ! touch
      sp(isp)%particle(ip)%bits = 0                                     ! touch
      sp(isp)%particle(ip)%i = ip_offset(isp) + ip                      ! touch 
    end do
    do iz=1,nz
    do iy=1,ny
    do ix=1,nx
      fields(ix,iy,iz,isp)%d = 0.                                       ! number density
      fields(ix,iy,iz,isp)%v = 0.                                       ! velocity
      fields(ix,iy,iz,isp)%i = 0                                        ! cell particle index
      fields(ix,iy,iz,isp)%n = 0                                        ! number of particles
    end do
    end do
    end do
  end do

  if (allocated(ip_offset)) deallocate(ip_offset)
END SUBROUTINE init_species
