! Patch/hybrid/flux_solver.f90 $Id$
! vim: nowrap
!===============================================================================
! Module with a solver that inverts exactly a 6th order stagger operator
! We still need to transpose the fields to make the solve along the full ray on
! a single core. This is done in the mpi module.
!===============================================================================
MODULE flux_solver
  implicit none
#if PREC==4
  integer, parameter :: wp_fl=4                                                 ! kind for penta solve oprations
#else
#if PREC==8
  integer, parameter :: wp_fl=8                                                 ! kind for penta solve oprations
#else
  integer, parameter :: wp_fl=16                                                ! kind for penta solve oprations
#endif
#endif
  integer, parameter :: order=6                                                 ! order of stagger ops
  real(kind=wp_fl), parameter :: dd(3)=(/1067./960.,-29./480.,3./640./)         ! coefs for a+b+c,b+c,c
  integer            :: max_n=-1
  real(kind=wp_fl), allocatable :: alpha(:), gamma(:), delta(:)
!===============================================================================
CONTAINS
! Solve a symmetric penta-diagonal linear system with fixed coefficients and
! diagonal a, diagonal+1 b, diagonal+2 c, and rhs r
! The solution is left in r on return
! Based on algorithm from : Numerical algorithms with C by Gisela Engeln-M�llges, Frank Uhlig
! This version has the input array transposed compared to the version below
!===============================================================================
SUBROUTINE penta_solve_transposed(n,ny,a,b,c,r)
  implicit none
  integer, intent(in)                :: n, ny
  real(kind=wp_fl),    intent(in)    :: a, b, c
  real(kind=wp_fl),  dimension(ny,n) :: r
  real(kind=wp_fl)                   :: inv
  integer                            :: i,j

  !$omp master
  if (n > max_n) then
    if (allocated(alpha)) deallocate(alpha,gamma,delta)
    allocate(alpha(n),gamma(n-1),delta(n-2))
    max_n=n
  endif
  alpha(1) = a
  inv      = 1./a
  gamma(1) = b*inv
  delta(1) = c*inv

  alpha(2) = a-b*gamma(1)
  inv      = 1./alpha(2)
  gamma(2) = (b-c*gamma(1))*inv
  delta(2) = c*inv

  do i=3,n-2
    alpha(i) = a-c*delta(i-2)-alpha(i-1)*gamma(i-1)*gamma(i-1)
    inv      = 1./alpha(i)
    gamma(i) = (b-c*gamma(i-1))*inv
    delta(i) = c*inv
  enddo

  alpha(n-1) = a-c*delta(n-3)-alpha(n-2)*gamma(n-2)*gamma(n-2)
  gamma(n-1) = (b-c*gamma(n-2))/alpha(n-1)
  alpha(n)   = a-c*delta(n-2)-alpha(n-1)*gamma(n-1)*gamma(n-1)
  !$omp end master
  !$omp barrier
    
  !$omp do
  do j=1,ny
    r(j,2)=r(j,2)-gamma(1)*r(j,1)
  enddo
  !$omp enddo nowait
  do i=3,n
    !$omp do
    do j=1,ny
      r(j,i)   = r(j,i)-gamma(i-1)*r(j,i-1)-delta(i-2)*r(j,i-2)
      r(j,i-2) = r(j,i-2)/alpha(i-2)
    enddo
    !$omp enddo nowait
  enddo
  
  !$omp do
  do j=1,ny
    r(j,n)   = r(j,n)/alpha(n)
    r(j,n-1) = r(j,n-1)/alpha(n-1)-gamma(n-1)*r(j,n)
  enddo
  !$omp enddo nowait
  do i=n-2,1,-1
    !$omp do
    do j=1,ny
      r(j,i) = r(j,i)-gamma(i)*r(j,i+1)-delta(i)*r(j,i+2)
    enddo
    !$omp enddo nowait
  enddo
  !$omp barrier
END SUBROUTINE penta_solve_transposed
! Solve a symmetric penta-diagonal linear system with fixed coefficients and
! diagonal a, diagonal+1 b, diagonal+2 c, and rhs r
! The solution is left in r on return
! Based on algorithm from : Numerical algorithms with C by Gisela Engeln-M�llges, Frank Uhlig
!===============================================================================
SUBROUTINE penta_solve(n,ny,a,b,c,r)
  implicit none
  integer, intent(in)                :: n, ny
  real(kind=wp_fl),    intent(in)    :: a, b, c
  real(kind=wp_fl),  dimension(n,ny) :: r
  real(kind=wp_fl)                   :: inv
  integer                            :: i, j

  !$omp master
  if (n > max_n) then
    if (allocated(alpha)) deallocate(alpha,gamma,delta)
    allocate(alpha(n),gamma(n-1),delta(n-2))
    max_n=n
  endif
  alpha(1) = a
  inv      = 1.0_wp_fl/a
  gamma(1) = b*inv
  delta(1) = c*inv

  alpha(2) = a-b*gamma(1)
  inv      = 1.0_wp_fl/alpha(2)
  gamma(2) = (b-c*gamma(1))*inv
  delta(2) = c*inv

  do i=3,n-2
    alpha(i) = a-c*delta(i-2)-alpha(i-1)*gamma(i-1)*gamma(i-1)
    inv      = 1.0_wp_fl/alpha(i)
    gamma(i) = (b-c*gamma(i-1))*inv
    delta(i) = c*inv
  enddo

  alpha(n-1) = a-c*delta(n-3)-alpha(n-2)*gamma(n-2)*gamma(n-2)
  gamma(n-1) = (b-c*gamma(n-2))/alpha(n-1)
  alpha(n)   = a-c*delta(n-2)-alpha(n-1)*gamma(n-1)*gamma(n-1)
  !$omp end master
  !$omp barrier
    
  !$omp do
  do j=1,ny
    r(2,j)=r(2,j)-gamma(1)*r(1,j)
    do i=3,n
      r(i,j)   = r(i,j)-gamma(i-1)*r(i-1,j)-delta(i-2)*r(i-2,j)
      r(i-2,j) = r(i-2,j)/alpha(i-2)
    enddo
    
    r(n,j)   = r(n,j)/alpha(n)
    r(n-1,j) = r(n-1,j)/alpha(n-1)-gamma(n-1)*r(n,j)
    do i=n-2,1,-1
      r(i,j) = r(i,j)-gamma(i)*r(i+1,j)-delta(i)*r(i+2,j)
    enddo
  enddo
END SUBROUTINE penta_solve
!===============================================================================
END MODULE flux_solver
!===============================================================================
! Solve the flux under the assumption of zero current in the first cell in the boundary.
! The x-direction is treated transposed compared to the y- and z-, for optimal memory layout
!===============================================================================
SUBROUTINE compute_flux(nUx, nUy, nUz)
  USE grid_m,  only : g
  USE species, only : nspecies
  USE flux_solver
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3),nspecies), target :: nUx, nUy, nUz
  real,         dimension(:,:,:,:), pointer :: v
  real(kind=wp_fl),  allocatable, dimension(:,:) :: out
  real(kind=8), allocatable, dimension(:)   :: fluxv
  real(kind=8)                              :: flux
  integer                                   :: dir,n,area,i,j,ix,iy,iz,isp

  ! Loop of the three components Jx, Jy, and Jz
  !-----------------------------------------------------------------------------
  !$omp parallel private(dir,isp,ix,iy,iz,i,j,flux,n,area)
  do dir=1,3
    if (g%gn(dir)==1) cycle                                                     ! cycle if only one point in this direction
    n    = g%n(dir)
    area = product(g%n) / n
    !$omp master
    if (dir==1) v => nUx
    if (dir==2) v => nUy
    if (dir==3) v => nUz
    if (allocated(out)) deallocate(out)
    if (dir==1) then
      allocate(out(n,area))
    else
      if (allocated(fluxv)) deallocate(fluxv)
      allocate(fluxv(area))
      allocate(out(area,n))
    endif
    !$omp end master
    !$omp barrier

    do isp=1, nspecies

      ! Copy fields data to contigious memory 
      !-------------------------------------------------------------------------
      select case (dir)
      case(1)
        !$omp do
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          i = iy + (iz - 1)*g%n(2)
          do ix=1,g%n(1)
            j = ix
            out(j,i) = v(ix,iy,iz,isp)
          enddo
        enddo
        enddo
      case(2)
        !$omp do
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          j = iy
          do ix=1,g%n(1)
            i = ix + (iz - 1)*g%n(1)
            out(i,j) = v(ix,iy,iz,isp)
          enddo
        enddo
        enddo
      case(3)
        !$omp do
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          j = iz
          do ix=1,g%n(1)
            i = ix + (iy - 1)*g%n(1)
            out(i,j) = v(ix,iy,iz,isp)
          enddo
          enddo
        enddo
      endselect
 
      ! Solve the penta-diagonal equation system along the coordinate direction
      !-------------------------------------------------------------------------
      if (dir==1) then
        call penta_solve(n,area,dd(1),dd(2),dd(3),out)
        !$omp do
        do i=1,area
          flux = out(1,i)
          do j=2,n
            flux = flux + out(j,i)
            out(j,i) = flux
          enddo
        enddo
      else
        call penta_solve_transposed(n,area,dd(1),dd(2),dd(3),out)
        !$omp do 
        do i=1,area
          fluxv(i) = out(i,1)
        enddo
        !$omp enddo nowait
        do j=2,n
          !$omp do 
          do i=1,area
            fluxv(i) = fluxv(i) + out(i,j)
            out(i,j) = fluxv(i)
          enddo
          !$omp enddo nowait
        enddo
        !$omp barrier
      endif
 
      ! Copy data back to fields array -- now J is stored in the array
      !-------------------------------------------------------------------------
      select case (dir)
      case(1)
        !$omp do 
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          i = iy + (iz - 1)*g%n(2)
          do ix=1,g%n(1)
            j = ix
            v(ix,iy,iz,isp) = out(j,i)
          enddo
        enddo
        enddo
      case(2)
        !$omp do 
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          j = iy
          do ix=1,g%n(1)
            i = ix + (iz - 1)*g%n(1)
            v(ix,iy,iz,isp) = out(i,j)
          enddo
        enddo
        enddo
      case(3)
        !$omp do 
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          j = iz
          do ix=1,g%n(1)
            i = ix + (iy - 1)*g%n(1)
            v(ix,iy,iz,isp) = out(i,j)
          enddo
          enddo
        enddo
      endselect

    enddo
  enddo
  !$omp end parallel

  if (allocated(fluxv)) deallocate(fluxv)
  deallocate(out)

END SUBROUTINE compute_flux
! Solve the flux under the assumption of zero current in the boundary. This is normally ok
! Notice how the x-direction is treated transposed compared to the z-, for optimal memory layout
!===============================================================================
SUBROUTINE compute_flux_2d
  USE grid_m,    only : g
  USE species, only : fields, nspecies
  USE flux_solver
  implicit none
  real(kind=wp_fl),  allocatable, dimension(:,:) :: out
  real(kind=8), allocatable, dimension(:)   :: fluxv
  real(kind=8)                              :: flux
  integer                                   :: dir,ix,iy,iz,isp

  ! Loop of the three components Jx, Jy, and Jz
  !-----------------------------------------------------------------------------
  iy = g%lb(2)
  allocate(out(g%n(1),g%n(3)),fluxv(g%n(1)))
  do dir=1,3,2

    do isp=1,nspecies

      ! Copy fields data to contigious memory 
      !-------------------------------------------------------------------------
      do iz=1,g%n(3)
      do ix=1,g%n(1)
        out(ix,iz) = fields(ix,iy,iz,isp)%v(dir)
      enddo
      enddo

      ! Solve the penta-diagonal equation system along the coordinate direction
      !-------------------------------------------------------------------------
      if (dir==1) then
        call penta_solve(g%n(1),g%n(3),dd(1),dd(2),dd(3),out)
        do iz=1,g%n(3)
          flux = 0.0_8
          do ix=1,g%n(1)
            flux = flux + out(ix,iz)
            out(ix,iz) = flux
          enddo
        enddo
      else
        call penta_solve_transposed(g%n(3),g%n(1),dd(1),dd(2),dd(3),out)
        fluxv = 0.0_8
        do iz=1,g%n(3)
          fluxv = fluxv + out(:,iz)
          out(:,iz) = fluxv
        enddo
      endif
 
      ! Copy data back to fields array -- now J is stored in the array
      !-------------------------------------------------------------------------
      do iz=1,g%n(3)
      do ix=1,g%n(1)
        fields(ix,iy,iz,isp)%v(dir) = out(ix,iz)
      enddo
      enddo
    enddo
  enddo
  deallocate(out,fluxv)

END SUBROUTINE compute_flux_2d
!===============================================================================
