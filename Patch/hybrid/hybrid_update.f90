! Patch/hybrid/hybrid_update.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------
MODULE hybrid_m
  implicit none
  real, dimension(:,:,:,:), allocatable, target :: nUx, nUy, nUz, nion_m
  real, dimension(:,:,:),   allocatable, target :: Ex_m, Ey_m, Ez_m, &
                                                   Ex_n, Ey_n, Ez_n
  real, dimension(:,:,:),   allocatable :: Bx_m, By_m, Bz_m, &          ! scratch arrays for time update
                                           Bx_n, By_n, Bz_n   
  real, dimension(:,:,:),   allocatable :: Dx,   Dy,   Dz,   &          ! scratch arrays for time update
                                           Dx_n, Dy_n, Dz_n             ! w shear-periodic boundaries

  real    :: T_electron=0.0                                             ! Electron temperature
  real    :: q_electron=0.0                                             ! Electron charge
  real    :: eta=0.0                                                    ! Resistivity
  ! If set to false use simple energy conserving method
  ! If set to true use more complicated mass conserving method
  logical :: do_mass_conservation=.true.                                ! Mass conservation on the mesh ?
  logical :: do_initial_time_staggering=.false.                         ! Initial time staggering of B and v
  logical :: do_drift=.false.                                           ! If set do drift instead of kick initially
  logical :: do_electron_density_floor=.true.                           ! Prevent division by zero in cells with no particles
  integer, parameter     :: nvmax=3                                     ! no of CFL-criteria
  real, dimension(nvmax) :: vmax                                        ! CFL-criteria
  character(len=100)     :: method='beyers'                             ! Integration method
  real    :: tol=1e-5                                                   ! Iteration tolerance
  integer :: itmax=20                                                   ! Max nr of its in iterative method
CONTAINS
!-----------------------------------------------------------------------
SUBROUTINE hybrid_update_beyers
  USE params, only : mid, do_shear, dtp
  USE species,only : fields
  USE grid_m, only : g, Ex, Ey, Ez, Bx, By, Bz
  implicit none
  integer            :: i
  logical            :: do_update
  !
  ! Update magnetic field to middle and next timestep (n and n+1/2)
  !---------------------------------------------------------------------
  if (do_shear) then
    call orbital_advection_B(Bx,By,Bz,-dtp)
    call orbital_advection_E(Ex,Ey,Ez,Dx,Dy,Dz,-0.5*dtp)
    call b_push(Bx,By,Bz,Dx,Dy,Dz,Bx_m,By_m,Bz_m,Bx_n,By_n,Bz_n)
    call orbital_advection_B(Bx_m,By_m,Bz_m,0.5*dtp)
  else
    call b_push(Bx,By,Bz,Ex,Ey,Ez,Bx_m,By_m,Bz_m,Bx_n,By_n,Bz_n)
  endif

  ! Store magnetic field for next timestep (B goes from n-1/2 to n+1/2)
  !---------------------------------------------------------------------
  Bx = Bx_n; By = By_n; Bz = Bz_n

  !.....................................................................
  ! Magnetic field updated to t = t_n + dt / 2
  !.....................................................................

  ! Move particles forward and calculate charge and current density
  ! Ion density and momenta - nion_m, Ux, Uy, Uz - are at n+1/2
  !---------------------------------------------------------------------

  do_update = .true.
  call move_particles(nion_m,nUx,nUy,nUz,Bx_m,By_m,Bz_m,Ex,Ey,Ez,do_update)

  !.....................................................................
  ! Particle positions updated to    t = t_n + dt
  ! Particle velocities updated to   t = t_n + dt / 2
  ! Ion densities updated to         t = t_n + dt
  ! Ion momentum densties updated to t = t_n + dt / 2
  !.....................................................................

  ! Calculate electric field from Ohm's law at time n+1/2
  !---------------------------------------------------------------------

  call ohms_law(Bx_n,By_n,Bz_n,nUx,nUy,nUz,nion_m,Ex_m,Ey_m,Ez_m)

  ! Predict new electric field. E[xyz]_n is at time n+1
  !---------------------------------------------------------------------
  if (do_shear) then
    call e_predict(Dx,Dy,Dz,Ex_m,Ey_m,Ez_m,Dx_n,Dy_n,Dz_n)
    call orbital_advection_E(Dx_n,Dy_n,Dz_n,Ex_n,Ey_n,Ez_n,-0.5*dtp)
  else
    call e_predict(Ex,Ey,Ez,Ex_m,Ey_m,Ez_m,Ex_n,Ey_n,Ez_n)
  endif

  ! Update a predicted magnetic field to middle and next timestep (n+1 and n+3/2)
  !---------------------------------------------------------------------
  if (do_shear) then
    call b_push(Bx,By,Bz,Dx_n,Dy_n,Dz_n,Bx_m,By_m,Bz_m,Bx_n,By_n,Bz_n)
  else
    call b_push(Bx,By,Bz,Ex_n,Ey_n,Ez_n,Bx_m,By_m,Bz_m,Bx_n,By_n,Bz_n)
  endif

  ! Calculate ion density and velocities at n+3/2 without moving particles
  !---------------------------------------------------------------------
  if (do_shear) call orbital_advection_B(Bx_m,By_m,Bz_m,-0.5*dtp)

  do_update = .false.
  call move_particles(nion_m,nUx,nUy,nUz,Bx_m,By_m,Bz_m,Ex_n,Ey_n,Ez_n,do_update)

  ! Calculate electric field from Ohm's law at time n+3/2
  !---------------------------------------------------------------------
  if (do_shear) call orbital_advection_B(Bx_n,By_n,Bz_n,-0.5*dtp)

  call ohms_law(Bx_n,By_n,Bz_n,nUx,nUy,nUz,nion_m,Ex_n,Ey_n,Ez_n)

  ! Correct the electric field. E[xyz] is now the fully updated field at time n+1
  !---------------------------------------------------------------------
  if (do_shear) then
    call orbital_advection_E(Ex_n,Ey_n,Ez_n,Dx_n,Dy_n,Dz_n,0.5*dtp)
    call orbital_advection_E(Ex_m,Ey_m,Ez_m,Dx,Dy,Dz,-0.5*dtp)
    call e_correct(Dx,Dy,Dz,Ex,Ey,Ez,Dx_n,Dy_n,Dz_n)
  else
    call e_correct(Ex_m,Ey_m,Ez_m,Ex,Ey,Ez,Ex_n,Ey_n,Ez_n)
  endif

  !.....................................................................
  ! Electric field updated to t = t_n + dt
  !.....................................................................
END SUBROUTINE hybrid_update_beyers
!-----------------------------------------------------------------------
SUBROUTINE hybrid_update_iterative
  USE params, only : mid
  USE species,only : fields
  USE grid_m, only : g, Ex, Ey, Ez, Bx, By, Bz
  implicit none
  integer            :: it
  real               :: error
  logical            :: do_update
  !
  ! Move particles forward and calculate charge and current density
  !---------------------------------------------------------------------
  do_update = .true.
  call move_particles(nion_m,nUx,nUy,nUz,Bx,By,Bz,Ex,Ey,Ez,do_update)

  !.....................................................................
  ! Particle positions updated to    t = t_n + dt
  ! Particle velocities updated to   t = t_n + dt / 2
  ! Ion densities updated to         t = t_n + dt
  ! Ion momentum densties updated to t = t_n + dt / 2
  !.....................................................................

  ! Iterate to find new Electric and Magnetic fields
  !---------------------------------------------------------------------
  it=0
  error = 2 * tol
  Ex_m = Ex; Ey_m = Ey; Ez_m = Ez
  do while (error > tol .and. it < itmax)
  
    ! Update magnetic field to middle and next timestep (n and n+1/2)
    !---------------------------------------------------------------------
    call b_push(Bx,By,Bz,Ex_m,Ey_m,Ez_m,Bx_m,By_m,Bz_m,Bx_n,By_n,Bz_n)

    ! Calculate electric field from Ohm's law at time n+1/2
    !---------------------------------------------------------------------
    call ohms_law(Bx_m,By_m,Bz_m,nUx,nUy,nUz,nion_m,Ex_m,Ey_m,Ez_m)

    ! Predict new electric field. E[xyz]_n is at time n+1
    !---------------------------------------------------------------------
    call e_predict(Ex,Ey,Ez,Ex_m,Ey_m,Ez_m,Ex_n,Ey_n,Ez_n)

    it = it + 1
  enddo
  ! Store fields for next timestep (E,B goes from n to n+1)
  !---------------------------------------------------------------------
  Bx = Bx_n; By = By_n; Bz = Bz_n
  Ex = Ex_n; Ey = Ey_n; Ez = Ez_n

  !.....................................................................
  ! Electric and Magnetic fields updated to t = t_n + dt
  !.....................................................................

END SUBROUTINE hybrid_update_iterative
!-----------------------------------------------------------------------
END MODULE hybrid_m
!-----------------------------------------------------------------------
SUBROUTINE read_hybrid_namelist
  USE params, only : master, out_namelists, params_unit, stdin, stdout
  USE hybrid_m
  USE species, only : sp
  implicit none
  !
  namelist /hybrid/ T_electron, q_electron, do_mass_conservation, do_initial_time_staggering, &
                    do_electron_density_floor, eta, method, tol, itmax
  !
  if (q_electron==0.0) q_electron = - sp(1)%charge                       ! Default electron charge
  rewind(stdin); read(stdin,hybrid)

  if (out_namelists) write(stdout,hybrid)
  if (master)        write(params_unit,hybrid)
END SUBROUTINE read_hybrid_namelist
!-----------------------------------------------------------------------
SUBROUTINE init_hybrid
  USE params,  only : stdin, stdout, out_namelists, master, params_unit, &
                      mid, do_overlap_add_at_io, dt, dtp, do_maxwell, do_shear
  USE grid_m,  only : g, Bx, By, Bz, Ex, Ey, Ez
  USE species, only : nspecies, sp, fields
  USE hybrid_m
  implicit none
  logical :: add
  integer :: nx, ny, nz
  character(len=mid) :: id = 'Patch/hybrid/hybrid_update.f90 $Id$'
!.......................................................................
  call print_id(id)

  if (nspecies > 1) call error('init_hybrid', &
    'The hybrid solver currently only support having 1 species')

  nx=g%n(1); ny=g%n(2); nz=g%n(3)
  allocate(Bx_m(nx,ny,nz), By_m(nx,ny,nz), Bz_m(nx,ny,nz), &            ! Allocate scratch arrays
           Bx_n(nx,ny,nz), By_n(nx,ny,nz), Bz_n(nx,ny,nz), &
           Ex_m(nx,ny,nz), Ey_m(nx,ny,nz), Ez_m(nx,ny,nz), &
           Ex_n(nx,ny,nz), Ey_n(nx,ny,nz), Ez_n(nx,ny,nz), &
           nUx(nx,ny,nz,nspecies), nUy(nx,ny,nz,nspecies),   &
           nUz(nx,ny,nz,nspecies), nion_m(nx,ny,nz,nspecies))

  if (do_shear) &
    allocate(Dx  (nx,ny,nz), Dy  (nx,ny,nz), Dz  (nx,ny,nz), &            ! Allocate scratch arrays for shear-periodic box
             Dx_n(nx,ny,nz), Dy_n(nx,ny,nz), Dz_n(nx,ny,nz))

  !---------------------------------------------------------------------

  ! Integration method. Choices are: beyrs, iterative
 
  call read_hybrid_namelist

  call init_particle_pusher                                             ! Point to correct particle integrator

  ! In the hybrid code, we do the add at each timestep, in the 
  ! impose_source_boundaries instead of at IO
  do_overlap_add_at_io = .false.

  ! Update boundary conditions for B-fields
  call b_boundaries(Bx,By,Bz)

  ! Make sure the field array has been initialized, and the sources are computed
  call send_particles
  call sort
  call mesh_sources

  ! Update source boundaries
  nion_m = fields%d
  nUx    = fields%v(1)
  nUy    = fields%v(2)
  nUz    = fields%v(3)
  add = .true.

  ! Boundaries for integrated quantities
  call impose_source_boundaries(nion_m, nUx, nUy, nUz, add)             
  fields%d    = nion_m
  fields%v(1) = nUx
  fields%v(2) = nUy
  fields%v(3) = nUz

  ! Calculate initial E-field
  if (do_maxwell) then
    call ohms_law(Bx,By,Bz,nUx,nUy,nUz,nion_m,Ex,Ey,Ez)
  else
    ! Update boundary conditions for E-fields
    call e_boundaries(Ex,Ey,Ez)
  endif

  ! Calculate initial dt
  call set_dt

  ! Check if experiment have asked for time staggering
  ! If yes, do a single Euler timestep
  if (do_initial_time_staggering) then
    if (do_drift) then
      dt = 0.5 * dt; dtp = dt                                                  ! set dt to half a time step forwards
      ! Update particle positions with a simple kick
      !---------------------------------------------------------------------
      call particle_position_update

      ! REMEMBER
      ! In case of do_drift the code assumes E- and B-fields are already correctly time centered
      ! REMEMBER

      dt = 2. * dt; dtp = dt                                                   ! reset dt back to original value
    else
      dt = -0.5 * dt; dtp = dt                                                  ! set dt to half a time step backwards

      ! Update particle velocities
      !---------------------------------------------------------------------
      call particle_velocity_update(Bx,By,Bz,Ex,Ey,Ez)

      if (do_maxwell) then
        if (trim(method) .ne. 'iterative') then
          ! Update magnetic field
          !-----------------------------------------------------------------
          call b_push(Bx,By,Bz,Ex,Ey,Ez,Bx_m,By_m,Bz_m,Bx_n,By_n,Bz_n)
          Bx = Bx_n; By = By_n; Bz = Bz_n
          if (do_shear) call orbital_advection_B(Bx,By,Bz,-dtp)
        end if
      endif

      dt = -2. * dt; dtp = dt                                                   ! reset dt back to original value
    endif
  endif

END SUBROUTINE init_hybrid

! Central timestep routine
!-----------------------------------------------------------------------
SUBROUTINE hybrid_update
  USE params, only : do_maxwell
  USE grid_m, only : Ex, Ey, Ez, Bx, By, Bz
  USE hybrid_m
  implicit none
  logical :: do_update
  !
  if (do_maxwell) then ! Fields are time evolved

    select case(trim(method))
    case('beyers')
      call hybrid_update_beyers
    case('iterative')
      call hybrid_update_iterative
    case default
      call error('hybrid_update','Unknow integration method. Has to be beyers or iterative. method='//trim(method))
    end select

  else

    ! Move particles forward and calculate charge and current density
    ! Ion density and momenta - nion_m, Ux, Uy, Uz - are at n+1/2
    !---------------------------------------------------------------------
    do_update = .true.
    call move_particles(nion_m,nUx,nUy,nUz,Bx,By,Bz,Ex,Ey,Ez,do_update)

    !.....................................................................
    ! Particle positions updated to    t = t_n + dt
    ! Particle velocities updated to   t = t_n + dt / 2
    ! Ion densities updated to         t = t_n + dt
    ! Ion momentum densties updated to t = t_n + dt / 2
    !.....................................................................

  endif
END SUBROUTINE hybrid_update
!-----------------------------------------------------------------------
