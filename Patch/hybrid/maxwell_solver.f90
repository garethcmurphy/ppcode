! Patch/hybrid/maxwell_solver.f90 $Id$
! vim: nowrap
!=======================================================================
MODULE maxwell_solver
  integer, save:: iter_e=0, iter_b=0                                    ! for write_log
END MODULE maxwell_solver

!=======================================================================
! Magnetic field update: B(t+dt/2) = B(t-dt/2) - dt * curl(E(t))
!=======================================================================
SUBROUTINE b_push(Bx,By,Bz,Ex,Ey,Ez,Bx_m,By_m,Bz_m,Bx_n,By_n,Bz_n)
  USE params, only : mid, dt
  USE grid_m, only : g
  USE pic_stagger
  implicit none
  real, dimension(g%n(1), g%n(2), g%n(3)), intent(in) :: Bx, By, Bz, &
                                                         Ex, Ey, Ez
  real, dimension(g%n(1), g%n(2), g%n(3))             :: Bx_m, By_m, Bz_m, &
                                                         Bx_n, By_n, Bz_n
  !
  real               :: dt4
  integer            :: iz
  character(len=mid) :: id='Patch/hybrid/maxwell_solver.f90 $Id$'
  !
  call print_id(id)
                                   call timer('b_push','start')
  dt4 = dt                                                              ! make sure dt is single prec

  ! Magnetic field update: B(t+dt/2) = B(t-dt/2) - dt * curl(E(t))
  !---------------------------------------------------------------------
  Bx_n = Bx - dt4 * (ddyup(Ez) - ddzup(Ey))
  By_n = By - dt4 * (ddzup(Ex) - ddxup(Ez))
  Bz_n = Bz - dt4 * (ddxup(Ey) - ddyup(Ex))

  ! Fix boundaries
  !---------------------------------------------------------------------
  call b_boundaries(Bx_n, By_n, Bz_n)

  ! Find B-field in between
  !---------------------------------------------------------------------
  Bx_m = 0.5 * (Bx + Bx_n)
  By_m = 0.5 * (By + By_n)
  Bz_m = 0.5 * (Bz + Bz_n)

END SUBROUTINE b_push
! Extrapolate the E-field: E(t+dt) = 2 E(t + dt/2) - E(t)
!=======================================================================
SUBROUTINE e_predict(Ex,Ey,Ez,Ex_m,Ey_m,Ez_m,Ex_n,Ey_n,Ez_n)
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1), g%n(2), g%n(3)), intent(in) :: Ex, Ey, Ez
  real, dimension(g%n(1), g%n(2), g%n(3)), intent(in) :: Ex_m, Ey_m, Ez_m
  real, dimension(g%n(1), g%n(2), g%n(3))             :: Ex_n, Ey_n, Ez_n
  !
  ! Predict electric field forward in time
  !---------------------------------------------------------------------
  Ex_n = 2. * Ex_m - Ex
  Ey_n = 2. * Ey_m - Ey
  Ez_n = 2. * Ez_m - Ez
  
END SUBROUTINE e_predict
! Correct the E-field: E(t+dt) = (E(t + dt/2) + E(t + 3dt/2)) / 2
!=======================================================================
SUBROUTINE e_correct(Ex,Ey,Ez,Ex_m,Ey_m,Ez_m,Ex_n,Ey_n,Ez_n)
  USE grid_m,  only : g
  implicit none
  real, dimension(g%n(1), g%n(2), g%n(3)), intent(in) :: Ex, Ey, Ez
  real, dimension(g%n(1), g%n(2), g%n(3)), intent(in) :: Ex_n, Ey_n, Ez_n
  real, dimension(g%n(1), g%n(2), g%n(3))             :: Ex_m, Ey_m, Ez_m
  !
  ! Find corrected electric field in between
  !---------------------------------------------------------------------
  Ex_m = 0.5 * (Ex + Ex_n)
  Ey_m = 0.5 * (Ey + Ey_n)
  Ez_m = 0.5 * (Ez + Ez_n)
  
END SUBROUTINE e_correct
! Number of boundary / ghost cell needed by this field method
!=======================================================================
SUBROUTINE field_solver_boundaries
  USE grid_m,        only : g
  implicit none
  g%lb = max(g%lb, 4)                                                   ! def lower field bndry
  g%ub = max(g%ub, 2)                                                   ! def upper field bndry (n-ub)
END SUBROUTINE
!=======================================================================
! Return iterations used (to avoid a module dependency)
!=======================================================================
SUBROUTINE maxwell_iterations (n_b, n_e)
  USE maxwell_solver, only: iter_b, iter_e
  implicit none
  integer n_b, n_e
  n_b = iter_b
  n_e = iter_e
END SUBROUTINE
