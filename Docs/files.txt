MOST RELEVANT FILES:
--------------------
  PPcodde/
    main.f90
    grid.f90
    units.f90
    species.f90
    set_dt.f90
    Fields/
      mesh_sources.f90
      maxwell_solver.f90
      maxwell_sources.f90
    Particles/
      move_particles.f90
    Experiments/*/
      init_experiment.f90

LESS RELEVANT FILES:
--------------------
    Fields/
      stagger_6th.f90
    Particles/
      celloperations.f90
      particleoperations.f90
	Interpolation/
	  tsc.f90
	  cubic.f90
	  csc.f90
    Boundaries/
      periodic.f90
    Utilities/
      utilities.f90
      distributions.f90
      maketable.f90
      math.f90
      random.f90
      sort.f90
	Mpi/
      mpi.f90
	  nompi.f90
	  openmp.f90
    Io/
      io.f90
      header.f90
      textio.f90
      stat.f90

CURRENT FILES (2013-08-21 22:40)

    467 grid.f90
     61 main.f90
    328 params.f90
    157 set_dt.f90
    503 species.f90
    118 units.f90

     27 Boundaries/init_boundaries.f90
    177 Boundaries/periodic.f90

    120 Fields/energy_filter.f90
    157 Fields/laplace.f90
   1511 Fields/maxwell_solver.f90
    272 Fields/maxwell_sources.f90
    615 Fields/stagger_2nd.f90
    450 Fields/stagger_4th.f90
    785 Fields/stagger_6th.f90

     11 Init/init_experiment.f90

    156 Interpolation/interpolation_cic.f90
   1654 Interpolation/interpolation_cubic.f90
    718 Interpolation/interpolation_tsc.f90

     55 Io/debug.f90
    434 Io/dump.f90
    133 Io/header.f90
   1478 Io/io.f90
    772 Io/stat.f90
    253 Io/textio.f90

   3795 Mpi/mpi.f90
   1075 Mpi/nompi.f90
     23 Mpi/omp.f90

    174 Particles/celloperations.f90
    292 Particles/load_balance.f90
    119 Particles/mesh_sources.f90
    170 Particles/move_particles.f90
    404 Particles/particleoperations.f90
    350 Particles/renormalize.f90
    485 Particles/sort.f90

    111 Utilities/bitoperations.f90
    666 Utilities/distributions.f90
    121 Utilities/errors.f90
    359 Utilities/fileoperations.f90
    202 Utilities/math.f90
    545 Utilities/random.f90
    421 Utilities/tests.f90
    133 Utilities/timer.f90
    135 Utilities/trace.f90
  
    168 Experiments/alfven_wave/init_experiment.f90
     70 Experiments/light_wave/init_experiment.f90
     94 Experiments/plasma_oscillations/init_experiment.f90
     75 Experiments/streaming_instability/init_experiment.f90
    100 Experiments/twostream/init_experiment.f90
    
    370 Patch/hybrid/flux_solver.f90
    148 Patch/hybrid/hybrid_update.f90
     57 Patch/hybrid/main.f90
    121 Patch/hybrid/maxwell_solver.f90
    990 Patch/hybrid/mesh_sources.f90
   1152 Patch/hybrid/move_particles.f90
   1259 Patch/hybrid/move_particles_simple.f90
    100 Patch/hybrid/ohms_law.f90
     71 Patch/hybrid/periodic.f90
     74 Patch/hybrid/set_dt.f90
    741 Patch/hybrid/stagger_6th.f90
    
    481 Patch/lean/flux_solver.f90
   1831 Patch/lean/interpolation_cubic.f90
   1511 Patch/lean/maxwell_solver.f90
   1095 Patch/lean/move_particles.f90
    756 Patch/lean/stagger_6th.f90
    
    893 Patch/simple/maxwell_solver.f90
    625 Patch/simple/stagger_6th.f90
    218 Patch/vectorize/move_particles.f90

  34094 totalt
