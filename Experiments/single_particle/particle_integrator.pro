FUNCTION bfield, r
  q = 2*!PI
  return, q * [ (cos(2.*r[0]*q)*cos(2.*r[1]*q + !pi/2)), $
                (sin(2.*r[0]*q)*cos(2.*r[1]*q)),         $
                (sin(r[0]*q)*cos(2.*r[1]*q)) ]
END

FUNCTION bfield_grid, r
  n = 200l
  d = 1./200l
  dh = 0.5*d
  ix  = d*floor(r[0]/d)
  ixs = d*floor((r[0] - dh)/d) + dh
  iy  = d*floor(r[1]/d)
  iys = d*floor((r[1] - dh)/d) + dh
  du = [ d,0.]
  ud = [0., d]
  uu = [ d, d]

  rr = [ix,iys]
  bx00 = (bfield(rr   ))[0]
  bx10 = (bfield(rr+du))[0]
  bx01 = (bfield(rr+ud))[0]
  bx11 = (bfield(rr+uu))[0]

  rr = [ixs,iy]
  by00 = (bfield(rr   ))[1]
  by10 = (bfield(rr+du))[1]
  by01 = (bfield(rr+ud))[1]
  by11 = (bfield(rr+uu))[1]

  rr = [ixs,iys]
  bz00 = (bfield(rr   ))[2]
  bz10 = (bfield(rr+du))[2]
  bz01 = (bfield(rr+ud))[2]
  bz11 = (bfield(rr+uu))[2]

  wx1  = (r[0] - ix)*n  & wx0  = 1. - wx1
  wy1  = (r[1] - iy)*n  & wy0  = 1. - wy1
  wsx1 = (r[0] - ixs)*n & wsx0 = 1. - wsx1
  wsy1 = (r[1] - iys)*n & wsy0 = 1. - wsy1

  ;print, f='(a,6f12.8)', 'R, W  :', [r[0]-ix,r[1]-iy]*n, wx0,wx1,wy0,wy1
  ;print, f='(a,6f12.8)', 'R, WS :', [r[0]-ix,r[1]-iy]*n, wsx0,wsx1,wsy0,wsy1
  bx = (bx00*wx0  + bx10*wx1 )*wsy0 + (bx01*wx0  + bx11*wx1 )*wsy1
  by = (by00*wsx0 + by10*wsx1)*wy0  + (by01*wsx0 + by11*wsx1)*wy1
  bz = (bz00*wsx0 + bz10*wsx1)*wsy0 + (bz01*wsx0 + bz11*wsx1)*wsy1

  return, [bx, by, bz]
END

PRO boris, r, v, dt

  ; accelerate according to b-field
  ;b = bfield(r)
  b = bfield_grid(r)

  
  ;dvdt = [ v(1) * b[2] - v[2] * b[1], $
  ;         v[2] * b[0] - v(0) * b[2], $
  ;         v[0] * b[1] - v(1) * b[0] ]

  ; Boris push
  v1 = fltarr(3)
  BB = dt / 2. * B
  v1[0] = v[0] + (v[1]*BB[2]-v[2]*BB[1])                       ; Rotation: p2 = p1 + p1 x BB
  v1[1] = v[1] + (v[2]*BB[0]-v[0]*BB[2])
  v1[2] = v[2] + (v[0]*BB[1]-v[1]*BB[0])
  s = 2. / (1. + total(BB^2))                                 ; Rescale factor: s = 2 / (1 + h^2)
  v[0] = v[0] + s * (v1[1]*BB[2]-v1[2]*BB[1])                  ; Second boost, rescale and rotation
  v[1] = v[1] + s * (v1[2]*BB[0]-v1[0]*BB[2])                  ; p = p1 + h_E E + s *(p2 x BB)
  v[2] = v[2] + s * (v1[0]*BB[1]-v1[1]*BB[0])

  ;print, f='(a,11f12.8)', 'P, V, B :', r, v, b, bfield(r)
  ;stop

  ;v = v + dvdt * dt

  ; drift position
  r(0) = r(0) + dt * v(0)
  r(1) = r(1) + dt * v(1)

END

v = [0., -1., 0.2]
r = [0.25, 0.25]

t = 0.
dt = 2.5e-3

plot, [0,1],[0,1], /nodata

while t lt 2. do begin
  ; update time
  t += dt

  ; update velocity and position
  boris, r, v, dt

  ; wrap boundaries 
  if r[0] ge 1.0 then r[0] -= 1.0
  if r[1] ge 1.0 then r[1] -= 1.0
  if r[0] lt 0.0 then r[0] += 1.0
  if r[1] lt 0.0 then r[1] += 1.0

  ; update plot
  ;oplot, [r[0]], [r[1]], ps=4, color=150
  oplot, [r[0]], [r[1]], ps=3

endwhile

END
