! Experiments/alfven_wave/init_experiment.f90 $ Date: 2013-09-02 09:40:06 +0200 $
! vim: nowrap
!-----------------------------------------------------------------------!
! Initialize a circularly polarized Alfven wave propagating parallel
! to an ambient magnetic field pointing in the x-direction. At the
! moment we assume that the ions are cold. In the future we should make
! them hot and look at cyclotron damping.
!
! SCALINGS: When changing the total volume and nothing else, wpe should
! remain the same. The total energies should scale with the volume. The
! particle weights remain the same, and therefore the charge densities 
! and current densities should also remain the same.  That, again, should
! mean that the field values remain the same, and that the total EM
! energy therefore indeed should scale with the volume.
!
! The coherent initial current density is the same when the wavelength 
! is shorter, but the divergence of it is larger, so the electric charge
! grows faster.  The coherent charge densities scale inversely with the
! size, while the coherent electric potential scales linearly with the
! size.  The noise charge density, on the other hand, starts out the 
! same, so the initial noise electric field scales with the size, and is
! smaller for a smaller size.

!=======================================================================
! Leave boundaries as they are, so no need to even read a namelist
!=======================================================================
SUBROUTINE init_experiment_boundaries
END SUBROUTINE init_experiment_boundaries

!=======================================================================
SUBROUTINE input (inputfile)
  USE params, only: mfile
  USE dumps, only: dump, dump_set
  implicit none
  character(len=mfile) inputfile
!.......................................................................
  ! Where to find input
  inputfile = 'input.nml'
END SUBROUTINE

!=======================================================================
SUBROUTINE init_experiment

  USE params
  USE grid_m,  ONLY : Bx, By, Bz, g
  USE units,   ONLY : c
  USE species, ONLY : sp, fields, particle
  USE particleoperations, only : global_coordinates, make_particle
  USE dumps,   ONLY : dump, dump_set

  implicit none

  real :: d0, B0, ampl, beta
  integer :: ikx
  logical :: lefthanded, do_cold_start

  real :: kx, kz, k2
  real :: kva, oc, omega, q, m, rho0, va2, w
  real :: hel
  real :: B_hat, u_hat
  real :: cfl, dsmin, vc, va, vph, periods, tstep
  integer :: power
  complex :: phase
  real, dimension (mdim) :: pos
  complex, dimension (mdim) :: ecirc
  integer :: i, isp, ix, iy, iz, np
  real, parameter:: pi = 4.*atan (1.)
  type(particle), pointer :: pa
  character(len=mid) :: id='Experiments/alfven_wave/init_experiment.f90 $Id$'
  namelist /init/ d0, B0, ampl, beta, ikx, lefthanded, do_cold_start
!.......................................................................
  call print_id(id)
  call dump_set ('experiment.dmp')
  ! MUST define for periodic
  periodic = .true.

  ! background number density
  d0 = 1.
  ! background magnetic field
  B0 = 1.
  ! Amplitude of the perturbation
  ampl = 0.1
  ! Plasma beta
  beta = 0.0
  ! Mode number
  ikx  = 1
  ! Left-handed wave?
  lefthanded = .false.
  ! use a regularly spaced distribution of particles ?
  do_cold_start = .false.
  
  rewind(stdin); read (stdin,init)
  if (master) write(params_unit,init)
  if (out_namelists) write(stdout,init)

  ! Components of wave vector
  kx = 2.0*pi*ikx/g%gs(1)

  ! Squared norm
  k2 = kx**2

  ! Fourier amplitude of Lagrangian displacement.
  if (lefthanded) then
    hel = -1.0
  else
    hel = +1.0
  endif

  ! Loop over species
  do isp=1,nspecies

    ! Particle charge
    q = sp(isp)%charge
    ! Particle mass
    m = sp(isp)%mass
    ! Mass density
    rho0 = m*d0
    ! Cyclotron frequency
    oc = q*abs (B0)/m

    ! Alfven speed
    va2 = B0**2/(c%mu0*rho0)
    ! Alfvenic frequency
    kva = sqrt (k2*va2)

    ! Wave frequency
    omega = kva*sqrt (1.0 + (0.5*kva/oc)**2) + 0.5*kva**2/(hel*oc)

    ! Fourier amplitudes
    u_hat = -ampl*sqrt (vA2)*kva/omega
    B_hat = ampl*B0

    ! zyx-loop
    do iz=g%lb(3),g%ub(3)-1
    do iy=g%lb(2),g%ub(2)-1
    do ix=g%lb(1),g%ub(1)-1

      !===========================!
      ! Initialize magnetic field !
      !===========================!

      ! Position
      pos(1) = g%x(ix) + 0.5*g%ds(1)
      ! Phase
      phase = kx*pos(1)

      ! Magnetic field
      Bx(ix,iy,iz) = B0
      By(ix,iy,iz) = B_hat*sin (phase)
      Bz(ix,iy,iz) = B_hat*cos (phase)

      !======================!
      ! Initialize particles !
      !======================!

      ! Density weight
      w = d0/(sp(isp)%ntarget)

      ! Particles/cell
      do i=1,sp(isp)%ntarget
        ! add particle
        call make_particle(isp, ix, iy, iz, (/0.,0.,0./), w)
        np = sp(isp)%np
        pa => sp(isp)%particle(np)
        if (do_cold_start) pa%r(1) = (i-0.5) / sp(isp)%ntarget
        pos = global_coordinates (pa)
        phase = kx*pos (1)
        pa%p(1) = 0.0
        pa%p(2) = u_hat*sin (phase)
        pa%p(3) = u_hat*cos (phase)
      end do

    end do
    end do
    end do
  end do

  if (master) then
    cfl = 1.0
    periods = 6.0
    dsmin = minval (g%ds)
    vc = dsmin*oc/(2.0*pi)
    va = sqrt (va2)
    vph = va*(sqrt (1.0 + (0.5*va/vc)**2) + 0.5*va/vc)
    power = floor (log (cfl*dsmin/vph)/log (2.0))
    tstep = 2.0**power
    print *, 'dt = ', tstep, ", log2 (dt) = ", power
    print *, periods, " periods = ", 2.0*pi*periods/omega, " time units"

    print *, '----------------------------'
    print *, 'B0    :', B0
    print *, 'kx    :', kx
    print *, 'kva   :', kva
    print *, 'v_a   :', abs (B0)/sqrt (c%mu0*rho0)
    print *, 'w_ci  :', oc
    print *, 'Omega :', omega
    print *, '----------------------------'
  endif

END SUBROUTINE init_experiment
