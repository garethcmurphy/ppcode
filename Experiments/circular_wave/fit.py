# -*- coding: utf8 -*-
from __future__ import unicode_literals
import numpy as np
from scipy import optimize
from matplotlib import pyplot as plt
import ppcode
from os import listdir
from re import match

def update (fname, (By_p, Bz_p, uy_p, uz_p)):

  (f, h) = ppcode.rf ('Data/' + fname)

  t = h.time

  By_p, err = optimize.leastsq (By_err, By_p, args = (h.time,))
  Bz_p, err = optimize.leastsq (Bz_err, Bz_p, args = (h.time,))
  uy_p, err = optimize.leastsq (uy_err, uy_p, args = (h.time,))
  uz_p, err = optimize.leastsq (uz_err, uz_p, args = (h.time,))

  f_uy = f.ruy/f.rho
  f_uz = f.ruz/f.rho

  [line.set_ydata (yd) for (line, yd) in zip (lines[0,0], [By (p0, t), f.By])]
  [line.set_ydata (yd) for (line, yd) in zip (lines[0,1], [Bz (p0, t), f.Bz])]
  [line.set_ydata (yd) for (line, yd) in zip (lines[1,0], [uy (p0, t), f_uy])]
  [line.set_ydata (yd) for (line, yd) in zip (lines[1,1], [uz (p0, t), f_uz])]

  return t, h.dt

params = ppcode.Params ()

# Number of grid points
nx, ny, nz = params.grid.n
# Bos size
Lx, Ly, Lz = params.grid.s
# Init parameters
globals ().update (params.init)
# Species parameters
q = params.species.charge
m = params.species.mass
# Vacuum permeability (Where is this to be read from)
mu0 = 1.0

# Grid spacing
dx = Lx/nx
dy = Ly/ny
dz = Lz/nz
# Coordinate arrays
x = dx*np.arange (nx)
y = dy*np.arange (ny)
z = dz*np.arange (nz)

# Wave numbers
kx = 2*np.pi*ikx/Lx
# Squared norm
k2 = kx**2

# Helicity
hel = -1.0 if lefthanded else 1.0

# Circular basis vector
ecirc = np.array ([0, 1, 1j])
# Normalize
ecirc /= np.sqrt (np.dot (ecirc.conj (), ecirc))

# Ambient density
rho0 = m*d0
# Cyclotron frequency
oc = q*abs (b0)/m
# Alfven speed squared
va2 = b0**2/rho0
# Alfvenic frequency
kva = np.sqrt (k2*va2)

# Wave frequency
omega = kva*np.sqrt (1.0 + (0.5*kva/oc)**2) + 0.5*kva**2/(hel*oc)

# Fourier amplitudes
u_hat = ampl*np.sqrt (va2)*(kva/(hel*oc) - omega/kva)
B_hat = ampl*b0

p0 = [1.0, 0.0]

By = lambda p, t: p[0]*B_hat*np.sin (kx*(x + dx/2) - omega*t + p[1])
Bz = lambda p, t: p[0]*B_hat*np.cos (kx*(x + dx/2) - omega*t + p[1])

uy = lambda p, t: p[0]*u_hat*np.sin(kx*x - omega*t + p[1])
uz = lambda p, t: p[0]*u_hat*np.cos(kx*x - omega*t + p[1])

fnames = [f for f in listdir('./Data') if match (r'fields-[0-9]{6}\.dat', f)]
fnames.sort ()

(f, h) = ppcode.rf ('Data/' + fnames[0])

plt.ion ()

plt.clf ()
fig, axes = plt.subplots (ncols = 2, nrows = 2, num = 1,
    sharex = True, sharey = "row")

lines = np.empty (axes.shape, dtype = object)

By_err = lambda p, t: f.By - By (p, t)
Bz_err = lambda p, t: f.Bz - Bz (p, t)
uy_err = lambda p, t: f.ruy/f.rho - uy (p, t)
uz_err = lambda p, t: f.ruz/f.rho - uz (p, t)

By_p, err = optimize.leastsq (By_err, p0, args = (h.time,))
Bz_p, err = optimize.leastsq (Bz_err, p0, args = (h.time,))
uy_p, err = optimize.leastsq (uy_err, p0, args = (h.time,))
uz_p, err = optimize.leastsq (uz_err, p0, args = (h.time,))

lines[0,0] = axes[0,0].plot (x, By (p0, h.time), 'b', x, f.By,        'k')
lines[0,1] = axes[0,1].plot (x, Bz (p0, h.time), 'b', x, f.Bz,        'k')
lines[1,0] = axes[1,0].plot (x, uy (p0, h.time), 'b', x, f.ruy/f.rho, 'k')
lines[1,1] = axes[1,1].plot (x, uz (p0, h.time), 'b', x, f.ruz/f.rho, 'k')

axes[0,0].set_title (r'By')
axes[0,1].set_title (r'Bz')
axes[1,0].set_title (r'uy')
axes[1,1].set_title (r'uz')

fig.set_tight_layout (True)

for ax in axes.flatten (): ax.title.set_y (1.02)

ampl = []
phase = []

for fname in fnames:

  (f, h) = ppcode.rf ('Data/' + fname)

  t = h.time

  By_p, err = optimize.leastsq (By_err, By_p, args = (t,))
  Bz_p, err = optimize.leastsq (Bz_err, Bz_p, args = (t,))
  uy_p, err = optimize.leastsq (uy_err, uy_p, args = (t,))
  uz_p, err = optimize.leastsq (uz_err, uz_p, args = (t,))

  ampl.append (By_p[0])
  phase.append (By_p[1])

  f_uy = f.ruy/f.rho
  f_uz = f.ruz/f.rho

  [line.set_ydata (yd) for (line, yd) in zip (lines[0,0], [By (By_p, t), f.By])]
  [line.set_ydata (yd) for (line, yd) in zip (lines[0,1], [Bz (Bz_p, t), f.Bz])]
  [line.set_ydata (yd) for (line, yd) in zip (lines[1,0], [uy (uy_p, t), f_uy])]
  [line.set_ydata (yd) for (line, yd) in zip (lines[1,1], [uz (uz_p, t), f_uz])]

  plt.draw ()

plt.ioff ()

np.savez ("fit-%d.npz" % h.ntarget, ampl = ampl, phase = phase)
