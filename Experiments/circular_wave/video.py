# -*- coding: utf8 -*-
from __future__ import unicode_literals
import numpy as np
from os import listdir
from re import match
from sys import argv
interactive = True
video = False
image = False
if len (argv) > 1:
  video = (argv[1] == 'video')
  image = (argv[1] == 'image')
  if video or image:
    interactive = False
    from matplotlib import use
    use ('agg')
from matplotlib import pyplot as plt
import ppcode
import re

def update (fname):

  (f, h) = ppcode.rf ('Data/' + fname)

  t = h.time

  [line.set_ydata (yd) for (line, yd) in zip (lines[0,0],[By(t),f.By])]
  [line.set_ydata (yd) for (line, yd) in zip (lines[0,1],[Bz(t),f.Bz])]
  [line.set_ydata (yd) for (line, yd) in zip (lines[1,0],[uy(t),f.ruy/f.rho])]
  [line.set_ydata (yd) for (line, yd) in zip (lines[1,1],[uz(t),f.ruz/f.rho])]

  return t, h.dt

params = ppcode.Params ()

# Number of grid points
nx, ny, nz = params.grid.n
# Bos size
Lx, Ly, Lz = params.grid.s
# Init parameters
globals ().update (params.init)
# Species parameters
q = params.species.charge
m = params.species.mass
# Vacuum permeability (Where is this to be read from)
mu0 = 1.0

# Grid spacing
dx = Lx/nx
dy = Ly/ny
dz = Lz/nz
# Coordinate arrays
x = dx*np.arange (nx)
y = dy*np.arange (ny)
z = dz*np.arange (nz)

# Wave numbers
kx = 2*np.pi*ikx/Lx
# Squared norm
k2 = kx**2

# Helicity
hel = -1.0 if lefthanded else 1.0

# Ambient density
rho0 = m*d0
# Cyclotron frequency
oc = q*abs (b0)/m
# Alfven speed squared
va2 = b0**2/rho0
# Alfvenic frequency
kva = np.sqrt (k2*va2)

# Wave frequency
omega = kva*np.sqrt (1.0 + (0.5*kva/oc)**2) + 0.5*kva**2/(hel*oc)

# Fourier amplitudes
u_hat = ampl*np.sqrt (va2)*(kva/(hel*oc) - omega/kva)
B_hat = ampl*b0

By = lambda t: B_hat*np.sin(kx*(x + dx/2) - omega*t)
Bz = lambda t: B_hat*np.cos(kx*(x + dx/2) - omega*t)

uy = lambda t: u_hat*np.sin(kx*x - omega*t)
uz = lambda t: u_hat*np.cos(kx*x - omega*t)

fnames = [f for f in listdir('./Data') if match (r'fields-[0-9]{6}\.dat', f)]
fnames.sort ()

(f, h) = ppcode.rf ('Data/' + fnames[0])

if interactive: plt.ion ()

plt.clf ()
fig, axes = plt.subplots (ncols = 2, nrows = 2, num = 1,
    sharex = True, sharey = "row")

lines = np.empty (axes.shape, dtype = object)

lines[0,0] = axes[0,0].plot (x, By (h.time), 'b', x, f.By,        'k')
lines[0,1] = axes[0,1].plot (x, Bz (h.time), 'b', x, f.Bz,        'k')
lines[1,0] = axes[1,0].plot (x, uy (h.time), 'b', x, f.ruy/f.rho, 'k')
lines[1,1] = axes[1,1].plot (x, uz (h.time), 'b', x, f.ruz/f.rho, 'k')

axes[0,0].set_title (r'By')
axes[0,1].set_title (r'Bz')
axes[1,0].set_title (r'uy')
axes[1,1].set_title (r'uz')

fig.set_tight_layout (True)

for ax in axes.flatten (): ax.title.set_y (1.02)

if video:

  import matplotlib.animation
  FFMpegWriter = matplotlib.animation.writers['ffmpeg']
  writer = FFMpegWriter (fps = 12, codec = 'libx264',
      extra_args = ['-preset','veryslow','-pix_fmt','yuv420p',
        '-crf','18','-refs','4'])

if image: it = 0

if interactive:

  for fname in fnames:
    print update (fname)
    plt.draw ()

else:

  dpi = 100
  if video:
    with writer.saving (fig, "video.mp4", dpi):
      for fname in fnames:
	print update (fname)
	writer.grab_frame ()
  if image:
    for fname in fnames:
      print update (fname)
      fig.savefig ('Data/image-%06d.png' % it)
      it += 1

if interactive: plt.ioff ()
