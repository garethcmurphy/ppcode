from numpy import *
from os import listdir
from re import match
from matplotlib import pyplot as plt
import ppcode

def unit_vectors (x, y, z):

  # Unit vectors
  e1 = array ([1.0, 0.0, 0.0])
  e2 = array ([0.0, 1.0, 0.0])
  e3 = array ([0.0, 0.0, 1.0])

  # Cylindrical radius
  r = sqrt (x**2 + y**2)

  # Exit if aligned with z-axis
  if (r < finfo (float64).tiny): return (e1, e2, e3)

  # Rotation angle
  angle = arccos (z/sqrt (x**2 + y**2 + z**2))

  # This matrix rotates e3 into (x,y,z)
  cross = zeros ((3, 3))
  cross[2,1] = +y/r
  cross[0,2] = -x/r
  cross[1,2] = -y/r
  cross[2,0] = +x/r

  # Rodrigues' formula
  rot = identity (3) - sin (angle)*cross + (1.0 - cos (angle))*dot (cross, cross)

  # New orthogonal unit vectors
  e1 = rot[:, 0]
  e2 = rot[:, 1]
  e3 = rot[:, 2]

  return (e1, e2, e3)

params = ppcode.Params ()

# Number of grid points
nx, ny, nz = params.grid.n
# Bos size
Lx, Ly, Lz = params.grid.s
# Init parameters
for (k, v) in params.init.items (): exec ("%s = %s" % (k, v))
# Species parameters
q = params.species.charge
m = params.species.mass
# Vacuum permeability (Where is this to be read from)
mu0 = 1.0

# Grid spacing
dx = Lx/nx
dy = Ly/ny
dz = Lz/nz
# Coordinate arrays
x = dx*arange (nx)
y = dy*arange (ny)
z = dz*arange (nz)

# Wave numbers
kx = 2*pi*ikx/Lx
ky = 2*pi*iky/Ly
kz = 2*pi*ikz/Lz
# Ambient density
rho0 = m*d0
# Cyclotron frequency
oc = q*abs (b0)/m

# Get an orthogonal basis (e1, e2, e3),
# where e3 is aligned with the ambient magnetic field
(e1, e2, e3) = unit_vectors (kx*b0, ky*b0, kz*b0)

# Circular unit vectors
ep = sqrt (0.5)*(e1 + 1j*e2)
em = sqrt (0.5)*(e1 - 1j*e2)

# Wave vector
kk = array ([kx, ky, kz])

# Ambient magnetic field vector
bb = abs (b0)*e3

# Fourier amplitude of Lagrangian displacement.
if lefthanded:
  xi_k = ampl*em
else:
  xi_k = ampl*ep

# Determine helicity
hel = vdot (1j*xi_k, cross (xi_k, e3)).real/vdot (xi_k, xi_k).real

# Scalar product of wave vector and Alfven velocity
kva = dot (kk, bb)/sqrt (mu0*rho0)
# Wave frequency
omega = kva*(sqrt(1.0 + (0.5*kva/oc)**2) + 0.5*kva/(hel*oc))

# Fourier amplitudes of magnetic field and ion fluid velocity
B_k = 1j*dot (kk, bb)*xi_k
u_k = -(1j*omega + kva**2/(1j*hel*oc))*xi_k
E_k = 1j*omega*cross (xi_k, bb)

phi = lambda t: kx*x - omega*t
BB = lambda t: [(B_k[i]*exp (1j*(phi (t) + 0.5*kx*dx))).real for i in range (3)]
uu = lambda t: [(u_k[i]*exp (1j*phi (t))).real for i in range (3)]
EE = lambda t: [(E_k[i]*exp (1j*phi (t))).real for i in range (3)]

plt.ion ()
plt.clf ()
fig, axes = plt.subplots (num = 1, ncols = 2, nrows = 3,
		sharex = True, sharey = "row")

files = [f for f in listdir('./Data') if match(r'fields-[0-9]{6}\.dat', f)]
files.sort ()

(f, h) = ppcode.rf ('Data/' + files.pop (0))

plots = empty (axes.shape, dtype = object)

t = h.time

plots[0,0] = axes[0,0].plot (x, BB(t)[1], 'k', x, f.By, 'r')
plots[0,1] = axes[0,1].plot (x, BB(t)[2], 'k', x, f.Bz, 'r')
          
plots[1,0] = axes[1,0].plot (x, uu(t)[1], 'k', x, f.vy, 'r')
plots[1,1] = axes[1,1].plot (x, uu(t)[2], 'k', x, f.vz, 'r')
          
plots[2,0] = axes[2,0].plot (x, EE(t)[1], 'k', x, f.Ey, 'r')
plots[2,1] = axes[2,1].plot (x, EE(t)[2], 'k', x, f.Ez, 'r')

fig.set_tight_layout (True)

from time import sleep

for fname in files:
  (f, h) = ppcode.rf ('Data/' + fname)

  t = h.time
  print fname, t

  [p.set_ydata (yd) for (p, yd) in zip (plots[0,0],(BB(t)[1],f.By))]
  [p.set_ydata (yd) for (p, yd) in zip (plots[0,1],(BB(t)[2],f.Bz))]
                                                               
  [p.set_ydata (yd) for (p, yd) in zip (plots[1,0],(uu(t)[1],f.vy))]
  [p.set_ydata (yd) for (p, yd) in zip (plots[1,1],(uu(t)[2],f.vz))]
                                                               
  [p.set_ydata (yd) for (p, yd) in zip (plots[2,0],(EE(t)[1],f.Ey))]
  [p.set_ydata (yd) for (p, yd) in zip (plots[2,1],(EE(t)[2],f.Ez))]

  plt.draw ()

plt.ioff ()
