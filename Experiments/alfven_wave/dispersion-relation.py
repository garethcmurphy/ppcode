from numpy import *
from scipy.special import wofz
from scipy.optimize import newton
from os import listdir
from re import match
from matplotlib import pyplot as plt
import ppcode
from ppcode._fortran import FortranFile

class DispersionRelation ():
  """
  This class solves the transverse part of the Vlasov-fluid
  dispersion relation for waves propagating along the background
  magnetic field. The solution is obtained as follows. We start
  with the solution of the cold ion dispersion relation. With
  that as initial guess, we seek a root for some reasonably small
  beta (default is 1e-12). We then increase beta -- all the time
  using the last root obtained as initial guess for the new one --
  until the desired beta is reached.
  """
  def __init__ (self, kva = 1.0, oc = 1.0, hel = 1.0):
    # Alfvenic frequency
    self.kva = kva
    # Cyclotron frequency
    self.oc = oc
    # Helicity
    self.hel = hel
    # Cold ion frequency
    self.guess = kva*(sqrt(1.0 + (0.5*kva/oc)**2) + 0.5*kva/(hel*oc))

  def det (self, omega, beta):
    # Thermal frequency
    kvt = self.kva*sqrt (beta)
    # Reduced frequency
    zeta = (omega + hel*oc)/kvt
    # Plasma response function
    U = lambda z: -1j*sqrt (pi)*wofz (z)
    # Dispersion relation
    return kva**2 + oc*oc*(omega/kvt)*U (zeta) - hel*oc*omega

  def solve (self, beta, start = 1e-12, n = 128):
    betas = logspace (log10 (start), log10 (beta), n)
    guess = self.guess
    for beta in betas:
      omega = newton (self.det, guess, args = (beta,))
      guess = omega
    return omega

if __name__ == '__main__':

  params = ppcode.Params ('./input.nml')

  # Bos size
  Lx, Ly, Lz = params.grid.s
  # Init parameters
  for (k, v) in params.init.items (): exec ('%s = %s' % (k, v))
  for var in ('b0','d0','ikx','iky','ikz','lefthanded','beta'):
    exec ('%s = params.init.%s' % (var,var))
  # Species parameters
  q = params.species.charge
  m = params.species.mass
  # Vacuum permeability (Where is this to be read from)
  mu0 = 1.0

  # Wave numbers
  kx = 2*pi*ikx/Lx
  ky = 2*pi*iky/Ly
  kz = 2*pi*ikz/Lz
  # Ambient density
  rho0 = m*d0
  # Cyclotron frequency
  oc = q*abs (b0)/m

  # Helicity
  hel = 1.0
  if lefthanded: hel = -1.0

  # Scalar product of wave vector and Alfven velocity
  kva = sqrt (kx**2 + ky**2 + kz**2)*b0

  # Solve the dispersion relation
  drel = DispersionRelation (kva = kva, oc = oc, hel = hel)
  omega = drel.solve (beta)

  numbers = (kva, oc, hel, beta, omega.real, omega.imag)
  print "kva = %g, oc = %g, hel = %g, beta = %g, omega = (%g,%g)" % numbers

  f = FortranFile ('omega.dat', 'w')
  f.write_record (array (numbers, dtype = float32))
  f.close ()
