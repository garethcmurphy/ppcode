! Experiments/alfven_wave/init_experiment.f90 $ Date: 2013-09-02 09:40:06 +0200 $
! vim: nowrap
!-----------------------------------------------------------------------!
! Initialize a circularly polarized Alfven wave propagating parallel
! to an ambient magnetic field pointing in the x-direction. At the
! moment we assume that the ions are cold. In the future we should make
! them hot and look at cyclotron damping.
!
! SCALINGS: When changing the total volume and nothing else, wpe should
! remain the same. The total energies should scale with the volume. The
! particle weights remain the same, and therefore the charge densities 
! and current densities should also remain the same.  That, again, should
! mean that the field values remain the same, and that the total EM
! energy therefore indeed should scale with the volume.
!
! The coherent initial current density is the same when the wavelength 
! is shorter, but the divergence of it is larger, so the electric charge
! grows faster.  The coherent charge densities scale inversely with the
! size, while the coherent electric potential scales linearly with the
! size.  The noise charge density, on the other hand, starts out the 
! same, so the initial noise electric field scales with the size, and is
! smaller for a smaller size.

!=======================================================================
! Leave boundaries as they are, so no need to even read a namelist
!=======================================================================
SUBROUTINE init_experiment_boundaries
END SUBROUTINE init_experiment_boundaries

!=======================================================================
SUBROUTINE input (inputfile)
  USE params, only: mfile
  USE dumps, only: dump, dump_set
  implicit none
  character(len=mfile) inputfile
!.......................................................................
  ! Where to find input
  inputfile = 'input.nml'
END SUBROUTINE

!=======================================================================
SUBROUTINE init_experiment

  USE params
  USE grid_m,  ONLY : Bx, By, Bz, g
  USE units,   ONLY : c
  USE species, ONLY : sp, fields, particle
  USE particleoperations, only : global_coordinates, make_particle
  USE dumps,   ONLY : dump, dump_set

  implicit none

  INTERFACE 
    FUNCTION cross_product (a, b)
      complex, dimension (3) :: cross_product
      complex, dimension (3), intent (in) :: a
      real, dimension (3), intent (in) :: b
    END FUNCTION cross_product
    FUNCTION randn ()
      real :: randn
    END FUNCTION randn
  END INTERFACE

  real :: d0, B0, ampl, beta
  integer :: ikx, iky, ikz
  logical :: lefthanded, do_cold_start

  real :: kx, ky, kz
  real :: hel, phi, w, q, m, rho0, oc, kva, va, vt
  real :: kva1, oc1, hel1, beta1, omega_i
  complex :: omega, phase
  logical :: lerr
  real, dimension (mdim)  :: kk, BB, pos
  complex, parameter      :: ic = cmplx(0., 1.)
  real, dimension (mdim) :: e1, e2, e3
  complex, dimension (mdim) :: ep, em
  complex, dimension (mdim) :: xi_k, B_k, u_k
  integer :: i, isp, ix, iy, iz, np
  real, parameter:: pi = 4.*atan (1.)
  type(particle), pointer :: pa
  character(len=mid) :: id='Experiments/alfven_wave/init_experiment.f90 $Id$'
  namelist /init/ d0, B0, ampl, beta, ikx, iky, ikz, lefthanded, do_cold_start
!.......................................................................
  call print_id(id)
  call dump_set ('experiment.dmp')
  ! MUST define for periodic
  periodic = .true.

  ! background number density
  d0 = 1.
  ! background magnetic field
  B0 = 1.
  ! Amplitude of the perturbation
  ampl = 0.1
  ! Plasma beta
  beta = 0.0
  ! Mode number
  ikx  = 1
  iky  = 0
  ikz  = 0
  ! Left-handed wave?
  lefthanded = .false.
  ! use a regularly spaced distribution of particles ?
  do_cold_start = .false.
  
  rewind(stdin); read (stdin,init)
  if (master) write(params_unit,init)
  if (out_namelists) write(stdout,init)

  ! Components of wave vector
  kx = 2.0*pi*ikx/g%gs(1)
  ky = 2.0*pi*iky/g%gs(2)
  kz = 2.0*pi*ikz/g%gs(3)

  ! Get an orthogonal basis (e1, e2, e3),
  ! where e3 is aligned with the ambient magnetic field
  call unit_vectors (kx*B0, ky*B0, kz*B0, e1, e2, e3)

  ! Circular unit vectors
  ep = sqrt (0.5)*(e1 + ic*e2)
  em = sqrt (0.5)*(e1 - ic*e2)

  ! Wave vector
  kk = (/kx, ky, kz/)

  ! Ambient magnetic field vector
  BB = abs (B0)*e3

  ! Fourier amplitude of Lagrangian displacement.
  if (lefthanded) then
    xi_k = ampl*em
  else
    xi_k = ampl*ep
  endif

  ! Determine helicity
  hel = dot_product (ic*xi_k, cross_product (xi_k, e3))/dot_product (xi_k, xi_k)

  ! Loop over species
  do isp=1,nspecies

    ! Particle charge
    q = sp(isp)%charge
    ! Particle mass
    m = sp(isp)%mass
    ! Mass density
    rho0 = m*d0
    ! Cyclotron frequency
    oc = q*abs (B0)/m

    ! Alfven speed
    va = abs (B0)/sqrt (c%mu0*rho0)
    ! Alfvenic frequency
    kva = dot_product (kk, va*e3)
    ! Thermal velocity
    vt = va*sqrt (0.5*beta)

    ! Wave frequency
    if (beta > 0.0) then
      open (1, file = "omega.dat", form = "unformatted")
      read (1) kva1, oc1, hel1, beta1, omega_r, omega_i
      close (1)
      lerr = .false.
      if (kva1 /= kva) then
        print *, "kva doesn't match";
        lerr = .true.
      endif
      if (oc1 /= oc) then
        print *, "oc doesn't match";
        lerr = .true.
      endif
      if (hel1 /= hel) then
        print *, "hel doesn't match";
        lerr = .true.
      endif
      if (beta1 /= beta) then
        print *, "beta doesn't match";
        lerr = .true.
      endif
      if (lerr) then
        call stop_program ("init_experiment", "Error! 'omega.dat' is out of date")
      endif
      omega = omega_r + ic*omega_i
    else
      omega = kva*(sqrt(1. + (.5*kva/oc)**2) + .5*kva/(hel*oc))
    endif

    ! vt

    ! Fourier amplitudes of magnetic field and ion fluid velocity
    B_k = ic*dot_product (kk, BB)*xi_k
    u_k = -(ic*omega + kva**2/(ic*hel*oc))*xi_k

    ! zyx-loop
    do iz=g%lb(3),g%ub(3)-1
    do iy=g%lb(2),g%ub(2)-1
    do ix=g%lb(1),g%ub(1)-1

      ! Initialize magnetic field

      ! Phase
      phi = kx*(g%x(ix) + 0.5*g%ds(1)) &
          + ky*(g%y(iy) + 0.5*g%ds(2)) &
          + kz*(g%z(iz) + 0.5*g%ds(3))
      phase = exp (ic*phi)

      Bx(ix,iy,iz) = BB(1) + real (B_k(1)*phase)
      By(ix,iy,iz) = BB(2) + real (B_k(2)*phase)
      Bz(ix,iy,iz) = BB(3) + real (B_k(3)*phase)

      ! Initialize particles

      ! Density weight
      w = d0/(sp(isp)%ntarget)

      ! Particles/cell
      do i=1,sp(isp)%ntarget
        ! add particle
        call make_particle(isp, ix, iy, iz, (/0.,0.,0./), w)
        np = sp(isp)%np
        pa => sp(isp)%particle(np)
        if (do_cold_start) pa%r(1) = (i-0.5) / sp(isp)%ntarget
        pos = global_coordinates (pa)
        phi = kx*pos(1) + ky*pos(2) + kz*pos(3)
        phase = exp (ic*phi)
        pa%p(1) = real (u_k(1)*phase) + vt*randn ()
        pa%p(2) = real (u_k(2)*phase) + vt*randn ()
        pa%p(3) = real (u_k(3)*phase) + vt*randn ()
      end do

    end do
    end do
    end do
  end do

  if (master) then
    print *, '----------------------------'
    print *, 'B0         :', B0
    print *, 'kx, ky, kz :', kx, ky, kz
    print *, 'kva        :', kva
    print *, 'v_a        :', abs (B0)/sqrt (c%mu0*rho0)
    print *, 'w_ci       :', oc
    print *, 'Omega      :', omega
    print *, '----------------------------'
  endif

END SUBROUTINE init_experiment

SUBROUTINE unit_vectors (x, y, z, e1, e2, e3)

  real, intent (in) :: x, y, z
  real, dimension (3), intent (out) :: e1, e2, e3

  real, dimension (3, 3) :: rot, cross, id
  real, dimension (3) :: axis
  real :: r, angle

  ! Unit vectors
  e1 = (/1, 0, 0/)
  e2 = (/0, 1, 0/)
  e3 = (/0, 0, 1/)

  ! Cylindrical radius
  r = sqrt (x**2 + y**2)

  ! Exit if aligned with z-axis
  if (r < tiny (r)) return

  ! Identity matrix
  id (:, 1) = e1
  id (:, 2) = e2
  id (:, 3) = e3

  ! Rotation angle
  angle = acos (z/sqrt (x**2 + y**2 + z**2))

  ! This matrix rotates e3 into (x,y,z)
  cross = 0
  cross(3,2) = +y/r
  cross(1,3) = -x/r
  cross(2,3) = -y/r
  cross(3,1) = +x/r

  ! Rodrigues' formula
  rot = id - sin (angle)*cross + (1 - cos (angle))*matmul (cross, cross)

  ! New orthogonal unit vectors
  e1 = rot (:, 1)
  e2 = rot (:, 2)
  e3 = rot (:, 3)

END SUBROUTINE

FUNCTION cross_product (a, b)
  complex, dimension (3) :: cross_product
  complex, dimension (3), intent (in) :: a
  real, dimension (3), intent (in) :: b
  cross_product(1) = a(2)*b(3) - a(3)*b(2)
  cross_product(2) = a(3)*b(1) - a(1)*b(3)
  cross_product(3) = a(1)*b(2) - a(2)*b(1)
END FUNCTION cross_product

FUNCTION randn ()

  ! Adapted from the following Fortran 77 code
  !      ALGORITHM 712, COLLECTED ALGORITHMS FROM ACM.
  !      THIS WORK PUBLISHED IN TRANSACTIONS ON MATHEMATICAL SOFTWARE,
  !      VOL. 18, NO. 4, DECEMBER, 1992, PP. 434-435.

  ! See http://calgo.acm.org/

  real :: randn

  real, parameter :: s = 0.449871, t = -0.386595, a = 0.19600, b= 0.25472
  real, parameter :: r1 = 0.27597, r2 = 0.27846

  real :: q, u, v, x, y

  do
    ! Generate P = (u,v) uniform in rectangle enclosing acceptance region
    call random_number (u)
    call random_number (v)

    v = 1.7156 * (v - 0.5)

    ! Evaluate the quadratic form
    x = u - s
    y = abs(v) - t
    q = x**2 + y*(a*y - b*x)

    ! Accept P if inside inner ellipse
    if (q < r1) exit
  end do

  ! Return ratio of P's coordinates as the normal deviate
  randn = v/u

END FUNCTION randn
