
:-----------------------------------------------------------------------
; to check for boundary errors
f=rf(0)
plot,shift(hrms(f.ex,/zv),20)
plot,shift(hrms(f.ey,/zv),20)
plot,shift(hrms(f.ez,/zv),20)

:-----------------------------------------------------------------------
; to plot the time evolution
n=50 & s=sin(2.*!pi*findgen(n)/n)           ; projection function
nt=40 & a=fltarr(nt)
for i=1,nt-1 do begin & f=rf(i) & a(i)=total(s*haver(f.ez,/zv))
plot,a

;------------------------------------------------------------------------
; animate phase space plots in twostream experiments
for i=5,75,5 do begin p=rp(i,data='.') & plot,(p.r[*,2,0]+500)mod 50,p.p[*,2,0],psym=3,tit=i,xst=3 & wait,1
END
