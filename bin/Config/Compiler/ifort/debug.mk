# $Id: debug.mk,v 1.1 2013/08/20 15:43:11 aake Exp $

OPT  = -O1

VERS = $(shell ifort -v 2>&1 | sed -e 's/ //g')
ifeq ($(VERS),Version9.1)
  DBG  = -g -traceback -fpe0 -check all -CB -ftrapuv -u
else
  DBG  = -g -traceback -fpe0 -check all -CB -ftrapuv -u -fp-stack-check
endif

# No speed impact:
# -g -traceback -fpe0 -u

# Small speed impact
# -ftrapuv

# Large speed impact
# -check all -CB
