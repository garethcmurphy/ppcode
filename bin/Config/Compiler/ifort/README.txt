lynx, 2007-09-30
----------------

Using ifort 10.0.023

options      compile      fields/particles
------------------------------------------

OPT=-O1       0:05          3.87/2.65
OPT=-O2       0:15          3.90/2.67
OPT=-O3       0:15          3.90/2.67
OPTS=fast     1:09          3.83/2.63

The execution time differences are probably not significant
