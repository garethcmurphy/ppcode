# Compiler optimizations
OPT = -O3 -fno-alias -fno-fnalias -stack_temps -align all -pad -ftz -IPF_fma
OPT = -O1 -IPF_fma
OPT = -O3

# Hardware tuning
#HW  = -tpp2 -assume byterecl
HW  = -assume byterecl

# Parallelization
PAR = -openmp
PAR =

# Debug options (-g -traceback has no speed penalty on ia64)
DBG = #-g -traceback -C
DBG = -g -traceback

LIBS = -lmpi
