# $Id: Makefile,v 1.1 2013/08/20 15:43:11 aake Exp $

# Allow MPI choice to be made based in Host/, Arch/, or User/
ifndef MPI
  MPI = mpi
endif

# Command name
IFORT = ifort
ifeq ($(MPI),mpi)
  IFORT = mpif90
endif

FC  = $(IFORT) -fpp -module $(BUILD)
FV  = $(IFORT) -v 2>&1
LD  = $(IFORT) -module $(BUILD)

# Option -O3 gives quite a boost, but takes much longer to compile. -xO for SSE3 on opteron.
OPT = -O2
HW  = -vec_report0 -assume byterecl -fltconsistency -fp-model strict

# For now
PAR =

DBG = #-traceback -check bounds # for Intel 8.1
DBG = #-g -traceback -C
DBG = -g -traceback
