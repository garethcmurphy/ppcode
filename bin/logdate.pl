#! /usr/bin/perl

# Get latest submission date
$file = $ARGV[0];
open $pipe, "-|", "git log -- $file";
while (<$pipe>) {
        last if (/^Date:/);
}
chomp;
s/Date: */Date: /;
$date=$_;
$str = "$file \$ $date \$";

# Substitute $Date$ in stdin
while (<STDIN>) {
        s/\$Date\$/$str/;
        s/\$ $file *Id.*\$/$str/;
        s/\$ $file *Date.*\$/$str/;
        s/$file *\$Id.*\$/$str/;
        s/\$Id.*\$/$str/;
        print;
}
