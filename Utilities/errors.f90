! $Id$
! vim: nowrap
!=======================================================================
SUBROUTINE assert(condition,name,text)
  USE params, only: master, stdall, hl, rank
  implicit none
  logical:: condition
  character(len=*):: name,text
  character(len=16):: fmt='(80x,a,i7.7)'
  if (condition) return
  write (stdall,fmt) hl(1:35),rank
  write (stdall,fmt) 'ASSERT FAILED IN ROUTINE '//name
  write (stdall,fmt) text
  write (stdall,fmt) hl(1:35)
  call flush(stdall)
END SUBROUTINE assert

!=======================================================================
SUBROUTINE warning_all_nodes(name,text)
  USE params, only : master, stdall, hl
  USE debug,  only : warnings, maxwarn
  implicit none
  character(len=*):: name,text
  character(len=16):: fmt='(80x,a)'
  warnings = warnings+1
  if (warnings > maxwarn) call error(name,text)
  write (stdall,fmt) hl(1:35)
  if (name .ne. 'noroutine') write (stdall,fmt) 'WARNING FROM ROUTINE '//name
  write (stdall,fmt) text
  write (stdall,fmt) hl(1:35)
  call flush(stdall)
END SUBROUTINE warning_all_nodes

!=======================================================================
SUBROUTINE warning(name,text)
  USE params, only : master, stdout, hl
  USE debug,  only : warnings, maxwarn
  implicit none
  character(len=*):: name,text
  character(len=16):: fmt='(80x,a)'
  warnings = warnings+1
  if (warnings > maxwarn) call error(name,text)
  if (master) then
    write (stdout,fmt) hl(1:35)
    if (name .ne. 'noroutine') write (stdout,fmt) 'WARNING FROM ROUTINE '//name
    write (stdout,fmt) text
    write (stdout,fmt) hl(1:35)
    call flush(stdout)
  end if
END SUBROUTINE warning

!=======================================================================
SUBROUTINE warning_any(err,name,text)
  implicit none
  logical err, any_mpi
  character(len=*):: name,text
  if (any_mpi(err)) then
    call warning(name,text)
  end if
END SUBROUTINE warning_any

!=======================================================================
SUBROUTINE error_all_nodes(name,text)
  USE params, only : stdall, hl, rank
  implicit none
  character(len=*):: name,text
  character(len=16):: fmt='(8x,a,i7.7)'
  write (stdall,fmt) hl(1:35)
  write (stdall,fmt) 'ERROR FROM ROUTINE '//name//' on rank ',rank
  write (stdall,fmt) text
  write (stdall,fmt) hl(1:35)
  call flush(stdall)
#ifndef __xlc__
  call sleep(1)
#endif
  call Finalize_MPI(.true.)   ! this REQUIRES that all nodes call error
END SUBROUTINE error_all_nodes

!=======================================================================
SUBROUTINE error(name,text)
  USE params, only : master, stdout, hl
  implicit none
  character(len=*):: name,text
  character(len=16):: fmt='(8x,a)'
  if (master) then
    write (stdout,fmt) hl(1:35)
    write (stdout,fmt) 'ERROR FROM ROUTINE '//name
    write (stdout,fmt) text
    write (stdout,fmt) hl(1:35)
    call flush(stdout)
  end if
#ifndef __xlc__
  call sleep(1)
#endif
  call stop_program(.false.) ! make full stop without deallocation or anything
END SUBROUTINE error

!=======================================================================
SUBROUTINE error_any(err,name,text)
  implicit none
  logical err, any_mpi
  character(len=*):: name,text
  if (any_mpi(err)) then
    call error(name,text)
  end if
END SUBROUTINE error_any

!=======================================================================
LOGICAL FUNCTION is_nan(a)
  implicit none
  real a
  is_nan = a.ne.a
  return
END FUNCTION

!=======================================================================
LOGICAL FUNCTION is_nan_r8(a)
  implicit none
  real(kind=8) a
  is_nan_r8 = a.ne.a
  return
END FUNCTION
