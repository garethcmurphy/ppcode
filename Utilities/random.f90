! Utilities/random.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------
! A C-program for MT19937, with initialization improved 2002/1/26.
! Before using, initialize the state by using init_genrand(seed)
module random
  USE params, only : random_unit
  implicit none
  intrinsic bit_size
  private
  public  :: init_genrand
  public  :: genrand_int32
  public  :: hbs,wr,wi,n,mti,mt,uiadd,uimlt,uisub
  integer,  parameter :: verbose_io = 0
  integer,  parameter :: intg = selected_int_kind( 9 )
  integer,  parameter :: long = selected_int_kind( 18 )
  integer,  parameter :: flot = selected_real_kind( 6, 37 )
  integer,  parameter :: dobl = selected_real_kind( 15, 307 )
  integer,  parameter :: wi = intg
  integer,  parameter :: wl = long
  integer,  parameter :: wr = dobl
  integer( kind = wi ), parameter :: n = 624_wi                         ! Period parameters
  integer( kind = wi ), parameter :: m = 397_wi
  integer( kind = wi ), parameter :: hbs = bit_size( n ) / 2_wi
  integer( kind = wi ), parameter :: qbs = hbs / 2_wi
  integer( kind = wi ), parameter :: tbs = 3_wi * qbs
  integer( kind = wi ), public    :: global_iz_offset
  integer( kind = wi ), public    :: lb, ub                             ! bounds for local stream
  integer( kind = wi ), dimension(:,:), allocatable, target :: mt       ! Array for the state vec
  integer( kind = wi ), dimension(:),   allocatable, target :: mti      ! mt[N] is not init
contains
!-----------------------------------------------------------------------
elemental function uiadd( a, b ) result( c )
  implicit none
  intrinsic ibits, ior, ishft
  integer( kind = wi ), intent( in )  :: a, b
  integer( kind = wi )  :: c
  integer( kind = wi )  :: a1, a2, b1, b2, s1, s2
  a1 = ibits( a, 0, hbs )
  a2 = ibits( a, hbs, hbs )
  b1 = ibits( b, 0, hbs )
  b2 = ibits( b, hbs, hbs )
  s1 = a1 + b1
  s2 = a2 + b2 + ibits( s1, hbs, hbs )
  c  = ior( ishft( s2, hbs ), ibits( s1, 0, hbs ) )
end function uiadd
!-----------------------------------------------------------------------
elemental function uisub( a, b ) result( c )
  implicit none
  intrinsic ibits, ior, ishft
  integer( kind = wi ), intent( in )  :: a, b
  integer( kind = wi )  :: c
  integer( kind = wi )  :: a1, a2, b1, b2, s1, s2
  a1 = ibits( a, 0, hbs )
  a2 = ibits( a, hbs, hbs )
  b1 = ibits( b, 0, hbs )
  b2 = ibits( b, hbs, hbs )
  s1 = a1 - b1
  s2 = a2 - b2 + ibits( s1, hbs, hbs )
  c  = ior( ishft( s2, hbs ), ibits( s1, 0, hbs ) )
end function uisub
!-----------------------------------------------------------------------
elemental function uimlt( a, b ) result( c )
  implicit none
  intrinsic ibits, ior, ishft
  integer( kind = wi ), intent( in )  :: a, b
  integer( kind = wi )  :: c
  integer( kind = wi )  :: a0, a1, a2, a3
  integer( kind = wi )  :: b0, b1, b2, b3
  integer( kind = wi )  :: p0, p1, p2, p3
  a0 = ibits( a, 0, qbs )
  a1 = ibits( a, qbs, qbs )
  a2 = ibits( a, hbs, qbs )
  a3 = ibits( a, tbs, qbs )
  b0 = ibits( b, 0, qbs )
  b1 = ibits( b, qbs, qbs )
  b2 = ibits( b, hbs, qbs )
  b3 = ibits( b, tbs, qbs )
  p0 = a0 * b0
  p1 = a1 * b0 + a0 * b1 + ibits( p0, qbs, tbs )
  p2 = a2 * b0 + a1 * b1 + a0 * b2 + ibits( p1, qbs, tbs )
  p3 = a3 * b0 + a2 * b1 + a1 * b2 + a0 * b3 + ibits( p2, qbs, tbs )
  c  = ior( ishft( p1, qbs ), ibits( p0, 0, qbs ) )
  c  = ior( ishft( p2, hbs ), ibits( c, 0, hbs ) )
  c  = ior( ishft( p3, tbs ), ibits( c, 0, tbs ) )
end function uimlt
!-----------------------------------------------------------------------
subroutine init_genrand( ns, s ) ! initializes mt[N] with a seed
  USE params, only : mpi, rank, mfile
  USE grid_m,   only : g
  implicit none
  intrinsic iand, ishft, ieor, ibits
  integer,              intent( in )                 :: ns
  integer( kind = wi ), intent( in ), dimension(ns)  :: s
  integer( kind = wi )                               :: i, is, go, scr
  integer( kind = wi ), parameter                    :: mult_a = 1812433253_wi !z'6C078965'
  character(len=mfile)                               :: file
  go = global_iz_offset + mpi%offset(3)
  lb = go + merge(g%lb(3)  ,     1, mpi%me(3) > 0         )    ! lb in local generator
  ub = go + merge(g%ub(3)-1,g%n(3), mpi%me(3) < mpi%n(3)-1)    ! ub in local generator
  allocate(mti(lb:ub),mt(n,lb:ub))
  mti = n + 1_wi
  do is = lb, ub
    mt(1,is) = ibits( s(is), 0, 32 )
    do i = 2, n, 1
      scr = ieor( mt(i-1,is), ishft( mt(i-1,is), -30 ) )
      scr = uimlt( scr, mult_a )
      scr = uiadd( scr, uisub( i, 1_wi ) )
      ! See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier.
      ! In the previous versions, MSBs of the seed affect
      ! only MSBs of the array mt[].
      ! 2002/01/09 modified by Makoto Matsumoto
      mt(i,is) = ibits( scr, 0, 32 )
      ! for >32 bit machines
    end do
  end do
                               write(file,'(a,i6.6,a)') 'random-',rank,'.dat'
                               if (verbose_io > 0) open(unit=random_unit,file=trim(file),form='formatted')
end subroutine init_genrand
!-----------------------------------------------------------------------
function genrand_int32( iiy, local_iz ) result( y ) ! generates a random nr on [0,0xffffffff]-interval
  USE params, only : dbg, stdout, hl, mpi, rank
  USE grid_m,   only : g
  implicit none
  intrinsic iand, ishft, ior, ieor, btest, ibset, mvbits
  integer, intent( in ) :: iiy, local_iz
  integer               :: iz
  integer( kind = wi )  :: y
  integer( kind = wi )  :: kk
  integer( kind = wi ), parameter :: temper_a = -1658038656_wi ! z'9D2C5680'
  integer( kind = wi ), parameter :: temper_b =  -272236544_wi ! z'EFC60000'
  integer( kind = wi ), parameter :: matrix_a = -1727483681_wi ! z'9908b0df'
  integer( kind = wi ), parameter :: matrix_b = 0_wi           ! z'0'
  iz = local_iz + mpi%offset(3) + global_iz_offset
  if ((iz-lb)*(ub-iz) .lt. 0) then
    write(stdout,'(a,3i8,a,3i8)') 'MTRAN: mpi%me(3), liz, offset:', mpi%me(3), local_iz, g%offset,' lb, iz, ub :', lb, iz, ub
    if (dbg .eq. 0) then
      call warning('genrand_int32 (mtran)','iz out of bounds. Reset to lb or ub')
    else
      call   error('genrand_int32 (mtran)','iz out of bounds, bailing out (dbg > 0)')
    endif
    iz = merge(lb, ub, iz .lt. lb)
  endif
  if ( mti(iz) > n ) then ! generate N words at one time
    do kk = 1, n-m, 1
      y = ibits( mt(kk+1,iz), 0, 31 )
      call mvbits( mt(kk,iz), 31, 1, y, 31 )
      if ( btest( y, 0 ) ) then
        mt(kk,iz) = ieor( ieor( mt(kk+m,iz), ishft( y, -1 ) ), matrix_a )
      else
        mt(kk,iz) = ieor( ieor( mt(kk+m,iz), ishft( y, -1 ) ), matrix_b )
      end if
    end do
    do kk = n-m+1, n-1, 1
      y = ibits( mt(kk+1,iz), 0, 31 )
      call mvbits( mt(kk,iz), 31, 1, y, 31 )
      if ( btest( y, 0 ) ) then
        mt(kk,iz) = ieor( ieor( mt(kk+m-n,iz), ishft( y, -1 ) ), matrix_a )
      else
        mt(kk,iz) = ieor( ieor( mt(kk+m-n,iz), ishft( y, -1 ) ), matrix_b )
      end if
    end do
    y = ibits( mt(1,iz), 0, 31 )
    call mvbits( mt(n,iz), 31, 1, y, 31 )
    if ( btest( y, 0 ) ) then
      mt(kk,iz) = ieor( ieor( mt(m,iz), ishft( y, -1 ) ), matrix_a )
    else
      mt(kk,iz) = ieor( ieor( mt(m,iz), ishft( y, -1 ) ), matrix_b )
    end if
    mti(iz) = 1_wi
  end if
  y = mt(mti(iz),iz)
  mti(iz) = mti(iz) + 1_wi
  ! Tempering
  y = ieor( y, ishft( y, -11) )
  y = ieor( y, iand( ishft( y, 7 ), temper_a ) )
  y = ieor( y, iand( ishft( y, 15 ), temper_b ) )
  y = ieor( y, ishft( y, -18 ) )
                               if (verbose_io > 0) write(random_unit,*) iz, y
end function genrand_int32
!-----------------------------------------------------------------------
end module random
!-----------------------------------------------------------------------
SUBROUTINE get_random_slice(pos,ns,sl1,sl2)
  USE params, only : mpi, rank
  USE random
  implicit none
  integer, intent(in)      :: pos, ns
  integer                  :: sl1
  integer, dimension(1:ns) :: sl2
  integer                  :: go
  integer( kind = wi ), dimension(:,:), allocatable, target :: smt      ! Array for the state vec
  integer( kind = wi ), dimension(:),   allocatable, target :: smti     ! mt[N] is not init
  if (pos .eq. 1) then
    go = lb
  else
    go = ub
  endif
  if (ns < n) then
    print *, ' Rank, mpi%me :', rank, mpi%me
    print *, ' lb, ub       :', lb, ub
    print *, ' length of random vector (n) :', n
    print *, ' length of slice vector (ns) :', ns
    call error('get_random_slice','slice vector has to be at least as long as th length of random nr stream')
  endif
!  if (go .ne. lb .and. go .ne. ub) then
!    print *, 'lb, ub, go, pos :', lb, ub, go, pos
!    call error('get_random_slice','not a bndry slice (lb or ub), something wrong.')
!  endif
  sl1 = mti(go)
  sl2(1:n) = mt(:,go)
  if (go .eq. lb) then
    lb=lb+1
  else
    ub=ub-1
  endif
END SUBROUTINE get_random_slice
!-----------------------------------------------------------------------
SUBROUTINE put_random_slice(pos,ns,sl1,sl2)
  USE params, only : mpi, rank
  USE random
  implicit none
  integer, intent(in)      :: pos, ns
  integer                  :: sl1
  integer, dimension(1:ns) :: sl2
  integer                  :: go
  integer( kind = wi ), dimension(:,:), allocatable, target :: smt      ! Array for the state vec
  integer( kind = wi ), dimension(:),   allocatable, target :: smti     ! mt[N] is not init
  if (pos .eq. 1) then
    go = lb-1
  else
    go = ub+1
  endif
  if (ns < n) then
    print *, ' Rank, mpi%me :', rank, mpi%me
    print *, ' lb, ub       :', lb, ub
    print *, ' length of random vector (n) :', n
    print *, ' length of slice vector (ns) :', ns
    call error('put_random_slice','slice vector has to be at least as long as the length of random nr stream')
  endif
  allocate(smti(lb:ub),smt(n,lb:ub))
  smti = mti(lb:ub); smt=mt(:,lb:ub)
  deallocate(mti,mt)
  if (go .eq. lb-1) then
    lb = lb - 1
    allocate(mti(lb:ub),mt(n,lb:ub))
    mti(lb)=sl1; mt(:,lb) = sl2(1:n)
    mti(lb+1:ub)=smti; mt(:,lb+1:ub)=smt
  else
    ub = ub + 1
    allocate(mti(lb:ub),mt(n,lb:ub))
    mti(lb:ub-1)=smti; mt(:,lb:ub-1)=smt  
    mti(ub)=sl1; mt(:,ub) = sl2(1:n)
  endif
  deallocate(smti,smt)
END SUBROUTINE put_random_slice
!-----------------------------------------------------------------------
SUBROUTINE init_random ! initialize generator and point to state for later saving
  USE params, only : seed, mid!, mpi
  USE grid_m,   only : g
  USE random
  implicit none
  integer( kind = wi ), allocatable, dimension(:) :: s
  integer                                         :: ns, is, status
  character(len=mid) :: id = &
    'Utilities/random.f90 $Id$'

  call print_id(id)
  global_iz_offset = 10                                                 ! arbitrary offset, if lb<0
  ns = g%gn(3) + 2*global_iz_offset                                     ! make seed vec long enough
!      setting initial seeds to seed[mtran_slices] using
!      the generator Line 25 of Table 1 in
!      [KNUTH 1981, The Art of Computer Programming
!         Vol. 2 (2nd Ed.), pp102]
  allocate(s(ns),stat=status)
  if (status /= 0) then
    print*,'Allocation ERROR in init_random:', ns, status
    stop
  end if
  s(1) = iand(seed,-1)                                                  ! seed of the seeds!
  do is=2, ns
    s(is) = int(modulo(69069_8*s(is-1), 2_8**32),kind=4)
  enddo
  s = -abs(s) 
  call init_genrand( ns, s )                                            ! send vector of seeds to mtran
  call init_rand( ns, s )                                               ! send start seed to rand
END SUBROUTINE init_random
!> dump state of generator for a single slice in a restart file
!-----------------------------------------------------------------------
SUBROUTINE write_random_slice(iz)
  USE params, only : data_unit, mpi
  USE grid_m,   only : g
  USE random
  implicit none
  integer, intent(in) :: iz
  integer             :: go
  go = global_iz_offset + mpi%offset(3) + iz
  write(data_unit) mti(go), mt(:,go)
END SUBROUTINE write_random_slice
!> read state of generator for a single slice in a restart file
!-----------------------------------------------------------------------
SUBROUTINE read_random_slice(iz)
  USE params, only : data_unit, mpi
  USE grid_m,   only : g
  USE random
  implicit none
  integer, intent(in)                :: iz
  integer                            :: go
  integer                            :: smti
  integer, allocatable, dimension(:) :: smt

  go = global_iz_offset + mpi%offset(3) + iz
  allocate(smt(n))
  read(data_unit) smti, smt
  mti(go)  = smti
  mt(:,go) = smt
  deallocate(smt)
END SUBROUTINE read_random_slice
!> skip state of generator for a single slice in a restart file
!-----------------------------------------------------------------------
SUBROUTINE skip_random_slice
  USE params, only : data_unit
  implicit none
  integer                 :: s
  read(data_unit) s
END SUBROUTINE skip_random_slice
!-----------------------------------------------------------------------
SUBROUTINE finalize_random
  USE params, only : random_unit
  USE random
  character(len=7) :: seq
  if (allocated(mt)) deallocate(mt,mti)
                               inquire(unit=random_unit,sequential=seq)
                               if (seq(1:3).eq."YES".or. &
                                   seq(1:3).eq."yes".or.seq(1:3).eq."Yes") close(random_unit)
END SUBROUTINE finalize_random
!-----------------------------------------------------------------------
function genrand_int31( iz ) result( i ) ! generates a random number on [0,0x7fffffff]-interval
  USE random
  implicit none
  intrinsic ishft
  integer, intent( in ) :: iz
  integer               :: iy
  integer( kind = wi )  :: i
  iy = 1
  i = ishft( genrand_int32( iy, iz ), -1 )
end function genrand_int31
!-----------------------------------------------------------------------
function genrand_real1( iz ) result( rr ) ! generates a random number on [0,1]-real-interval
  USE random
  implicit none
  integer, intent( in ) :: iz
  integer               :: iy
  real( kind = wr )     :: r
  real                  :: rr
  integer( kind = wi )  :: a, a1, a0
  iy = 1
  a = genrand_int32( iy, iz )
  a0 = ibits( a, 0, hbs )
  a1 = ibits( a, hbs, hbs )
  r = real( a0, kind = wr ) / 4294967295.0_wr
  r = real( a1, kind = wr ) * ( 65536.0_wr / 4294967295.0_wr ) + r      ! divided by 2^32-1
  rr = r
end function genrand_real1
!-----------------------------------------------------------------------
function genrand_real2( iz ) result( rr ) ! generates a random number on [0,1)-real-interval
  USE random
  implicit none
  intrinsic ibits
  integer, intent( in ) :: iz
  integer               :: iy
  real( kind = wr )     :: r
  real                  :: rr
  integer( kind = wi )  :: a, a1, a0
  iy = 1
  a = genrand_int32( iy, iz )
  a0 = ibits( a, 0, hbs )
  a1 = ibits( a, hbs, hbs )
  r = real( a0, kind = wr ) / 4294967296.0_wr
  r = real( a1, kind = wr ) / 65536.0_wr + r                            ! divided by 2^32
  rr = r
end function genrand_real2
!-----------------------------------------------------------------------
function genrand_real3( iz ) result( rr ) ! generates a random number on (0,1)-real-interval
  USE random
  implicit none
  integer, intent( in ) :: iz
  integer               :: iy
  real( kind = wr )     :: r
  real                  :: rr
  integer( kind = wi )  :: a, a1, a0
  iy = 1
  a = genrand_int32( iy, iz )
  a0 = ibits( a, 0, hbs )
  a1 = ibits( a, hbs, hbs )
  r = ( real( a0, kind = wr ) + 0.5_wr ) / 4294967296.0_wr
  r = real( a1, kind = wr ) / 65536.0_wr + r                            ! divided by 2^32
  rr = r
end function genrand_real3
!-----------------------------------------------------------------------
function genrand_res53( iz )  result( r ) ! generates a random number on [0,1) with 53-bit resolution
  USE random
  implicit none
  intrinsic ishft
  integer, intent( in ) :: iz
  integer               :: iy
  real( kind = wr )     :: r
  integer( kind = wi )  :: a, a0, a1
  integer( kind = wi )  :: b, b0, b1
  iy = 1
  a = ishft( genrand_int32( iy, iz ), -5 )
  a0 = ibits( a, 0, hbs )
  a1 = ibits( a, hbs, hbs )
  b = ishft( genrand_int32( iy, iz ), -6 )
  b0 = ibits( b, 0, hbs )
  b1 = ibits( b, hbs, hbs )
  r = real( a1, kind = wr ) / 2048.0_wr
  r = real( a0, kind = wr ) / 134217728.0_wr + r
  r = real( b1, kind = wr ) / 137438953472.0_wr + r
  r = real( b0, kind = wr ) / 9007199254740992.0_wr + r
end function genrand_res53
!-----------------------------------------------------------------------

!===============================================================================
MODULE rand_mod
  implicit none
  integer, allocatable, dimension(:,:,:) :: rand_seed
END MODULE rand_mod
!===============================================================================
SUBROUTINE change_rand(pos,dir,slice)
  USE rand_mod
  USE grid_m, only : g
  implicit none
  integer, intent(in)                    :: pos, dir
  integer, dimension(g%n(1),g%n(2))      :: slice
  integer, allocatable, dimension(:,:,:) :: scr_seed
  !
  ! OBS! OBS! OBS! We assume that g%n are already updated to the new dimensions.
  ! That, and the proper pos and dir arguments, are the only info needed to upd correctly
  ! 
  if (dir .eq. -1) then 
    allocate(scr_seed(g%n(1),g%n(2),g%n(3)))
    if (pos .eq. 1) then
      slice = rand_seed(:,:,1)
      scr_seed=rand_seed(:,:,2:g%n(3)-1)
    else
      scr_seed=rand_seed(:,:,1:g%n(3))
      slice = rand_seed(:,:,g%n(3)+1)
    endif
    deallocate(rand_seed); allocate(rand_seed(g%n(1),g%n(2),g%n(3)))
    rand_seed = scr_seed
  else
    allocate(scr_seed(g%n(1),g%n(2),g%n(3)-1))
    scr_seed = rand_seed
    deallocate(rand_seed); allocate(rand_seed(g%n(1),g%n(2),g%n(3)))
    if (pos .eq. 1) then
      rand_seed(:,:,1)        = slice
      rand_seed(:,:,2:g%n(3)) = scr_seed
    else
      rand_seed(:,:,1:g%n(3)-1) = scr_seed
      rand_seed(:,:,g%n(3))  = slice
    endif  
  endif
  deallocate(scr_seed)
END SUBROUTINE
!===============================================================================
SUBROUTINE init_rand(ns, s)
  USE rand_mod
  USE params, only : mpi
  USE random, only : global_iz_offset,uiadd,uimlt,uisub
  USE grid_m,   only : g
  implicit none
  intrinsic iand, ishft, ieor, ibits
!
!  This subroutine sets the integer seed to be used with the
!  companion RAND function.
!
  integer, intent(in)                   :: ns
  integer, intent(in), dimension(ns)    :: s
  integer                               :: ss, lb, ub, is, &
                                           ix, iy, lix, liy, liz
  integer, parameter                    :: mult_a = 1812433253 !z'6C078965'
  lb = global_iz_offset + mpi%offset(3)
  ub = lb + g%n(3) - 1
  allocate(rand_seed(g%n(1),g%n(2),g%n(3)))
  !-----------------------------------------------------------------------------
  ! loop over z-slices
  do is = lb, ub
    liz = is - lb + 1
    ss = ibits( s(is), 0, 32 )
    !---------------------------------------------------------------------------
    ! do a full xy-slice to burn the numbers correctly
    do iy = -10, g%gn(2)+11
    do ix = -10, g%gn(1)+11
      ss = ieor( ss, ishft( ss, -30 ) )
      ss = uimlt( ss, mult_a )
      ss = uiadd( ss, uisub( ix, 1 ) )
      ! See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier.
      ! In the previous versions, MSBs of the seed affect
      ! only MSBs of x-slices.
      ! 2002/01/09 modified by Makoto Matsumoto
      ss = ibits( ss, 0, 32 )
      !-------------------------------------------------------------------------
      ! compute corresponding local coords and store if ok
      lix = ix - mpi%offset(1)
      liy = iy - mpi%offset(2)
      if (lix > 0 .and. lix <= g%n(1) .and. liy > 0 .and. liy <= g%n(2)) &
        rand_seed(lix, liy, liz) = ss
    end do
    end do
  end do
END SUBROUTINE init_rand
!===============================================================================
REAL FUNCTION rand(ix, iy, iz)
  USE rand_mod
  implicit none
!
!  This function returns a pseudo-random number for each invocation.
!  It is an adaptation of the "Integer Version 2" minimal 
!  standard number generator which appears in the article:
!
!     Park, Steven K. and Miller, Keith W., "Random Number Generators: 
!     Good Ones are Hard to Find", Communications of the ACM, 
!     October, 1988.
!
  integer, intent(in) :: ix, iy, iz
  integer, parameter  :: mplier=16807, modlus=2147483647, mobymp=127773, momdmp=2836
  real,    parameter  :: rmodlus=1./2147483647.
!
  integer :: hvlue, lvlue, testv

  hvlue = rand_seed(ix,iy,iz) / mobymp
  lvlue = mod(rand_seed(ix,iy,iz), mobymp)
  testv = mplier*lvlue - momdmp * hvlue
  testv = merge(testv,-1,testv .ne. 0)
  rand_seed(ix,iy,iz) = merge(testv, testv + modlus, testv > 0)

  rand = rand_seed(ix,iy,iz)*rmodlus
!
END FUNCTION RAND
!===============================================================================
SUBROUTINE finalize_rand
  USE rand_mod, only : rand_seed
  if (allocated(rand_seed)) deallocate(rand_seed)
END SUBROUTINE finalize_rand
!===============================================================================
