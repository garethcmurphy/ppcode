! Utilities/fileoperations.f90 $Id$
! vim: nowrap
!===============================================================================
SUBROUTINE open_text_file (unit, file, message)
  USE params, only: stdout
  implicit none
  integer unit
  character(len=*) file, message
!...............................................................................
  open(unit=unit,file=trim(file),form='formatted',status='unknown')
  if (stdout>0) write(stdout,*) 'Opening '//trim(file)//message
END SUBROUTINE open_text_file

!===============================================================================
SUBROUTINE open_binary_file (unit, file, message)
  USE params, only: stdout
  implicit none
  integer unit
  character(len=*) file, message
!...............................................................................
  open(unit=unit,file=trim(file),form='unformatted',status='unknown')
  if (stdout>0) write(stdout,*) 'Opening '//trim(file)//message
END SUBROUTINE open_binary_file

!===============================================================================
logical FUNCTION check_stop()
  USE params, only : time_limit, walltime_step, time, tend, mid, do_validate
  implicit none
  logical, external :: file_flag
  real(8), external :: wallclock
  real, save        :: wc0 = 0.0
  character(len=mid), save:: id = &
    'Utilities/fileoperations.f90 $Id$'
!.......................................................................
  call print_id (id)

  ! set the wall clock for the beging of the step
  walltime_step = wallclock()
  check_stop = .true.

  if (do_validate) then
    if (wc0 == 0) wc0 = walltime_step
    check_stop = walltime_step < (wc0 + 60.)
  endif
  
  ! check if stop.flag has been touched
  if (file_flag('stop')) check_stop = .false.

  ! check if we are out of wall clock time
  if (walltime_step .gt. time_limit) check_stop = .false.

  ! check if we have passed tend in simulation time
  if (time >= tend) check_stop = .false.
END FUNCTION check_stop
!=======================================================================
LOGICAL FUNCTION file_exists(file)
  implicit none
  character(len=*) file
  inquire(file=file,exist=file_exists)
END FUNCTION
!=======================================================================
LOGICAL FUNCTION dir_exists(dir)
  implicit none
  character(len=*) dir
#if __INTEL_COMPILER
  inquire(directory=dir,exist=dir_exists)
#else
  inquire(file=dir,exist=dir_exists)
#endif
END FUNCTION
!=======================================================================
LOGICAL FUNCTION mkdir(dir,name,slp,catastrophic)
  USE params, only : master
#if __INTEL_COMPILER
  USE ifport,       only : makedirqq                                    ! make dir routine from ifort
#endif
  implicit none
  character(len=*), intent(in) :: dir, name                             ! name of dir and calling routine
  integer,          intent(in) :: slp                                   ! how long to sleep after creation
  logical,          intent(in) :: catastrophic                          ! is it catastrophic, if dir was not created ?
  logical,          external   :: dir_exists
  integer                      :: ierr
!
  call barrier()
  if (master) then
#if __INTEL_COMPILER
    mkdir = makedirqq(trim(dir))
#else
    call system('mkdir '//trim(dir))
    mkdir = dir_exists(trim(dir))
#endif
    ierr = merge(1,0,mkdir)
    call mpi_broadcast(ierr, 0)
  else
    call mpi_broadcast(ierr, 0)
    mkdir = (ierr .eq. 1)
  endif
  if (mkdir) then
    !call warning(name,'Dir '//trim(dir)//' did not exist.'// &
    !                       ' I have now made it; but check your input'// &
    !                       ' file or directory structure for typos and errors.')
  else
    if (catastrophic) &
      call error(name,'could not make directory '//trim(dir)) ! Error if we could not make dir
  endif
#ifndef __xlc__ 
  !if (mkdir) call sleep(slp)                                            ! give cluster file system a bit of time
#endif
END FUNCTION mkdir
!=======================================================================
#ifndef __xlc__
INTEGER FUNCTION get_proc_id()
#if __INTEL_COMPILER
  USE IFPORT, only : getpid 
#endif
  implicit none
#ifdef __PGI
  integer, external :: getpid
#endif
  get_proc_id = getpid()
END FUNCTION get_proc_id 
!=======================================================================
SUBROUTINE get_memory_use(mem_in_use)
  USE params, only : mfile, touch_unit
  implicit none
  integer(kind=8)      :: mem_in_use, tmp
  integer, external    :: get_proc_id
  logical, external    :: file_exists
  character(len=mfile) :: fname
#ifdef LIBMEMACCT
  call gwms(mem_in_use)
#else
  write(fname,*) get_proc_id()
  fname = '/proc/'//trim(fname)//'/statm'
  if (file_exists(fname)) then
    open(touch_unit, file=fname, form='formatted')
    read(touch_unit,*) tmp
    mem_in_use = tmp*1024_8
  else
    mem_in_use = -1
  endif
#endif 
END SUBROUTINE get_memory_use
#endif
#ifdef __PGI
! Only needed for the portland group compiler to get around read(..,pos=p) problem
!=======================================================================
SUBROUTINE file_seek(u,p)
  USE params, only : master
  implicit none
  integer,         intent(in) :: u
  integer(kind=8), intent(in) :: p
  integer                     :: void
#ifdef __i386__
  integer, external           :: fseek
  void = fseek(u,int(p,kind=4),2)
#else
  integer, external           :: fseek64
  void = fseek64(u,p,0_8)
#endif
  if (void .ne. 0) then
    print *, 'Error code :', void
    call error('file_seek','Error when seeking')
  endif

END SUBROUTINE file_seek
! Only needed for the portland group compiler to get around go-to-end problem
!=======================================================================
SUBROUTINE file_append(u)
  USE params, only : master
  implicit none
  integer,         intent(in) :: u
  integer                     :: void
  ! Magically, element nr 8 in the stat array is the size of the file
#ifdef __i386__
  integer, external              :: fseek, fstat
  integer, dimension(32)         :: stat
  void = fstat(u,stat)
  void = fseek(u,stat(8),0)
#else
  integer, external              :: fseek64, fstat64
  integer(kind=8), dimension(32) :: stat
  void = fstat64(u,stat)
  void = fseek64(u,stat(8),0)
#endif
  if (void .ne. 0) then
    print *, 'Error code :', void
    call error('file_append','Error when seeking to the end of the file')
  endif

END SUBROUTINE file_append
#endif
#ifdef __xlc__
!=======================================================================
SUBROUTINE flush(u)
  implicit none
  integer :: u
  call flush_(u)
END SUBROUTINE flush
#endif
!=======================================================================
INTEGER(KIND=8) FUNCTION file_position(u)
#if __INTEL_COMPILER
  USE IFPORT, only : ftelli8
#endif
  USE params, only : master
  implicit none
#ifdef __PGI
#ifdef __i386__
  integer, external   :: ftell
#else
  integer, external   :: ftell64
#endif
#endif
#ifdef __xlc__
  integer(kind=8), external   :: ftell64_
#endif
  integer, intent(in) :: u
  logical, save       :: first_call
  logical             :: compiler_detected
  compiler_detected = .false.
#if __INTEL_COMPILER
  file_position = ftelli8(u)
  compiler_detected = .true.
#endif
#if defined(__GFORTRAN__) || defined(__G95__)
  if (first_call .and. master) call warning('file_position','You have compiled with g95'// &
                                            ' or gfortran, and they only contain a 32 bit'// &
                                            ' version of ftell, the underlying C-function.'// &
                                            ' file_position could overflow for files > 2GB.')
  file_position = ftell(u)
  compiler_detected = .true.  
#endif
#ifdef __xlc__
  file_position = ftell64_(u)
  compiler_detected = .true.  
#endif
#ifdef __PGI
#ifdef __i386__
  file_position = ftell(u)
#else
  file_position = ftell64(u)
#endif
  compiler_detected = .true.  
#endif

  if (.not. compiler_detected) call error('file_position', 'Compiler not'// &
    ' detected. Could not use compiler specific function for file_position.')

  first_call =.false.

END FUNCTION file_position
!=======================================================================
SUBROUTINE file_delete(file)
  USE params, only : touch_unit
  implicit none
  character(len=*) file
  logical, external :: file_exists
  
  if (file_exists(file)) then
    open(file=file,unit=touch_unit,status='old')
    close(touch_unit,status='delete')
  else
    call error('file_delete','Trying to delete '//trim(file)//' but it does not exist')
  endif
END SUBROUTINE file_delete
!=======================================================================
SUBROUTINE textfile_copy(file1,file2)
  USE params, only : copy_unit1, copy_unit2
  implicit none

  character(len=*), intent(in)  :: file1, file2
  character(len=120)            :: line
  logical, external             :: file_exists

  ! check that it exists
  if (.not. file_exists(trim(file1))) then
    call warning('textfile_copy',trim(file1)//' does not exist. returning silently!')
    return
  endif
  ! Stuff below taken from stagger code
  open (copy_unit1,file=trim(file1),form='formatted',status='old')    ! copy from
  open (copy_unit2,file=trim(file2),form='formatted',status='unknown')! copy to
  do
    read (copy_unit1,'(a)',end=1) line                                ! read line by line
    write(copy_unit2,'(a)') line(1:max(1,len_trim(line)))
  end do
1 close (copy_unit2)
  close (copy_unit1)

END SUBROUTINE textfile_copy
! service routine to detect flags on disk
!-------------------------------------------------------------------------------
LOGICAL FUNCTION file_flag(file) result(flag)
  USE params, only: flag_unit, master, datadir
  implicit none
  character(len=*) file
  integer iostat
!-----------------------------------------------------------------------
  if (master) then
    inquire (file=trim(datadir)//'/'//trim(file)//'.flag',exist=flag)
    if (flag) then
      open (flag_unit,file=trim(datadir)//'/'//trim(file)//'.flag',status='old')
      close (flag_unit,status='delete',iostat=iostat)
    end if
  end if
  call mpi_broadcast_logical(flag,0)
END FUNCTION file_flag
! service routine to detect flags with a number on disk for updating run parameters
!-----------------------------------------------------------------------
LOGICAL FUNCTION file_integer(file,number) result(flag)
  USE params, only: master, flag_unit, datadir, stdout
  implicit none
  integer number,lnumber
  character(len=*) file
  integer iostat
!-----------------------------------------------------------------------
  if (master) then
    inquire (file=trim(datadir)//'/'//trim(file)//'.flag',exist=flag)
    if (flag) then
      open (flag_unit,file=trim(datadir)//'/'//trim(file)//'.flag',status='old',form='formatted')
      read(flag_unit,*,iostat=iostat) lnumber
      if (iostat .ne. 0) then
        write(stdout,*) 'Problems in reading number from '//file//'.flag'
        write(stdout,*) 'Iostat =', iostat
        write(stdout,*) 'Number =', lnumber
        write(stdout,*) 'Continuing as if no flag has been set'
        flag = .false.
      else
        write(stdout,*) 'file_integer:, flag, value =', file, lnumber
        number = lnumber
      endif
      close (flag_unit,status='delete',iostat=iostat)
    end if
  end if
  call mpi_broadcast_logical(flag,0)
  if (flag) call mpi_broadcast(number,0)
END FUNCTION file_integer
! service routine to detect flags with a real number on disk for updating run parameters
!-----------------------------------------------------------------------
LOGICAL FUNCTION file_real(file,number) result(flag)
  USE params, only: master, flag_unit, datadir, stdout
  implicit none
  real number,lnumber
  character(len=*) file
  integer iostat
!-----------------------------------------------------------------------
  if (master) then
    inquire (file=trim(datadir)//'/'//trim(file)//'.flag',exist=flag)
    if (flag) then
      open (flag_unit,file=trim(datadir)//'/'//trim(file)//'.flag',status='old',form='formatted')
      read(flag_unit,*,iostat=iostat) lnumber
      if (iostat .ne. 0) then
        write(stdout,*) 'Problems in reading number from '//file//'.flag'
        write(stdout,*) 'Iostat =', iostat
        write(stdout,*) 'Number =', lnumber
        write(stdout,*) 'Continuing as if no flag has been set'
        flag = .false.
      else
        write(stdout,*) 'file_real:, flag, value =', file, lnumber
        number = lnumber
      endif
      close (flag_unit,status='delete',iostat=iostat)
    end if
  end if
  call mpi_broadcast_logical(flag,0)
  if (flag) call mpi_broadcast_real(number,0)
END FUNCTION file_real
!-----------------------------------------------------------------------
