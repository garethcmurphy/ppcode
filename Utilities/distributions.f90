! Utilities/distributions.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------
MODULE distributions
  implicit none
  
  type :: table
    real                        :: vmax, vmin                           ! min and max of func
    real                        :: arg1, arg2                           ! arg1 and arg2 in pdf call
    logical                     :: inuse                                ! is table in use for this
    logical                     :: set_vmax=.false.                     ! should we assign the upper endpoint??
    real, dimension(:), pointer :: v
    real, dimension(:), pointer :: pv
  end type                                                              ! species?
  type(table),  allocatable, dimension(:)   :: maxwell
!-----------------------------------------------------------------------
END MODULE distributions
!-----------------------------------------------------------------------
SUBROUTINE init_distributions                                           ! create tables
  USE params,  only : nspecies,mid,tsize,stdout,out_namelists,master,do_2d2v
  USE species, only : sp
  USE units,   only : c
  USE distributions
  implicit none
  integer  :: isp, i
  external :: relmaxwell, maxwell_juttner, maxwell_juttner_2D
  character(len=mid), save:: id = &
    'Utilities/distributions.f90 $Id$'
  real(kind=8), parameter :: pi     = 3.14159265358979323846264338328
  real(kind=8)            :: th, v_mean 
  logical,      parameter :: do_table_dump=.false.                      ! debug switch to dump distributions to file
!.......................................................................
  if (nspecies==0) return
  call print_id(id)

  allocate(maxwell(nspecies))
  maxwell(:)%set_vmax= .false.                                          ! We don't use end point (v==c) for thermal stuff.
 
  maxwell%inuse = (sp%mass > 0) .and. (sp%temperature > 0)
  do isp=1,nspecies
    if (maxwell(isp)%inuse) then 
      allocate(maxwell(isp)%pv(tsize))
      allocate(maxwell(isp)%v(tsize))
    endif
  enddo
  if (stdout.ge.0 .and. out_namelists) &
    write(stdout,*) 'init_distributions: inuse =',nspecies,maxwell(1:nspecies)%inuse
  
  !=============================================================================
  ! Table in units of vg and using correct maxwell-juttner distribution
  !=============================================================================
  ! One can integrate the pdf to find the typical value of vg for a givn temperature, to set a reasonable vmax:
  ! Setting theta = m c^2 / kb T we have (K2: Bessel-K function with index 2)
  ! <vg> = Int_0^infinity dvg vg f(vg) = th / K2(th) Int_0^infinity dvg  exp(-th g) (vg)^3
  ! which can be integrated analytically to yield ( using the approximation K2(th) = sqrt[pi/(2 th)] exp(-th) )
  ! <vg> = sqrt( 8 pi / th) * (3 + th *(3 + th)) / th^2 * c, which has the correct non-relativistic limit of sqrt(8 / (pi th))
  ! We set the max speed to 30 times the most probable speed, which should be more than reasonable
  !=============================================================================
  maxwell%inuse = (sp%mass > 0) .and. (sp%temperature > 0)
  do isp=1,nspecies
    if (.not. maxwell(isp)%inuse) cycle
    th = (sp(isp)%mass * real(c%c,kind=8)**2) / (c%kb * sp(isp)%temperature)
    v_mean = sqrt(8 / (pi * (th + 0.055)) ) * (3 + th*(3 + th)) / th**2
    maxwell(isp)%vmax = v_mean * 100.
    maxwell(isp)%vmin = maxwell(isp)%vmax / 1e6
    maxwell(isp)%arg1 = sp(isp)%mass
    maxwell(isp)%arg2 = sp(isp)%temperature
    if (master .and. out_namelists) write(stdout,'(i4,a,1p,4g10.2)') &
                isp, ' mc2/kbT, vmin, vmean, vmax :', &
                th, maxwell(isp)%vmin, v_mean, maxwell(isp)%vmax
  enddo

  if (do_2d2v) then
    call maketable(maxwell,nspecies,maxwell_juttner_2D)
  else
    call maketable(maxwell,nspecies,maxwell_juttner)
  endif

  if (do_table_dump) then
    open(17,file='table_dump.dat',form='formatted')
    do isp=1,nspecies
      if (maxwell(isp)%inuse) then 
        do i=1,tsize
          write(17,*) maxwell(isp)%v(i), maxwell(isp)%pv(i)
        enddo
      endif
    enddo
    close(17)
  endif
END SUBROUTINE init_distributions
!-----------------------------------------------------------------------
!>  Returns probability for relativistic maxwellian C(umulative)DF
!!  i.e. the integral of the PDF. Notice the wikipedia entry is incorrect!!
!!
!!      Reference: Jorn Dunkel, Peter Hanggi: Relativistic Brownian Motion
!!                 Phys. Rep. 471(1): 1-73, 2009, arXiv: 0812.1996
!!                 DOI: http://dx.doi.org/10.1016/j%2Ephysrep%2E2008%2E12%2E001
!!
!!  cdf = return value
!!  m = electron mass, vg = v*gamma, c = lightspeed, kb = Boltzmann
!!  t = temperature.
SUBROUTINE maxwell_juttner(cdf, vg, m, t, vgmin, vgmax)
  USE units, only  : c
  implicit none
  real             :: cdf
  real, intent(in) :: vg, m, t, vgmin, vgmax
  real(kind=8)     :: cc, th, a, b, x, x2, dx, gm1, s, s_old, err, pp
  real(kind=8), save       :: vg_last=-1., cdf_last=0., th_last=0., pref=0.     ! last point integrated
  real(kind=8), parameter  :: pi     = 3.14159265358979323846264338328
  real(kind=8), parameter  :: sqrtpi = 1.77245385090551588191942755657
  real(kind=8), parameter  :: eps    = 1e-7_8                                   ! integration accurracy
  integer            :: i, n, it
  integer, parameter :: max_it = 24
  integer, save      :: nprint=100

  ! We just use straightforward integration with error control. It is not fast, but it works A-OK.
  ! The pdf is given as:
  !   f(vg)d(vg) = th exp(-th) / K2(th) exp(-th(g-1)) (vg)^2 d(vg), th = m c^2 / kb T
  ! We do not need to get the prefactor totally right, because we normalise afterwards.

  cc = real(c%c, kind=8)
  th = m * cc**2 / (c%kb*t)

  if (vg .lt. vg_last .or. vg_last .lt. 0. .or. th_last .ne. th) then ! starting a new integration
    cdf_last = 0.0_8
    vg_last  = vgmin
    th_last  = th
    ! We can use this aprroximate taylor expansion for K2(x) to get things more or less right
    ! it is correct to 20% over the whole range, and for low temperatures (th >> 1), it is an
    ! exceptionally good approximation. It is just to keep the integral around 1, renormalization is done in the end.
    !
    ! Notice that in exp(-th) / K2(th) the exponential factor cancels out, direct evaluation is unstable.
    pref = sqrt(2.0_8*(th + 0.055) / pi) * th ! pref = th exp(-th) / K2(th)
  endif

  if (vg .eq. vgmin) then
    cdf_last = 0.0_8
    cdf = cdf_last
    return
  endif

  ! use trapez integration to get from vg_last to vg. Probably overkill, but it works and is fast enough.
  err = eps; n=8
  a = vg_last; b=vg
  s_old = 0.0_8
  it = 0
  do while (err >= eps .and. it < max_it)
    it = it + 1
    n = n * 2
    dx = (b-a)/n
    s = 0.0_8
    do i=0,n
      x = a + dx*i; x2 = x*x
      gm1 = x2/(sqrt(1.0_8 + x2) + 1.0_8)     ! gamma - 1 = vg^2 / (gamma + 1), numerically stable.
      pp = merge(1.0_8,0.5_8,i*(n-i) > 0)     ! endpoints only give a factor 0.5
      s = s + pp*exp( - min(th * gm1, 500.0_8)) * x2
    enddo
    s = s * dx * pref
    err = abs((s - s_old) / s)
    s_old = s
  enddo
                               !print *, 'maxwell_juttner :', err, eps, a, b, s_old, s, n, it, max_it

  cdf = cdf_last + s
  
  if (cdf <= 0.) then
    if (nprint > 0 .and. cdf < -1e-10) then
      nprint = nprint-1
      call warning('maxwell_juttner','cdf==0. in maxwell-juttner, setting to 1e-10')
    endif
    if (nprint == 0) call warning('maxwell_juttner','additional printout suppressed')
    cdf = 1e-10
  end if

  vg_last = vg
  cdf_last = cdf_last + s

END SUBROUTINE maxwell_juttner
!-----------------------------------------------------------------------
!>  Returns probability for relativistic maxwellian C(umulative)DF
!!  i.e. the integral of the PDF. Notice the wikipedia entry is incorrect!!
!!
!!      Reference: Jorn Dunkel, Peter Hanggi: Relativistic Brownian Motion
!!                 Phys. Rep. 471(1): 1-73, 2009, arXiv: 0812.1996
!!                 DOI: http://dx.doi.org/10.1016/j%2Ephysrep%2E2008%2E12%2E001
!!
!!  cdf = return value
!!  m = electron mass, vg = v*gamma, c = lightspeed, kb = Boltzmann
!!  t = temperature.
SUBROUTINE maxwell_juttner_2D(cdf, vg, m, t, vgmin, vgmax)
  USE units, only  : c
  implicit none
  real             :: cdf
  real, intent(in) :: vg, m, t, vgmin, vgmax
  real(kind=8)     :: cc, th, a, b, x, x2, dx, gm1, s, s_old, err, pp
  real(kind=8), save       :: vg_last=-1., cdf_last=0., th_last=0., pref=0.     ! last point integrated
  real(kind=8), parameter  :: pi     = 3.14159265358979323846264338328
  real(kind=8), parameter  :: sqrtpi = 1.77245385090551588191942755657
  real(kind=8), parameter  :: eps    = 1e-7_8                                   ! integration accurracy
  integer            :: i, n, it
  integer, parameter :: max_it = 24
  integer, save      :: nprint=100

  ! We just use straightforward integration with error control. It is not fast, but it works A-OK.
  ! The pdf is given as:
  !   f(vg)d(vg) = 1 / [th * (1 + th)] exp(-th(g-1)) (vg) d(vg), th = m c^2 / kb T
  ! We do not need to get the prefactor totally right, because we normalise afterwards.

  cc = real(c%c, kind=8)
  th = m * cc**2 / (c%kb*t)

  if (vg .lt. vg_last .or. vg_last .lt. 0. .or. th_last .ne. th) then ! starting a new integration
    cdf_last = 0.0_8
    vg_last  = vgmin
    th_last  = th
    pref = 1.0_8 / (th * (1.0_8 + th))
  endif

  if (vg .eq. vgmin) then
    cdf_last = 0.0_8
    cdf = cdf_last
    return
  endif

  ! use trapez integration to get from vg_last to vg. Probably overkill, but it works and is fast enough.
  err = eps; n=8
  a = vg_last; b=vg
  s_old = 0.0_8
  it = 0
  do while (err >= eps .and. it < max_it)
    it = it + 1
    n = n * 2
    dx = (b-a)/n
    s = 0.0_8
    do i=0,n
      x = a + dx*i; x2 = x*x
      gm1 = x2/(sqrt(1.0_8 + x2) + 1.0_8)     ! gamma - 1 = vg^2 / (gamma + 1), numerically stable.
      pp = merge(1.0_8,0.5_8,i*(n-i) > 0)     ! endpoints only give a factor 0.5
      s = s + pp*exp( - min(th * gm1, 500.0_8)) * x
    enddo
    s = s * dx * pref
    err = abs((s - s_old) / s)
    s_old = s
  enddo
                               !print *, 'maxwell_juttner :', err, eps, a, b, s_old, s, n, it, max_it

  cdf = cdf_last + s
  
  if (cdf <= 0.) then
    if (nprint > 0 .and. cdf < -1e-10) then
      nprint = nprint-1
      call warning('maxwell_juttner','cdf==0. in maxwell-juttner, setting to 1e-10')
    endif
    if (nprint == 0) call warning('maxwell_juttner','additional printout suppressed')
    cdf = 1e-10
  end if

  vg_last = vg
  cdf_last = cdf_last + s

END SUBROUTINE maxwell_juttner_2D

!-------------------------------------------------------------------------------
MODULE SEARCH

  INTERFACE openbinsearchb
    MODULE PROCEDURE openbinsearchb_r4, openbinsearchb_i2
  END INTERFACE

CONTAINS
!-------------------------------------------------------------------------------
! xx(nl:nu) must be monotonic in- or decreasing and x is _inside_ [xx(1):xx(n)]
FUNCTION openbinsearchb_i2(xx,x,nl,nu) result(jl)
  implicit none
  integer,                           intent(in) :: nl,nu                                   ! length
  integer(kind=2), dimension(nl:nu), intent(in) :: xx                                      ! search array
  integer(kind=2),                   intent(in) :: x                                       ! value
  integer                                       :: jl                                      ! index 
  integer                                       :: jm,ju 
  logical                                       :: ascnd
!...............................................................................
  ascnd = (xx(nu) .ge. xx(nl))
  jl = nl - 1
  ju = nu + 1
  do
    if (ju - jl .le. 1) exit
    jm = (ju + jl)/2
    if (ascnd .eqv. (x .ge. xx(jm))) then
      jl = jm
    else
      ju=jm
    end if
  end do
END FUNCTION openbinsearchb_i2
!-------------------------------------------------------------------------------
! xx(nl:nu) must be monotonic in- or decreasing and x is _inside_ [xx(1):xx(n)]
FUNCTION openbinsearchb_r4(xx,x,nl,nu) result(jl)
  implicit none
  integer,                intent(in) :: nl,nu                                   ! length
  real, dimension(nl:nu), intent(in) :: xx                                      ! search array
  real,                   intent(in) :: x                                       ! value
  integer                            :: jl                                      ! index 
  integer                            :: jm,ju 
  logical                            :: ascnd
!...............................................................................
  ascnd = (xx(nu) .ge. xx(nl))
  jl = nl - 1
  ju = nu + 1
  do
    if (ju - jl .le. 1) exit
    jm = (ju + jl)/2
    if (ascnd .eqv. (x .ge. xx(jm))) then
      jl = jm
    else
      ju=jm
    end if
  end do
END FUNCTION openbinsearchb_r4
!-------------------------------------------------------------------------------
! xx(1:n) must be monotonic in- or decreasing and x is _inside_ [xx(1):xx(n)]
FUNCTION openbinsearch(xx,x,n) result(jl)
  implicit none
  integer,            intent(in) :: n                                           ! length
  real, dimension(n), intent(in) :: xx                                          ! search array
  real,               intent(in) :: x                                           ! value
  integer                        :: jl                                          ! index 
  integer                        :: jm,ju 
  logical                        :: ascnd
!...............................................................................
  ascnd = (xx(n) .ge. xx(1))
  jl = 0
  ju = n + 1
  do
    if (ju - jl .le. 1) exit
    jm = (ju + jl)/2
    if (ascnd .eqv. (x .ge. xx(jm))) then
      jl = jm
    else
      ju=jm
    end if
  end do
END FUNCTION openbinsearch
!-------------------------------------------------------------------------------
! xx(1:n) must be monotonic in- or decreasing
FUNCTION binsearch(xx,x,n)
  implicit none
  integer,            intent(in) :: n                                           ! length
  real, dimension(n), intent(in) :: xx                                          ! search array
  real,               intent(in) :: x                                           ! value
  integer                        :: binsearch                                   ! index 
  integer                        :: jl,jm,ju 
  logical                        :: ascnd
!...............................................................................
  ascnd = (xx(n) .ge. xx(1))
  jl = 0
  ju = n + 1
  do
    if (ju - jl .le. 1) exit
    jm = (ju + jl)/2
    if (ascnd .eqv. (x .ge. xx(jm))) then
      jl = jm
    else
      ju=jm
    end if
  end do
  if (x .eq. xx(1)) then
    binsearch = 1
  else if (x .eq. xx(n)) then 
    binsearch = n - 1
  else
    binsearch = jl
  end if
END FUNCTION binsearch
!-------------------------------------------------------------------------------
! Given an array xx(1:N), and given a value x, returns a value jlo 
! such that x is between xx(jlo) and xx(jlo+1). xx must be monotonic, 
! either increasing or decreasing. jlo = 0 or jlo = N is returned to 
! indicate that x is out of range. jlo on input is taken as the initial
! guess for jlo on output. 
SUBROUTINE hunt(xx,x,jlo,nn)
  implicit none
  integer                :: jlo
  real,               intent(in) :: x
  real, dimension(:), intent(in) :: xx
  integer, optional,  intent(in) :: nn
  integer                        :: n,inc,jhi,jm 
  logical                        :: ascnd 
!...............................................................................
  if (present(nn)) then ; n=nn ; else ; n=size(xx) ; endif 
  ascnd = (xx(n) >= xx(1))                                                      ! True if ascending order of table, false otherwise. 
  if (jlo <= 0 .or. jlo > n) then                                               ! Input guess not useful. Go immediately to bisection. 
    jlo=0 
    jhi=n+1 
  else 
    inc=1                                                                       ! Set the hunting increment. 
    if (x >= xx(jlo) .eqv. ascnd) then                                          ! Hunt up: 
      do
        jhi=jlo+inc 
        if (jhi > n) then                                                       ! Done hunting, since off end of table
          jhi= n+1
          exit
        else 
          if (x < xx(jhi) .eqv. ascnd) exit
          jlo=jhi                                                               ! Not done hunting, 
          inc=inc+inc                                                           ! so double the increment
        end if
      end do                                                                    ! and try again.
    else                                                                        ! Hunt down:
      jhi=jlo
      do 
        jlo=jhi-inc
        if (jlo < 1) then                                                       ! Done hunting, since off end of table.
          jlo=0
          exit
        else
          if (x >= xx(jlo) .eqv. ascnd) exit
          jhi=jlo                                                               ! Not done hunting,
          inc=inc+inc                                                           ! so double the increment
        end if
      end do                                                                    ! and try again.
    end if
  end if                                                                        ! Done hunting, value bracketed.
  do                                                                            ! Hunt is done, so begin the final bisection phase:
    if (jhi-jlo <= 1) then
      if (x == xx(n)) jlo=n-1
      if (x == xx(1)) jlo=1
      exit
    else
      jm=(jhi+jlo)/2
      if (x >= xx(jm) .eqv. ascnd) then
        jlo=jm
      else
        jhi=jm
      end if
    end if
  end do
END SUBROUTINE hunt
!-------------------------------------------------------------------------------
LOGICAL FUNCTION inside(a,l,x,m)                                                ! check if a is in [x(l),x(l)+1]
  implicit none
  integer l, m, ll
  real a, x(m)
!...............................................................................
  ll = max(1,min(l,m-1))
  inside = (a-x(ll))*(x(ll+1)-a) >= 0.
END FUNCTION inside
!-------------------------------------------------------------------------------
SUBROUTINE check_search (ok,a,l,x,m)                                            ! check if a search routine worked
  logical ok
  integer l,m
  real a,x(m)
!...............................................................................
  if ((a-x(1))*(x(m)-a) >= 0.) then
    ok = ok .and. inside(a,l,x,m)
  else
    ok = ok .and. (.not.inside(a,l,x,m))
  end if
END SUBROUTINE
!-------------------------------------------------------------------------------
END MODULE SEARCH

!-------------------------------------------------------------------------------
SUBROUTINE test_search                                                          ! verify search routines
  USE search
  implicit none
  integer, parameter:: nx=100, na=6                                             ! must not have too few points nx!
  real x(nx), a(na)
  integer i, mx, l, lh, l1, l2, k
  logical ok
!...............................................................................
  ok = .true.                                                                   ! assume the best
  do k=0,1                                                                      ! increasing / decreasing
    x = (/(  i*(-1)**k, i=1,nx )/)                                              ! linear scale
    lh = 0                                                                      ! force binary search in hunt
    do mx=nx-1,nx                                                               ! try even and odd
      a = (/ 0.9, 1.1, mx/2., mx-2., mx-.1, mx+.1 /)                            ! a comprehensive list
      do i=1,na                                                                 ! try them
        l1 = openbinsearch(x,a(i),mx)    ; call check_search(ok,a(i),l1,x,mx)   ! test openbinsearch
        l2 = openbinsearchb(x,a(i),1,mx) ; call check_search(ok,a(i),l2,x,mx)   ! test openbinsearchb
        call hunt(x,a(i),lh,mx)          ; call check_search(ok,a(i),lh,x,mx)   ! test hunt
      end do
    end do
  end do
  print*,'Testing the search module, result =',ok                               ! always print result
  if (ok) return                                                                ! return if all is well
  lh = 0
  do k=0,1                                                                      ! increasing / decreasing
    do mx=nx-1,nx                                                               ! odd and even
      a = (/ 0.9, 1.1, mx/2., mx-2., mx-.1, mx+.1 /)
      do i=1,na                                                                 ! various values
        l1 = openbinsearch(x,a(i),mx)
        l2 = openbinsearchb(x,a(i),1,mx)
        call hunt(x,a(i),lh,mx)
        print'(f12.6,3(i5,l3))',a(i),l1,inside(a(i),l1,x,mx), &                 ! print details
                                     l2,inside(a(i),l2,x,mx), &
                                     lh,inside(a(i),lh,x,mx)
      end do
    end do
  end do
  call error('test_search','test failed')                                       ! dead end
END SUBROUTINE test_search

!-----------------------------------------------------------------------
! Produces a lookup table for finding velocity magn. for 'dim' species.
! Works ONLY for monotone increasing functions (as are all CDF's). 
!-----------------------------------------------------------------------
! NOTE: There are endpoint issues in the case of CDF that reaches Pmax(x)==1
! (fx for relatively cold distributions) at low values of x (from where CDF is thus constant). 
!
! The solution was not to hack a maxvalue of 1, as was done earlier: In this case since we will get 
! a topheavy distribution. Instead we should let Pmax = Plast, such that we get that constant 
! value over the range from that x, where P becomes Pmax to xmax. This should not affect extremely
! warm distributions, as they simply don't have vmax=1 (lightspeed) but rather vmax -> 1.-epsilon(1.),
! depending on 'tsize' (the resolution of the table). 
!
! Now; for distribution-tables used for - fx - placement of particles, we may want to use the endpoint
! in order to get representation over an entire range. 
!
! Therefore; we introduced an endpoint parameter, logical :: table(nsp)%set_vmax, defaulted to .false.
! and we can switch this if we'll need the endpoint in init-calculations, or in boundary conditions.

!-----------------------------------------------------------------------
SUBROUTINE maketable(tab,dim,cdf)
  USE params,        ONLY : tsize, stdout, out_namelists, master
  USE search,        ONLY : hunt
  USE distributions, ONLY : table
  USE units,         ONLY : c
  USE species,       ONLY : sp
  implicit none
  
  integer isp, i, j, jsave
  integer, intent(in) :: dim
  type(table), dimension(dim) :: tab
  real,      dimension(tsize) :: v_i(tsize), pv_i(tsize)
  real :: rcdf, pv_min, pv_max, slope, lnv_min, lnv_max
  real :: pv_plus, pv_minus, v_plus, v_minus, v_n, pv_n

  interface
    SUBROUTINE cdf(cf,v,arg1,arg2,vmin,vmax)
    implicit none
    real               :: cf
    real, intent(in) :: v,arg1,arg2,vmin,vmax
    END SUBROUTINE cdf
  end interface
!...............................................................................
  call trace_enter('maketable')
  pv_max = 1.0                                                                  ! for probability max value
  pv_min = 0.0
  if (stdout.ge.0 .and. out_namelists) write(stdout,*) &
    'maketable: inuse =',dim,tab(1:dim)%inuse
  do isp = 1,dim
    if (.not. tab(isp)%inuse) cycle
    jsave = 1
    v_minus = 0.
    v_plus = 0.
    pv_minus = 0.
    pv_plus = 0.
    lnv_min = alog(tab(isp)%vmin)
    lnv_max = alog(tab(isp)%vmax)
    if (master .and. out_namelists) write(stdout,'(a,i3,i9,2(1pe11.3))') &
      ' maketable: isp,tsize,vmin,vmax =',isp,tsize,tab(isp)%vmin,tab(isp)%vmax
                                                                                ! Make equal part intervals (# of tsize) in v_i \in [vmin,vmax] .
                                                                                ! Find corresponding probabilities; these will not be equally spaced 
                                                                                ! so we will need interpolate to evenly spaced pv's to produce a
                                        ! reasonable lookup-table.
    do i = 1,tsize
      v_i(i) = exp((lnv_max - lnv_min) * ((i-1.)/(tsize-1.)) + lnv_min)
      call cdf(rcdf,v_i(i), tab(isp)%arg1, tab(isp)%arg2, tab(isp)%vmin, &
               tab(isp)%vmax)
      pv_i(i) = (pv_max - pv_min) * rcdf + pv_min

      if(pv_i(i) .lt. 0.) then                                                  ! Make a safety check on final value of pv_i (relevant
                                                                                ! only at (tops) the few first lower end points)
        if(stdout.ge.0) write(stdout,*) 'i,v_i(i),pv_i(i): ',i,v_i(i), pv_i(i)  ! FIXME: this is a hack - should in future be found better
                                                                                ! more robust table inversion method. But we may use this for now.
        if(stdout.ge.0) write(stdout,*) &
      'Error, pv_i(i) < 0, checking |pv_i(i)|...'
        if(pv_i(i).ge.-epsilon(0.)) then 
          if(stdout.ge.0) write(stdout,*) 'pv_i(i): ',pv_i(i)
          if(stdout.ge.0) write(stdout,*) &
        'pv_i(i) not "too" small... substituting pv_i(i)=',abs(pv_i(i))
          pv_i(i) = abs(pv_i(i))
        else
          call error('maketable','pv_i(i) too far off - stopping...')
        endif
      endif
     !if (i<10.or.i>tsize-10) print'(i5,2(1pe15.7))',i,v_i(i),pv_i(i)

    enddo
    v_i(tsize) = tab(isp)%vmax                                                  ! Make sure end point is correct, despite round-off

    pv_i(1) = 0.                                                                ! Make sure that CDF is within bounds and normalized.
    if (maxval(pv_i(:)) == 0.) then
      call error('maketable','maxval(pv_i) == 0 -- stopping...')
    end if
    pv_i(:) = pv_i(:)/maxval(pv_i(:))                                           ! Actually, we don't need the requirement that CDF is 
                                                                                ! normalized at entry. Just that it is monotonically 
                                                                                ! increasing and begins with Pmin=0.

! Now we need to construct an array where the pv_n's are equally spaced.
! This is done by a sort of backtracking 1st-order interpolation algorithm (demands that
! CDF is a simple increasing function (but it should be since _it's an integral_ of PDF))
!
!      1) Find equally spaced pv_n.
!      2) Run through pv_i to find pv_i's bracketing pv_n call them p+ and p- .
!      3) Find the corresponding v+ and v- from these.
!      4) Now produce v_n = (v-) + A * (pv_n - (p-)), where A = [(v+ - v-)/(p+ - p-)] (local slope)
!      5) v_n is now the look-up value for a given pv_n = [0;1], and the pv_n's 
!         are equally spaced whereas the v_n's are not.
!         The CDF function is now inverted.
!
!      SO:
!
    j = 0                                                                       ! force first hunt to use binary search
    do i = 1,tsize-1
      pv_n = (i-1.)/(tsize-1.)                                                  ! 1) Find equally spaced pv_n.

      call hunt (pv_i,pv_n,j,tsize)                                             ! 2) Find bracketing pv_i's call them p+ and p-
      j = j+1                                                                   !  (no max...no security)

      if((pv_i(j).ge.pv_n.and.pv_i(j-1).le.pv_n).or.j.eq.tsize) then            ! 3) Find p+ and p- 
        pv_plus = pv_i(j)
        pv_minus = pv_i(j-1)
        v_plus = v_i(j)                                                         ! 3a) Find corresponding v+ and v- 
        v_minus = v_i(j-1)
                                                                                ! 4) Now produce v_n = (v-) + slope * (pv_n - (p-)), 
                                                                                ! slope = [(v+  -  v-)/(p+  -  p-)] (1st ord interpol.)
        slope = (v_plus - v_minus) / ((pv_plus - pv_minus) + 1.-epsilon(1.))
        slope = (v_plus - v_minus) / (pv_plus - pv_minus + tiny(0.))
        v_n = v_minus + slope * (pv_n - pv_minus)
                                                                                ! In this table, row p is the probability btw [0;1] to be chosen at random
                                                                                ! and row v is the corresponding isp-species velocity.
        tab(isp)%pv(i) = pv_n
        tab(isp)%v(i) = v_n                                                     ! isp for species v for lookup value of pv_n
        jsave = j-1                                                             ! for shortening the search loop...
      else
        if (stdout.ge.0)  write(stdout,*) 'tsizesearch failed', &
                  pv_n,j,i,pv_i(j-1),pv_i(j),pv_i(j+1) ! This should not happen
        call stop_program(.false.)
      endif
     !if (i<10.or.i>tsize-10) print'(i5,2(1pe15.7))',i,v_n,pv_n
    enddo
                                                                                !    END POINT (cheating)
    tab(isp)%pv(tsize) = 1.                                                     ! Pmax
    tab(isp)%v(1) = tab(isp)%vmin                                               ! should be, except for dbl -> real
    
    if(tab(isp)%set_vmax) then
      tab(isp)%v(tsize) = tab(isp)%vmax                                         ! Use max end point. (good for spatial distri.,
                                                                                ! not good for momentum/temper. assignment)
    else 
      tab(isp)%v(tsize) = tab(isp)%v(tsize-1)                                   ! Set Pmax P(Last good point) so that P(x)=const
                                                                                ! beyond x(where P=pmax). See comment in header.
    endif
                                                                                !-------------------------------------------------------------------
    do i=1,tsize                                                                ! Sanity check of table
      !if (isnan(tab(isp)%v(i))             .or. &
      if (&
          tab(isp)%v(i) .lt. tab(isp)%vmin .or. &
          tab(isp)%v(i) .gt. tab(isp)%vmax) then
        write(stdout,'(a,i3,i5,1p,3e15.7)') "Table is not sane:", &
          isp,i,tab(isp)%vmin, tab(isp)%v(i),tab(isp)%vmax
      endif
    enddo
  enddo

  call trace_exit('maketable')
END SUBROUTINE maketable
