! $Id $ 
! vim: nowrap
!=======================================================================
SUBROUTINE deposit_cached_fields(ix, iy, iz, isp, nion, nUx, nUy, nUz)
  USE params,               only : mcoord, nspecies, romp
  USE grid_m,               only : g
  USE interpolation_cached, only : slc, suc, d, v
  USE species,              only : lfields
  implicit none
  integer :: ix, iy, iz, isp
  real, dimension(g%n(1),g%n(2),g%n(3),nspecies) :: nion, nUx, nUy, nUz
  !
  integer :: jx, jy, jz, kx, ky, kz
  !
  ! romp is the OMP thread number. First thread copies to the global copy,
  ! the other threads to local copies. Everything is summed in the end
  !
  if (romp==1) then
     do kz=slc,suc                                                      ! copy cached fluxes to fields
       jz=kz+iz
       if (jz <= 0 .or. jz > g%n(3)) cycle
       do ky=slc,suc
         jy=ky+iy
         if (jy <= 0 .or. jy > g%n(2)) cycle
         do kx=slc,suc
           jx=kx+ix
           if (jx <= 0 .or. jx > g%n(1)) cycle
           nion(jx,jy,jz,isp) = nion(jx,jy,jz,isp) + d(kx,ky,kz)
            nUx(jx,jy,jz,isp) =  nUx(jx,jy,jz,isp) + v(kx,ky,kz,1)
            nUy(jx,jy,jz,isp) =  nUy(jx,jy,jz,isp) + v(kx,ky,kz,2)
            nUz(jx,jy,jz,isp) =  nUz(jx,jy,jz,isp) + v(kx,ky,kz,3)
         enddo
       enddo
     enddo
  else
     do kz=slc,suc                                                      ! copy cached fluxes to fields
       jz=kz+iz
       if (jz <= 0 .or. jz > g%n(3)) cycle
       do ky=slc,suc
         jy=ky+iy
         if (jy <= 0 .or. jy > g%n(2)) cycle
         do kx=slc,suc
           jx=kx+ix
           if (jx <= 0 .or. jx > g%n(1)) cycle
           lfields(jx,jy,jz,isp,romp)%d    = lfields(jx,jy,jz,isp,romp)%d    + d(kx,ky,kz)
           lfields(jx,jy,jz,isp,romp)%v(1) = lfields(jx,jy,jz,isp,romp)%v(1) + v(kx,ky,kz,1)
           lfields(jx,jy,jz,isp,romp)%v(2) = lfields(jx,jy,jz,isp,romp)%v(2) + v(kx,ky,kz,2)
           lfields(jx,jy,jz,isp,romp)%v(3) = lfields(jx,jy,jz,isp,romp)%v(3) + v(kx,ky,kz,3)
         enddo
       enddo
     enddo
  endif
END SUBROUTINE deposit_cached_fields
!=======================================================================
