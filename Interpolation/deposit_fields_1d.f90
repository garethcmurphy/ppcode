! $ Id $
! vim: nowrap
!-----------------------------------------------------------------------
! Copy the local fluxes stored in the cached fields d, v back to global (thread-local) arrays
!=======================================================================
SUBROUTINE deposit_cached_fields(ix, iy, iz, isp, nion, nUx, nUy, nUz)
  USE params,               only : mcoord, nspecies, romp
  USE grid_m,               only : g
  USE interpolation_cached, only : slc, suc, d, v
  USE species,              only : lfields
  implicit none
  integer :: ix, iy, iz, isp
  real, dimension(g%n(1),g%n(2),g%n(3),nspecies) :: nion, nUx, nUy, nUz
  !
  integer :: jx, kx
  !
  ! romp is the OMP thread number. First thread copies to the global copy,
  ! the other threads to local copies. Everything is summed in the end
  !
  if (romp==1) then
    do kx=slc,suc
      jx=kx+ix
      if (jx <= 0 .or. jx > g%n(1)) cycle
      nion(jx,iy,iz,isp) = nion(jx,iy,iz,isp) + d(kx)
       nUx(jx,iy,iz,isp) =  nUx(jx,iy,iz,isp) + v(kx,1)
       nUy(jx,iy,iz,isp) =  nUy(jx,iy,iz,isp) + v(kx,2)
       nUz(jx,iy,iz,isp) =  nUz(jx,iy,iz,isp) + v(kx,3)
    enddo
  else
    do kx=slc,suc
      jx=kx+ix
      if (jx <= 0 .or. jx > g%n(1)) cycle
      lfields(jx,iy,iz,isp,romp)%d    = lfields(jx,iy,iz,isp,romp)%d    + d(kx)
      lfields(jx,iy,iz,isp,romp)%v(1) = lfields(jx,iy,iz,isp,romp)%v(1) + v(kx,1)
      lfields(jx,iy,iz,isp,romp)%v(2) = lfields(jx,iy,iz,isp,romp)%v(2) + v(kx,2)
      lfields(jx,iy,iz,isp,romp)%v(3) = lfields(jx,iy,iz,isp,romp)%v(3) + v(kx,3)
    enddo
  endif
END SUBROUTINE deposit_cached_fields
!=======================================================================
