! vim: nowrap
!=====================================================================!
SUBROUTINE e_boundaries(ex,ey,ez)
!----------------------------------------------------------------------!
!=> GENERALLY BOUNDARY CONDITIONS FOR E-FIELD MUST SUPPLY:             !
!   !Ex for x={flb-1},{fub  }, y={flb-1},{fub+1}, z={flb-1},{fub+1}    !
!   !Ey for x={flb-1},{fub+1}  y={flb-1},{fub  }, z={flb-1},{fub+1}    !
!   !Ez for x={flb-1},{fub+1}, y={flb-1},{fub+1}, z={flb-1},{fub  }    !
!Such that:                                                            !
!   !Ex is defined on x={flb-1:fub  }, y={flb-1:fub+1}, z={flb-1:fub+1}!
!   !Ey is defined on x={flb-1:fub+1}, y={flb-1:fub  }, z={flb-1:fub+1}!
!   !Ez is defined on x={flb-1:fub+1}, y={flb-1:fub+1}, z={flb-1:fub  }!
!----------------------------------------------------------------------!
  USE grid_m, ONLY : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: ex,ey,ez

!SET XYZ-PERIODIC BOUNDARY CONDITIONS ON Ex,Ey and Ez.

  call overlap(ex)
  call overlap(ey)
  call overlap(ez)
     !=> Ex is now defined on {flb-1:fub  }{flb-1:fub+1}{flb-1:fub+1}
     !=> Ey is now defined on {flb-1:fub+1}{flb-1:fub  }{flb-1:fub+1}
     !=> Ez is now defined on {flb-1:fub+1}{flb-1:fub+1}{flb-1:fub  }
END SUBROUTINE e_boundaries

!=======================================================================
SUBROUTINE j_boundaries_noadd(jx,jy,jz)
  USE grid_m, only: g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: jx, jy, jz
!-----------------------------------------------------------------------
! The physical BCs for the electric current.
!-----------------------------------------------------------------------
  call overlap(jx)
  call overlap(jy)
  call overlap(jz)
END SUBROUTINE j_boundaries_noadd

!=======================================================================
SUBROUTINE j_boundaries(jx,jy,jz)
  USE grid_m, only: g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: jx, jy, jz
!-----------------------------------------------------------------------
! The physical BCs for the electric current.
!-----------------------------------------------------------------------
  call overlap_add(jx)
  call overlap_add(jy)
  call overlap_add(jz)
END SUBROUTINE j_boundaries

!=======================================================================
SUBROUTINE charge_boundaries_noadd(charge)
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: charge

  call overlap(charge)
END SUBROUTINE charge_boundaries_noadd

!=======================================================================
SUBROUTINE charge_boundaries(charge)
  USE grid_m, only: g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: charge

  call overlap_add(charge)
END SUBROUTINE charge_boundaries

!=====================================================================!
SUBROUTINE b_boundaries(bx,by,bz)

!---------------------------------------------------------------------!
!=> GENERALLY BOUNDARY CONDITIONS FOR B-FIELD MUST SUPPLY:            !
!     !Bx for x={flb-1},{fub+1}, y={flb-1},{fub  }, z={flb-1},{fub  } !
!     !By for x={flb-1},{fub  }  y={flb-1},{fub+1}, z={flb-1},{fub  } !
!     !Bz for x={flb-1},{fub  }, y={flb-1},{fub  }, z={flb-1},{fub+1} !
!Such that:                                                           !
!     !Bx defined on x={flb-1:fub+1}, y={flb-1:fub},   z={flb-1:fub}  !
!     !By defined on x={flb-1:fub},   y={flb-1:fub+1}, z={flb-1:fub}  !
!     !Bz defined on x={flb-1:fub},   y={flb-1:fub},   z={flb-1:fub+1}!
!---------------------------------------------------------------------!

  USE grid_m, ONLY : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: bx
  real, dimension(g%n(1),g%n(2),g%n(3)) :: by
  real, dimension(g%n(1),g%n(2),g%n(3)) :: bz

!SET XYZ-PERIODIC BOUNDARY CONDITIONS ON Bx,By and Bz.

  call overlap(bx)
  call overlap(by)
  call overlap(bz)

END SUBROUTINE b_boundaries
!=====================================================================!
SUBROUTINE phi_boundaries(phi)
  USE grid_m, ONLY : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: phi

!SET BOUNDARY CONDITIONS ON 'PHI' -- phi is needed on [flb,fub+1], and
!is known on [flb+1,fub]. 

  call overlap(phi)

END SUBROUTINE phi_boundaries
!=====================================================================!
SUBROUTINE phib_boundaries(phi)
  USE grid_m, ONLY : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: phi

!SET BOUNDARY CONDITIONS ON 'PHI' -- phi is needed on [flb,fub+1], and
!is known on [flb+1,fub]. 

  call overlap(phi)

END SUBROUTINE phib_boundaries

!--------------------------------------------------------------------------!
! THE BOUNDARY CONDITIONS ARE TRICKY AT BEST
!
!       z
!       ^
!       |
!       Ez            Bx
!       |
!       |
!    By |
!       |
!       |
!       |
!       *-------------Ey---> y
!      /
!     /
!    Ex             Bz
!   /
!  x
!
!  we need, so to speak, one extra grid point at the
!    min location. There only Bt and En are of
!    relevance and need to be supplied via bc.
!  at the max location, again bc need to be supplied only for Bt and En
!    these have now the same grid index as the outermost cell
!    Bn can be calculated from the existing info, and is
!    already calculated at the min boundaries (the ghost cell is irrelevant)
!
!  sx and sy are half point centered, as bx and by
!----------------------------------------------------------------------!
!=======================================================================
SUBROUTINE impose_particle_boundaries
  implicit none
  ! No special boundaries. SendParticles takes care of sending between mpi-threads
END SUBROUTINE impose_particle_boundaries
!=======================================================================
SUBROUTINE impose_source_boundaries
!-----------------------------------------------------------------------
! Nothing to do. Overlap is done on EM fields and at I/O not on fields%...
!-----------------------------------------------------------------------
  implicit none
END SUBROUTINE impose_source_boundaries
!=======================================================================
SUBROUTINE ddj_boundaries(n,u,v,q,dd,dir,isp)
  implicit none
  integer, intent(in) :: n, dir, isp
  real,    intent(in) :: dd(3)
  real                :: v(n-2,2), q(2,2)
  real, target        :: u(n-2,2)
!-----------------------------------------------------------------------
! Peridic case
!-----------------------------------------------------------------------
  u(1,1) = dd(3);   u(1,2) = dd(2);   u(2,2) = dd(3)
  v(1,1) = dd(3);   v(1,2) = dd(2);   v(2,2) = dd(3)
END SUBROUTINE ddj_boundaries
