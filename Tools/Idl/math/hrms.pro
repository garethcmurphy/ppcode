function hrms,f,xvertical=xvertical,yvertical=yvertical,zvertical=zvertical,haverage=haverage, array=array
;
;  Horizontal rms of 3-d scalar
;
    COMMON cvertxy, cxvert, cyvert

    if          keyword_set(xvertical) then begin
      cxvert=1
      cyvert=0
    end else if keyword_set(yvertical) then begin
      cxvert=0
      cyvert=1
    end else if keyword_set(zvertical) then begin
      cxvert=0
      cyvert=0
    end

    s=size(f)
    if s(0) eq 2 then begin
      h=fltarr(s(1))
      haverage=h
      for n=0,s(1)-1 do begin
        h(n)=rms(f(n,*),aver=aver)
        haverage(n)=aver
      end
    end else if keyword_set(cxvert)  then begin
      h=fltarr(s(1))
      haverage=h
      for n=0,s(1)-1 do begin
        h(n)=rms(f(n,*,*),aver=aver)
        haverage(n)=aver
      end
      if keyword_set(array) then h=rebin(reform(h,s(1),1,1),s(1),s(2),s(3))
    end else if keyword_set(cyvert)  then begin
      h=fltarr(s(2))
      haverage=h
      for n=0,s(2)-1 do begin
        h(n)=rms(f(*,n,*),aver=aver)
        haverage(n)=aver
      end
      if keyword_set(array) then h=rebin(reform(h,1,s(2),1),s(1),s(2),s(3))
    end else begin
      h=fltarr(s(3))
      haverage=h
      for n=0,s(3)-1 do begin
        h(n)=rms(f(*,*,n),aver=aver)
        haverage(n)=aver
      end
      if keyword_set(array) then h=rebin(reform(h,1,1,s(3)),s(1),s(2),s(3))
    end
    return,h
end
