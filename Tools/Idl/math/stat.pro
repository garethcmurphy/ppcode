FUNCTION rms_stat,f,normalize=normalize,average=average
	n=n_elements(f)
	average=aver(f)
	if keyword_set(normalize) then $
		return,sqrt(aver((f-average)^2))/average $
	else $
		return,sqrt(aver((f-average)^2))
END

;+
; $Id: stat.pro,v 1.7 2011/08/11 18:16:48 aake Exp $
 PRO stat,a,imin=imin,imax=imax,norms=norms,quiet=quiet,cm=cm,label=label
; NAME:
;       STAT
;
; FUNCTION:
;       Calculate the mean value, root mean square deviation and extrema of
;       a multi- (max 4) dimensional array.
;
; CALLING SEQUENCE
;       STAT, A [, IMIN=IMIN, IMAX=IMAX]
;
; INPUTS:
;       A:      input array
;
; OUTPUT:
;       Prints out results
;
; OPTIONAL PARAMETERS:
;       IMIN:   receives indices to the minimum value
;       IMAX:   receives indices to the maximum value
;-
        on_error, 2     ; return to caller

        if keyword_set(cm) then begin
           s=size(a)
           if s[0] eq 3 then begin
             stat,a(4:s[1]-5,*,*)
             return
           end
         end

        s=size(a)
        f1=min(a,kmin)
        f2=max(a,kmax)

	if abs(f2)+abs(f1) gt 1e18 then fact=1e-18 else fact=1.
	if fact ne 1. then a=a*fact

        imin=lonarr(s[0])
        imax=lonarr(s[0])

        m=1     & for i=1,s[0] do m=m*s[i]

        for i=s[0],1,-1 do begin
                m=m/s[i]
                imin(i-1)=long(kmin/m)  & kmin=kmin-imin(i-1)*m
                imax(i-1)=long(kmax/m)  & kmax=kmax-imax(i-1)*m
        end

        fa=0.0
        if not keyword_set(norms) then fr=rms_stat(a,aver=fa) else fa=aver(a)
        if abs(fa) lt 1.e-30 then fa=1.e-30

	if fact ne 1. then begin
	  fa=fa/fact
	  fr=fr/fact
	end

        if keyword_set(quiet) then return

        if keyword_set(label) then $
	  print,format='(a19,a,g13.4)',label,'     aver     =',fa $
	else $
	  print,format='(2(a,1x,g12.4,5x))','aver =',fa
        if not keyword_set(norms) then print,format='(2(a,1x,g12.4,5x))','rms  =',fr,'rms/aver =',fr/fa
        print,format='(2(a,1x,g12.4,5x),2x,5i6)','min  =',f1,'min/aver =',$
                f1/fa,imin
        print,format='(2(a,1x,g12.4,5x),2x,5i6)','max  =',f2,'max/aver =',$
                f2/fa,imax
END
