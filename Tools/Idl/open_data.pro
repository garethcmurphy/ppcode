;+
; IDL> open_data, 'fields-000000.dat'    ,   $        ; file name (fields or particles)
;                 structure = s          ,   $        ; parameters returned
;               [ data      =  '../Data' , ] $        ; data directory
;               [ verbose   = 0          , ]          ; noise level
;
; $Id: open_data.pro,v 1.35 2012/04/04 19:29:52 gbaumann Exp $
;-----------------------------------------------------------------------
FUNCTION int2char, int
  ; convert integer arr to byte arr
  byt = bytarr(n_elements(int)*4)
  byt[0:*:4] = byte(int)
  byt[1:*:4] = byte(int/2l^8)
  byt[2:*:4] = byte(int/2l^16)
  byt[3:*:4] = byte(int/2l^24)
  return, string(byt)
END

;-----------------------------------------------------------------------
PRO read_data, unit, reals=reals, integers=integers, verbose=verbose
  type=0L
  length=0L
  readu,unit,length,type
  if (verbose gt 3) then help,length,type
  if type eq 1 then begin
    if (verbose gt 3) then print,length,' integers being read'
    integers=lonarr(length)
    readu,unit,integers
    if (verbose gt 4) then print,integers
  end
  if type eq 2 then begin
    if (verbose gt 3) then print,length,' reals being read'
    reals=fltarr(length)
    readu,unit,reals
    if (verbose gt 4) then print,reals
  end
  if type eq 3 then begin
  ;  length=length<76
    if (verbose gt 3) then print,length,' reals being read'
    reals=fltarr(length)
    readu,unit,reals
    if (verbose gt 4) then print,reals
    if (verbose gt 3) then print,length,' integers being read'
    integers=lonarr(length)
    readu,unit,integers
    if (verbose gt 4) then print,integers
  end
  return
END

;-----------------------------------------------------------------------
PRO open_data, file, u, datadir=datadir, verbose=verbose, $
    structure=structure, position=position, _extra=_extra
  ;default,datadir,'.'+path_sep()
  default,datadir,'Data'
  default,file,'fields-000000.dat'
  default,verbose,0
  dirfile=datadir+path_sep()+file
  if (not fexists(dirfile)) then begin
    print,dirfile,' cannot be opened'
  end else if (verbose gt 2) then begin
    print,'reading from ',dirfile
  end

  if n_elements(u) gt 0 then free_lun,u
  openr,u,/get,dirfile,/f77,_extra=_extra
  fmt=1L
  mid=60L
  n=0L
  a=0L
  mpi=0L

  units     = {l:1., t:1., d:1., $
               m:1., v:1., e:1., $
               p:1., f:1., b:1., $
	       name:'identity'}
  constants = {k_b:1.,planck:1., fourpi:1., $
               elm_q:1., name:'identity'}
  elm       = {ke:1., kb:1., kf:1.}

  while (not eof(u)) do begin
    readu,u,fmt,mid
    if (verbose gt 0) then print,'Reading format type',fmt
    if (fmt le 0) then goto, fin
    id=bytarr(mid)
    readu,u,id
    id = string(id)
    if verbose gt 2 then print,id
    i=strpos(id,'.')
    i0=strpos(id,'/')+1
    module=strmid(id,i0,i-i0)
    readu,u,n
    if (verbose gt 3) then print,'Read module = ',module
    case module of
      'io': begin
        read_data,u,verbose=verbose,reals=ptime
        read_data,u,verbose=verbose,integers=indices
        if (n_elements(ids) eq 0) then ids={io:id} $
		else ids=create_struct('io',id,ids)
        if (n_elements(format) eq 0) then format={io:fmt} $
		else format=create_struct('io',fmt,format)
      end
      'params': begin
        read_data,u,verbose=verbose,integers=mpi
        read_data,u,verbose=verbose,integers=dims
        read_data,u,verbose=verbose,integers=ipar
        read_data,u,verbose=verbose,reals=rpar
        read_data,u,verbose=verbose,integers=lpar1
        read_data,u,verbose=verbose,integers=lpar2
        read_data,u,verbose=verbose,integers=lpar3
        if (fmt gt 1) then run_id  = dims[6]   else run_id  = -1l
        if (fmt gt 6) then do_vth  = lpar1[21] else do_vth  = 0
        if (n_elements(ids) eq 0) then ids={params:id} $
		else ids=create_struct('params',id,ids)
        if (n_elements(format) eq 0) then format={params:fmt} $
		else format=create_struct('params',fmt,format)
      end
      'species': begin
        read_data,u,verbose=verbose,reals=rspec,int=ispec
        pname = strarr(n_elements(ispec)/18)
        for i=0,n_elements(pname)-1 do pname[i]=int2char(ispec(14+i*18:17+i*18))
        if (n_elements(ids) eq 0) then ids={species:id} $
		else ids=create_struct('species',id,ids)
        if (n_elements(format) eq 0) then format={species:fmt} $
		else format=create_struct('species',fmt,format)
      end
      'grid': begin
        read_data,u,verbose=verbose,reals=rgrid,integers=igrid
        if (n_elements(ids) eq 0) then ids={grid:id} $
		else ids=create_struct('grid',id,ids)
        if (n_elements(format) eq 0) then format={grid:fmt} $
		else format=create_struct('grid',fmt,format)
      end
      'units': begin
        read_data,u,verbose=verbose,reals=runits,integ=iunits
        if (n_elements(ids) eq 0) then ids={units:id} $
		else ids=create_struct('units',id,ids)
        if (n_elements(format) eq 0) then format={units:fmt} $
		else format=create_struct('units',fmt,format)
        if (fmt ge 2) then begin
        endif else begin
          units     = {l:runits[0], t:runits[1], d:runits[2], $
		               m:runits[3], v:runits[4], e:runits[5], $
                       p:runits[6], f:runits[7], b:runits[8], $
					   name:int2char(iunits[9:12])}
          constants = {k_b:runits[17],planck:runits[18],fourpi:runits[22], $
		               elm_q:runits[26],name:int2char(iunits[34:37])}
          elm       = {ke:runits[38], kb:runits[39], kf:runits[40]}
        endelse
        if (fmt gt 1) then elm = $
		  create_struct(elm, 'name', int2char(iunits[41:44]))
      end
      'stat': begin
        read_data,u,verbose=verbose,reals=rstat
        if (n_elements(ids) eq 0) then ids={stat:id} $
		else ids=create_struct('stat',id,ids)
        if (n_elements(format) eq 0) then format={stat:fmt} $
		else format=create_struct('stat',fmt,format)
      end
      'slice': begin
        read_data,u,verbose=verbose,reals=rslice,integ=islice
        if (n_elements(ids) eq 0) then ids={slice:id} $
		else ids=create_struct('slice',id,ids)
      end
      'synthetic_spectra': begin
    	read_data,u,verbose=verbose,integ=ifname & fname=int2char(ifname)
    	read_data,u,verbose=verbose,integ=isynth_spectra
    	read_data,u,verbose=verbose,reals=rsynth_spectra
    	read_data,u,verbose=verbose,integ=lsynth_spectra
    	read_data,u,verbose=verbose,reals=obs_synth_spectra
        if (n_elements(ids) eq 0) then ids={synthetic_spectra:id} $
		else ids=create_struct('synthetic_spectra',id,ids)
        if (n_elements(format) eq 0) then format={synthetic_spectra:fmt} $
		else format=create_struct('synthetic_spectra',fmt,format)
      end
      else: begin
        print,'unknown module ',module
        print,'cvs id   : ',id
        print,'io format: ',fmt
        for i=1,n do readu,u,a
      end
    end
    position=(fstat(u)).cur_ptr
  end

fin:
  position=(fstat(u)).cur_ptr

  if(n_elements(pname) gt 0) then begin
  structure={                $
      run_id:run_id         ,$
         ids:ids            ,$
      format:format         ,$
       nodes:mpi[0]         ,$
        rank:mpi[1]         ,$
    periodic:mpi[5:7]       ,$
        time:rpar[0]        ,$
          dt:rpar[1]        ,$
         Cdt:rpar[5]        ,$
       ptime:ptime[0]       ,$
     indices:indices[0]     ,$
    nspecies:ipar[2]        ,$
        mdim:dims[0]        ,$
      mcoord:dims[1]        ,$
  do_lorentz:lpar1[0]       ,$
  do_maxwell:lpar1[8]       ,$
      do_vth:do_vth         ,$
         tnp:ispec[0:*:18]  ,$
        imax:ispec[2:*:18]  ,$
        mass:rspec[4:*:18]  ,$
      charge:rspec[5:*:18]  ,$
    lifetime:rspec[6:*:18]  ,$
 temperature:rspec[7:*:18]  ,$
  min_weight:ispec[8:*:18]  ,$
          mp:ispec[9:*:18]  ,$
          np:ispec[10:*:18] ,$
        nmax:ispec[11:*:18] ,$
        nmin:ispec[12:*:18] ,$
     ntarget:ispec[13:*:18] ,$
       pname:pname          ,$
           n:igrid[0:2]     ,$
          lb:igrid[3:5]     ,$
          ub:igrid[6:8]     ,$
          gs:rgrid[9:11]    ,$
           s:rgrid[12:14]   ,$
          ds:rgrid[15:17]   ,$
          gn:igrid[18:20]   ,$
      offset:igrid[21]      ,$
         rlb:rgrid[22:24]   ,$
         rub:rgrid[25:27]   ,$
        grlb:rgrid[28:30]   ,$
        grub:rgrid[31:33]   ,$
          gV:rgrid[34]      ,$
           V:rgrid[35]      ,$
          dV:rgrid[36]      ,$
       units:units          ,$
   constants:constants      ,$
         elm:elm             $
   }
   endif else begin
  structure={                $
      run_id:run_id         ,$
         ids:ids            ,$
      format:format         ,$
       nodes:mpi[0]         ,$
        rank:mpi[1]         ,$
    periodic:mpi[5:7]       ,$
        time:rpar[0]        ,$
          dt:rpar[1]        ,$
         Cdt:rpar[5]        ,$
       ptime:ptime[0]       ,$
     indices:indices[0]     ,$
    nspecies:ipar[2]        ,$
        mdim:dims[0]        ,$
      mcoord:dims[1]        ,$
  do_lorentz:lpar1[0]       ,$
  do_maxwell:lpar1[8]       ,$
      do_vth:do_vth         ,$
           n:igrid[0:2]     ,$
          lb:igrid[3:5]     ,$
          ub:igrid[6:8]     ,$
          gs:rgrid[9:11]    ,$
           s:rgrid[12:14]   ,$
          ds:rgrid[15:17]   ,$
          gn:igrid[18:20]   ,$
      offset:igrid[21]      ,$
         rlb:rgrid[22:24]   ,$
         rub:rgrid[25:27]   ,$
        grlb:rgrid[28:30]   ,$
        grub:rgrid[31:33]   ,$
          gV:rgrid[34]      ,$
           V:rgrid[35]      ,$
          dV:rgrid[36]      ,$
       units:units          ,$
   constants:constants      ,$
         elm:elm             $
   }
   endelse
   position = position + structure.nspecies*8 + 8
   if keyword_set(verbose) then print,'position = ',strtrim(string(position),2)

END
