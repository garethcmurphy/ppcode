; ----------------------------------------------------------------------
; small routine for reading 16bit integer data

; ----------------------------------------------------------------------
pro header_size,o,lun,ww,_extra=_extra
  fs=fstat(lun)
  off=fs.cur_ptr
  openr,u,/get,o.file,_extra=_extra
  a1=assoc(u,lonarr  (1),off) & l1=a1[0]
  a2=assoc(u,lon64arr(1),off) & l3=a2[0]
  if off+l1+4 lt fs.size then begin
    a=assoc(u,lonarr(1),off+l1+4)
    l2=a[0]
  end else begin
    l2=0L
  end
  if off+l3+8 lt fs.size then begin
    a=assoc(u,lon64arr(1),off+l3+8)
    l4=a[0]
  end else begin
    l4=0L
  end
  free_lun,u
  l=0
  if l1 eq l2 and l1 gt 0 then l=4
  if l3 eq l4 and l3 gt 0 then l=8
  if l eq 0 then begin
    print,'cannot determine header size'
    stop
  end
  ww=l
end

; ----------------------------------------------------------------------
function rf2,f,iz,verbose=verbose,_extra=_extra
  default,iz,0
  gn = f.s.gn
  if (f.s.format.params ge 10) then begin
    w=where(f.s.periodic eq 0,nw)
    if nw gt 0 then gn[w] += 1                                                ; add physical bndry point if needed
  end
  mx=gn[0]
  my=gn[1]
  lxy=4L*mx*my
  openr,u,/get,f.o.file,_extra=_extra
  a=assoc(u,fltarr(mx,my),f.o.bx+iz*lxy) & bx=a[0]
  a=assoc(u,fltarr(mx,my),f.o.by+iz*lxy) & by=a[0]
  a=assoc(u,fltarr(mx,my),f.o.bz+iz*lxy) & bz=a[0]
  a=assoc(u,fltarr(mx,my),f.o.ex+iz*lxy) & ex=a[0]
  a=assoc(u,fltarr(mx,my),f.o.ey+iz*lxy) & ey=a[0]
  a=assoc(u,fltarr(mx,my),f.o.ez+iz*lxy) & ez=a[0]
  d=fltarr(mx,my,f.s.nspecies)
  v=fltarr(mx,my,f.s.mcoord,f.s.nspecies)
  vt=fltarr(mx,my,f.s.nspecies)
  for j=0,f.s.nspecies-1 do begin
    a=assoc(u,fltarr(mx,my),f.o.d[j] +iz*lxy) & d[*,*,j]=a[0]
    if f.s.do_vth then begin 
      a=assoc(u,fltarr(mx,my),f.o.vt[j]+iz*lxy) & vt[*,*,j]=a[0]
    endif
    for i=0,f.s.mcoord-1 do begin
      a=assoc(u,fltarr(mx,my),f.o.v[i,j]+iz*lxy) & v[*,*,i,j]=a[0]
    endfor
  endfor
;  if keyword_set(swap_if_l) or keyword_set(swap_if_b) then begin
;    swap_endian_inplace,bx,swap_if_l=swap_if_l,swap_if_b=swap_if_b
;    swap_endian_inplace,by,swap_if_l=swap_if_l,swap_if_b=swap_if_b
;    swap_endian_inplace,bz,swap_if_l=swap_if_l,swap_if_b=swap_if_b
;    swap_endian_inplace,ex,swap_if_l=swap_if_l,swap_if_b=swap_if_b
;    swap_endian_inplace,ey,swap_if_l=swap_if_l,swap_if_b=swap_if_b
;    swap_endian_inplace,ez,swap_if_l=swap_if_l,swap_if_b=swap_if_b
;    swap_endian_inplace,d ,swap_if_l=swap_if_l,swap_if_b=swap_if_b
;    swap_endian_inplace,v ,swap_if_l=swap_if_l,swap_if_b=swap_if_b
;    if f.s.do_vth then $
;    swap_endian_inplace,vt,swap_if_l=swap_if_l,swap_if_b=swap_if_b
;  endif
  free_lun,u
  f={o:f.o,s:f.s,bx:bx,by:by,bz:bz,ex:ex,ey:ey,ez:ez,v:v,d:d,vth:vt}
  return,f
end

; ----------------------------------------------------------------------
pro read_int16, lun, var
  mn=0. & mx=0.
  ivar = uintarr(size(var,/dim),/NoZero)
  readu, lun, mn, mx
  readu, lun, ivar
  var = ivar*(mx-mn)/65535.+mn
end

; routine to read only a subsection of a chunk
; ----------------------------------------------------------------------
pro readu_sub, lun, f, s=s, verbose=verbose
  point_lun,-lun,pos1                                                      ; get position
  readu_skip,lun,s=s, counter=c1                                           ; test if file is corrupted, get counter type
  point_lun,lun,pos1                                                       ; go back to start position
  c2 = c1 & readu, lun, c1
  if (size(c1,/type) eq 3) then off=4ll else off=8ll                       ; get file offset size
  pos1 = pos1 + off

  ngb  = long64(s.ngb)
  ncut = long64(s.ncut)
  xr=long64(s.xrange)
  yr=long64(s.yrange)
  zr=long64(s.zrange)

  pos2 = pos1 + ngb[0]*ngb[1]*ngb[2]*4ll + off                             ; position at end of chunk

  if (zr[0] gt 0) then begin                                               ; advance filepointer to first xy-plane
    pos1 += zr[0]*ngb[0]*ngb[1]*4ll
    point_lun,lun,pos1
  endif

  read_done = 0b

  if (verbose gt 0) then t1 = systime(/s)

  ; I) cut-out is a set of xy-slices. Read continously
  if array_equal(ngb[0:1],ncut[0:1]) then begin                            ; we can read the whole chunk
    readu,lun,f
    readu,lun,c2
    read_done = 1b
    method = 1
  endif

  ; II) xy-slices are really small, read whole chunk and cut-out
  size = long64(ngb[0])*long64(ngb[1])*long64(ncut[2])*4ll
  if (size lt 100ll*1024ll^2 and not read_done) then begin                 ; a total z-chunk is less than 100mb, read all of it
    tmp = fltarr(ngb[0],ngb[1],ncut[2],/NoZero)
    readu,lun,tmp
    f = tmp[xr[0]:xr[1],yr[0]:yr[1],*]
    read_done = 1b
    method = 2
  endif 

  ; III) xy-slices are really small, but chunk is large. Read slice-by-slice
  size = long64(ngb[0])*long64(ngb[1])*4ll
  if (size lt 1ll*1024ll^2 and not read_done) then begin                  ; a total xy-plane is less than 1mb, read all of it
    tmp = fltarr(ngb[0],ngb[1],/NoZero)
    for iz=0,ncut[2]-1 do begin
      readu,lun,tmp
      f[*,*,iz] = tmp[xr[0]:xr[1],yr[0]:yr[1]]
    endfor
    read_done = 1b
    method = 3
  endif 

  ; IV) An x-slice X y-block is small, scan in z, while reading blocks 
  size = long64(ngb[0])*long64(ncut[1])*4ll
  xpencil = ngb[0] le (10+ncut[0])                                         ; check we anyway need almost a whole x-pencil
  if ((size lt 1ll*1024ll^2 or xpencil) and not read_done) then begin     ; a x-plane x y-block is less than 1mb, read all of it
    tmp = fltarr(ngb[0],ncut[1],/NoZero)
    offy = ngb[0] * yr[0] * 4ll                                            ; offset for part before y-block in an xy-plane
    pos1 += offy & if (offy gt 0) then point_lun, lun, pos1
    xytot = ngb[0]*ngb[1] * 4ll                                            ; size in bytes of a full xy-plane
    for iz=0,ncut[2]-1 do begin
      readu,lun,tmp                                                        ; read block
      f[*,*,iz] = tmp[xr[0]:xr[1],*]                                       ; copy in to array
      pos1 += xytot & point_lun,lun,pos1                                   ; advance to start of block in next xy-slice
    endfor
    read_done = 1b
    method = 4
  endif 
    
  ; V) Worst case. Read one x-pencil a time
  if (not read_done) then begin
    tmp = fltarr(ncut[0],/NoZero)
    offy = ngb[0] * yr[0] * 4ll                                            ; offset for part before y-block in an xy-plane
    offx = xr[0] * 4ll                                                     ; offset for part before y-block in an xy-plane
    pos1 += offx + offy & if (offx + offy gt 0) then point_lun, lun, pos1
    xytot = ngb[0]*ngb[1] * 4ll                                            ; size in bytes of a full xy-plane
    ytot  = ngb[0]* 4ll                                                    ; size in bytes of a full xy-plane
    for iz=0,ncut[2]-1 do begin
      poss = pos1
      for iy=0,ncut[1]-1 do begin
        readu,lun,tmp                                                      ; read vector
        f[*,iy,iz] = tmp                                                   ; copy in to array
        poss += ytot & if (iy lt ncut[1]-1) then point_lun,lun,poss        ; advance to start of vector in next x-pencil
      endfor
      pos1 += xytot & if (iz lt ncut[2]-1) then point_lun,lun,pos1         ; advance to start of vector in next xy-slice
    endfor
    read_done = 1b
    method = 5
  endif 

  if (verbose gt 0) then begin
    size = product(ncut) * 4ll / (1024.^2)
    t1 = systime(/s) - t1
    print, format='(a,f8.2,a)', 'Read data with ', $
	  size/t1, ' MB/s using method '+string(method)
  endif
    
  point_lun,lun,pos2                                                       ; set file pointer to start of next chunk
end

; small routine to skip over a chunk - check manually counters
; ----------------------------------------------------------------------
pro readu_skip,lun,s=s,use_counter=use_counter,counter=counter
 c1=0l & c2=0l
 point_lun,-lun,pos1
 pos1 = long64(pos1)
 readu,lun,c1
 ok = 1b
 counter = c1
 if (keyword_set(use_counter)) then begin
   point_lun, lun, pos1 + c1 + 4ll
   readu,lun,c2
   if (c1 ne c2) then begin                                                ; try with 64bit counters
     point_lun,lun,pos1
     c81=0ll & c82=0ll
     readu,lun,c81
     counter = c81
     point_lun, lun, pos1 + c81 + 8ll
     readu,lun,c82
     if (c81 ne c82) then ok = 0b
   endif
 endif else begin
   offset = product(long64(s.ngb))*4ll
   point_lun, lun, pos1 + offset + 4ll
   readu,lun,c2
   if (c1 ne c2) then begin                                                ; try with 64bit counters
     point_lun,lun,pos1
     c81=0ll & c82=0ll
     readu,lun,c81
     counter = c81
     point_lun, lun, pos1 + offset + 8ll
     readu,lun,c82
     if (c81 ne c82) then ok = 0b
     if (c81 eq c82 and c81 ne offset) then $
       print, 'Warning. 64bit filecounters match (c=',c81,'), but do not match chunk size=',offset
   endif
 endelse

 if (not ok) then begin
   print, 'Error in readu_skip'
   if (keyword_set(use_counter)) then $
     print, 'Happend when using the file counter' $
   else $
     print, 'Happend when reading a field block with dimensions', s.ngb
   print, 'File corrupted at position ', pos1
   print, 'Tried both 32 and 64bit counters, and got :'
   print, '32 bit :',c1, c2
   print, '64 bit :',c81, c82
   stop
 endif
end
; -------------------------------------------------------------------------
;+
; Options for rf:
; -------------------------------------------------------------------------
; default     : store the data in a structure
; commonb     : copy the data to the common block only
; snapnr      : snapshot number to load
; datadir     : directory where the data reside
; thread      : only read from one thread (may, or may not work)
; merge       : force merging of data from different thread (should be set by auto)
; params      : structure containing the header
; verbose     : debug level
; next        : get the snapshot with a valid filename closest to -- and proceeding -- that given by "snapnr".
; only_params : only pass the structure from open_data with the header, and return
; [xyz]range  : only read in a subsection of the field data
;
; If one of the following is set, only the fields indicated by switches are load.
; bfield    : load the magnetic field
; efield    : load the electric field
; density   : load the particle density fields
; velocity  : load the velocity fields
; vth       : load the thermal velocity
;
; Examples:
;  f = rf(0)                ; read all fields and store in structure
;  f = rf(0,/common)        ; read all fields and store in common block
;  f = rf(0,/b)             ; only read in the magntic field of snapshot 0
;  f = rf(0,/b,/e)          ; read the electric and magntic fields of snapshot 0
;  f = rf(1,/b,/e,/next)    ; read the electric and magntic fields of snapshot 1 or the first later time snapshot
;
; Extra keywords are passed on to open_data
;
; $Id: rf.pro,v 1.38 2012/04/04 22:50:51 gbaumann Exp $
; -------------------------------------------------------------------------
function rf,snapnr,datadir=datadir,thread=thread,merge=merge,next=next,        $
                   params=params,verbose=verbose,commonb=commonb,bfield=bfield,$
                   efield=efield,density=density,velocity=velocity,time=time,  $
                   delete=delete,vth=vth,only_params=only_params,int16=int16,  $
                   xrange=xrange,yrange=yrange,zrange=zrange,iz=iz,            $
                   partial_read=partial_read,_extra=_extra
common fields,bx,by,bz,ex,ey,ez,d,v
common cparams,s
default, verbose, 0
;-

if (n_params() lt 1) then begin & doc_library, 'rf' & return,0 & endif ; auto doc if no given snap
if keyword_set(time) then t0=systime(1)                                ; are we timing the read procedure?
if (snapnr lt 0) then begin & print, "No data with negative snapnr. Exiting." & return, 0 & endif
if n_elements(iz) gt 0 then begin
  f=rf(snapnr,datadir=datadir,/partial,_extra=_extra)
  f=rf2(f,iz,_extra=_extra)
  return,f
endif
if keyword_set(next) then begin
 nextsnap = 0
 names = get_snap_files('fields',snapnr,datadir=datadir,merge=merge,thread=thread,verbose=verbose,int16=int16)
 while (names[0] eq '') do begin
  names = get_snap_files('fields',snapnr,datadir=datadir,merge=merge,thread=thread,verbose=verbose,int16=int16)
  if (names[0] eq '') then begin
   print, "Cannot find data with snapnr="+strtrim(snapnr,1)+", keyword next is set -- trying to find next existing snapshot."
   snapnr   += 1
   nextsnap += 1
  endif else begin
   print, "Found & read next existing snapshot, snapname = ",names[0]
  endelse
  if (nextsnap eq 100) then begin                                         ; hack exit
   print,'No snapnumber within 100 steps from chosen one. Exiting.'
   print, "Cannot find data."
   return, 0
  endif
 endwhile
endif else begin
 names = get_snap_files('fields',snapnr,datadir=datadir,merge=merge,thread=thread,verbose=verbose,int16=int16)
 if (names[0] eq '') then begin
  datadir = 'Data'
  names = get_snap_files('fields',snapnr,datadir=datadir,merge=merge,thread=thread,verbose=verbose,int16=int16)
  if (names[0] eq '') then begin
   datadir = './'
   names = get_snap_files('fields',snapnr,datadir=datadir,merge=merge,thread=thread,verbose=verbose,int16=int16)
   if (names[0] eq '') then begin
    print, "Cannot find data!"
    return, 0
   endif
  endif
 endif
endelse
nchunks = n_elements(names)                                             ; nr of chunks to read

; open first chunk to get global lower boundary and to get nglobal
open_data,names[0],lun,/F77_UNFORMATTED,struct=s,datadir=datadir, $
  verbose=verbose,_extra=_extra
ptime=s.ptime & mdim=s.mdim & offset=s.offset & nglobal=s.mdim & n=s.n
mcoord=s.mcoord & nsp=s.nspecies & params=s & lb=s.lb & ub=s.ub &
do_em = s.do_lorentz or s.do_maxwell

if (s.format.io ge 8) then readu,lun,do_em,lb,ub                        ; vth properly supported

if (verbose gt 1) then begin
  print, "ptime,mdim,offset,nglobal,n,lb,ub,mcoord,nsp:", $
          ptime,mdim,offset,nglobal,n,lb,ub,mcoord,nsp
endif
free_lun,lun

if keyword_set(only_params) then begin
  params = s
  return, {s:s}
endif

if (n_elements(thread) gt 0) then nglobal=n

; Define the full arrays based on the global size
ngb = s.gn
if (s.format.params ge 10) then begin
  w=where(s.periodic eq 0,nw)
  if nw gt 0 then ngb(w) += 1                                                ; add physical bndry point if needed
endif

ncut = ngb

if (n_elements(xrange) ne 2) then $                                          ; check x-range
  xrange=[0,ngb[0]-1] $
else $
  ncut[0] = xrange[1] - xrange[0] + 1

if (n_elements(yrange) ne 2) then $                                          ; check z-range
  yrange=[0,ngb[1]-1] $
else $
  ncut[1] = yrange[1] - yrange[0] + 1

if (n_elements(zrange) ne 2) then $
  zrange=[0,ngb[2]-1] $
else $
  ncut[2] = zrange[1] - zrange[0] + 1

nlb = [ub(0)-lb(0),ub(1)-lb(1),ub(2)-lb(2)]
if (n_elements(thread) gt 0) then begin ngb = nlb & ncut= nlb & endif        ; only reading 1 chunk?
if (keyword_set(int16)) then ngb=nlb
all = not (keyword_set(bfield) or keyword_set(efield) or    $
           keyword_set(density) or keyword_set(velocity) or $
           keyword_set(vth) )                                                ; read all fields?

if (do_em and not keyword_set(partial_read)) then begin                      ; only read em fields if they are there
  if (all or keyword_set(bfield)) then begin
    lbx=fltarr(ncut,/NoZero) & lby=fltarr(ncut,/NoZero) & lbz=fltarr(ncut,/NoZero)
  endif
  if (all or keyword_set(efield)) then begin
    lex=fltarr(ncut,/NoZero) & ley=fltarr(ncut,/NoZero) & lez=fltarr(ncut,/NoZero)
  endif
endif

if (nsp gt 0 and not keyword_set(partial_read)) then begin
  if (all or (keyword_set(density))) then begin
    ld =fltarr([ncut,nsp],/NoZero)        & ld = reform(ld,[ncut,nsp],/overwrite)
  endif
  if (all or (keyword_set(velocity))) then begin
    lv =fltarr([ncut,mcoord,nsp],/NoZero) & lv = reform(lv,[ncut,mcoord,nsp],/overwrite)
  endif
  if ((all or (keyword_set(vth))) and s.do_vth ne 0) then begin
    lvth =fltarr([ncut,nsp],/NoZero) & lvth = reform(lvth,[ncut,nsp],/overwrite)
  endif
endif

if (keyword_set(partial_read)) then begin
  l1=0LL
  l2=lon64arr(mcoord,nsp)
  l3=lon64arr(nsp)
  o={bx:l1,by:l1,bz:l1,ex:l1,ey:l1,ez:l1,v:l2,d:l3,vt:l3,lun:lun,file:' '}
endif

; Loop over chunks and fuse stuff
skip = 0l
for ch=0,nchunks-1 do begin
  open_data,names[ch],lun,/F77_UNFORMATTED,struct=s,datadir=datadir, $
   delete=delete,verbose=verbose,_extra=_extra
  if keyword_set(partial_read) then begin
    openr,lun1,/get,datadir+'/'+names[ch],_extra=_extra
    o.lun=lun1
    o.file=datadir+'/'+names[ch]
  endif
  ptime=s.ptime & mdim=s.mdim & offset=s.offset & nglobal=s.mdim & n=s.n
  lb=s.lb & ub=s.ub & mcoord=s.mcoord & nsp=s.nspecies


  if (s.format.params gt 4) then begin
    if (s.format.io   ge 8) then readu,lun,do_em,lb,ub $                ; vth properly supported
                            else readu,lun,lb,ub                        ; we use the new io format
  endif

  if (nchunks eq 1) then begin                                          ; single node or np MPI
    s = create_struct('xrange', xrange, 'yrange', yrange, 'zrange', zrange, 'ngb', ngb, 'ncut', ncut, s)
    if (not keyword_set(int16) and not keyword_set(partial_read)) then begin ; use direct io for big chunks
      point_lun, -lun, pos_on_disk
      free_lun, lun
      openr, lun, /get, datadir+names[ch], _extra=_extra 
      point_lun, lun, pos_on_disk
    endif
    if (do_em) then begin
      if (all or keyword_set(bfield)) then begin
        if (keyword_set(int16)) then begin
          read_int16,lun,lbx & read_int16,lun,lby  & read_int16,lun,lbz
        endif else if keyword_set(partial_read) then begin
	  header_size,o,lun,ww,_extra=_extra
          o.bx = (fstat(lun)).cur_ptr+ww & readu,lun,skip
          o.by = (fstat(lun)).cur_ptr+ww & readu,lun,skip
          o.bz = (fstat(lun)).cur_ptr+ww & readu,lun,skip
        endif else begin
          readu_sub,lun,lbx,s=s,verbose=verbose  & readu_sub,lun,lby,s=s,verbose=verbose  & readu_sub,lun,lbz,s=s,verbose=verbose
        endelse
      endif else begin
        if (keyword_set(int16) or keyword_set(partial_read)) then begin
          if keyword_set(int16) then begin readu,lun,skip & readu,lun,skip & readu,lun,skip & endif
          readu,lun,skip & readu,lun,skip & readu,lun,skip
        endif else begin
          readu_skip,lun,s=s & readu_skip,lun,s=s & readu_skip,lun,s=s
        endelse
      endelse
      if (all or keyword_set(efield)) then begin
        if (keyword_set(int16)) then begin
          read_int16,lun,lex & read_int16,lun,ley  & read_int16,lun,lez
        endif else if keyword_set(partial_read) then begin
	  header_size,o,lun,ww,_extra=_extra
          o.ex = (fstat(lun)).cur_ptr+ww & readu,lun,skip
          o.ey = (fstat(lun)).cur_ptr+ww & readu,lun,skip
          o.ez = (fstat(lun)).cur_ptr+ww & readu,lun,skip
        endif else begin
          readu_sub,lun,lex,s=s,verbose=verbose  & readu_sub,lun,ley,s=s,verbose=verbose  & readu_sub,lun,lez,s=s,verbose=verbose
        endelse
      endif else begin
        if (keyword_set(int16) or keyword_set(partial_read)) then begin
          if keyword_set(int16) then begin readu,lun,skip & readu,lun,skip & readu,lun,skip & endif
          readu,lun,skip & readu,lun,skip & readu,lun,skip
        endif else begin
          readu_skip,lun,s=s & readu_skip,lun,s=s & readu_skip,lun,s=s
        endelse
      endelse
    endif
    if not keyword_set(partial_read) then vv=fltarr(ncut,/NoZero)
    for i=0,nsp-1 do begin
      if (all or keyword_set(density)) then begin
        if (keyword_set(int16)) then begin
          read_int16,lun,vv
          ld[*,*,*,i]=vv
        endif else if keyword_set(partial_read) then begin
	  header_size,o,lun,ww,_extra=_extra
          o.d[i] = (fstat(lun)).cur_ptr+ww & readu,lun,skip
        endif else begin
          readu_sub,lun,vv,s=s,verbose=verbose
          ld[*,*,*,i]=vv
        endelse
      endif else begin
        if (keyword_set(int16)) then begin
          readu,lun,skip
          readu,lun,skip
        endif else begin
          readu_skip,lun,s=s
        endelse
      endelse
      for j=0,mcoord-1 do begin
        if (all or keyword_set(velocity)) then begin
          if (keyword_set(int16)) then begin
            read_int16,lun,vv 
            lv[*,*,*,j,i]=vv
          endif else if keyword_set(partial_read) then begin
	    header_size,o,lun,ww,_extra=_extra
            o.v[j,i] = (fstat(lun)).cur_ptr+ww & readu,lun,skip
          endif else begin 
            readu_sub,lun,vv,s=s,verbose=verbose
            lv[*,*,*,j,i]=vv
          endelse
        endif else begin
          if (keyword_set(int16) or keyword_set(partial_read)) then begin
            if keyword_set(int16) then readu,lun,skip
            readu,lun,skip
          endif else begin
            readu_skip,lun,s=s
          endelse
        endelse
      endfor
      if (s.do_vth ne 0) then begin
        if (all or keyword_set(vth)) then begin
          if (keyword_set(int16)) then begin
            read_int16,lun,vv
            lvth[*,*,*,i]=vv
          endif else if keyword_set(partial_read) then begin
	    header_size,o,lun,ww,_extra=_extra
            o.vt[i] = (fstat(lun)).cur_ptr+ww & readu,lun,skip
          endif else begin
            readu_sub,lun,vv,s=s,verbose=verbose
            lvth[*,*,*,i]=vv
          endelse
        endif else begin
          if (keyword_set(int16) or keyword_set(partial_read)) then begin
            if keyword_set(int16) then readu,lun,skip
            readu,lun,skip
          endif else begin
            readu_skip,lun,s=s
          endelse
        endelse
      endif
    endfor
  endif else begin                                                      ; multiple nodes, MPI-data
    nlb = [ub(0)-lb(0),ub(1)-lb(1),ub(2)-lb(2)]                         ; local bndry idx different
    temp=fltarr(nlb,/NoZero)                                            ; redefine scratch arrays
    zl = 0L
    zu = ub(2)-lb(2)
    if (ch eq 0) then begin
      off = offset + lb(2)                                              ; was == 0
    endif    else begin
      off = offset + lb(2)                                              ; was +1 ... no longer
    endelse                                                             ; need for the "+1"
    if do_em then begin
      if (all or keyword_set(bfield)) then begin
        readu, lun, temp & lbx[*,*,off:off+zu-zl-1] = temp
        readu, lun, temp & lby[*,*,off:off+zu-zl-1] = temp
        readu, lun, temp & lbz[*,*,off:off+zu-zl-1] = temp
      endif else begin
        readu,lun,skip & readu,lun,skip & readu,lun,skip
      endelse
      if (all or keyword_set(efield)) then begin
        readu, lun, temp & lex[*,*,off:off+zu-zl-1] = temp
        readu, lun, temp & ley[*,*,off:off+zu-zl-1] = temp
        readu, lun, temp & lez[*,*,off:off+zu-zl-1] = temp
      endif else begin
        readu,lun,skip & readu,lun,skip & readu,lun,skip
      endelse
    endif
    if (verbose gt 1) then print,names[ch],ch,off
    for i=0,nsp-1 do begin
      if (all or keyword_set(density)) then begin
        readu,lun,temp & ld[*,*,off:off+zu-zl-1,i]=temp
      endif else readu,lun,skip
      for j=0,mcoord-1 do begin
        if (all or keyword_set(velocity)) then begin
          readu,lun,temp & lv[*,*,off:off+zu-zl-1,j,i]=temp
        endif else readu,lun,skip
      endfor
      if (s.do_vth ne 1) then begin
        if (all or keyword_set(vth)) then begin
          readu,lun,temp & lvth[*,*,off:off+zu-zl-1,i]=temp
        endif else readu,lun,skip
      endif
    endfor
  endelse
  free_lun, lun
endfor

if (verbose gt 0) then print,' time       =',string(ptime)
if keyword_set(partial_read) then begin
  return,{o:o,s:s}
endif
if (not keyword_set(commonb)) then begin
  exe = 'data = {'
  if (all or keyword_set(bfield))   and do_em      then exe = exe+'bx:lbx, by:lby, bz:lbz,'
  if (all or keyword_set(efield))   and do_em      then exe = exe+'ex:lex, ey:ley, ez:lez,'
  if (all or keyword_set(density))  and (nsp gt 0) then exe = exe+'d:ld,'
  if (all or keyword_set(velocity)) and (nsp gt 0) then exe = exe+'v:lv,'
  if (all or keyword_set(vth))      and s.do_vth   then exe = exe+'vth:lvth,'
  exe = exe + 's:s}'
  void = execute(exe)
  if keyword_set(time) then print,'time: ', systime(1)-t0,format='(a,f5.1)'
  return, data
endif
if (s.do_vth ne 0) then begin
  print, 'Warning from RF: No support for putting vth into the common block,'+ $
         ' please remove the commonb keyword from the call if you want to access vth'
endif
if (all or keyword_set(bfield)) then begin
  bx = temporary(lbx) & by = temporary(lby) & bz = temporary(lbz)
endif
if (all or keyword_set(efield)) then begin
  ex = temporary(lex) & ey = temporary(ley) & ez = temporary(lez)
endif
if (all or keyword_set(density))  then d = temporary(ld)
if (all or keyword_set(velocity)) then v = temporary(lv)
if (not keyword_set(commonb)) then begin
  if (all) then begin
    if keyword_set(time) then print,'time: ', systime(1)-t0,format='(a,f5.1)'
    return, {bx:bx,by:by,bz:bz,ex:ex,ey:ey,ez:ez,d:d,v:v,s:s}
  endif
  exe = 'data = {'
  if (keyword_set(bfield))   then exe = exe+'bx:bx, by:by, bz:bz, '
  if (keyword_set(efield))   then exe = exe+'ex:ex, ey:ey, ez:ez, '
  if (keyword_set(density))  then exe = exe+'d:d, '
  if (keyword_set(velocity)) then exe = exe+'v:v, '
  exe = exe + 's:s}'
  void = execute(exe)
  if keyword_set(time) then print,'time: ', systime(1)-t0,format='(a,f5.1)'
  return, data
endif
if keyword_set(time) then print,'wall time:', systime(1)-t0,format='(a,g6.1,a)', ' s'
return, 1
end
