; $Id$
;----------------------------------------------------------------------
; Read files prouced with "call dump (var, 'label')"
;----------------------------------------------------------------------
FUNCTION rd, name1, search, count, datadir=datadir, verbose=verbose, $
             prefix=prefix
  default, datadir, 'Data'
  default, search, 'void'
  default, count, 1
  default, verbose, 0
  rank = 0
  name = name1
  if n_elements(prefix) gt 0 then name=strtrim(prefix)+'-'+name
  file = strtrim(datadir)+'/'+strtrim(name)+'-'+string(rank,format='(i4.4)')+'.dmp'
  if verbose gt 0 then print, file
  if not exists(strtrim(file)) then begin
    print,strtrim(file)+' does not exist'
    return, 0.
  end
  openr, u, /get, file, /f77
  nodes=0L & rank=0L & time=0.0 & nlab=0L
  readu, u, nodes, rank, time, nlab
  label = bytarr(nlab)
  readu, u, label
  n=lonarr(3) & gn=n & o0=lonarr(3)
  readu, u, n, gn, o0
  if verbose gt 0 then begin
    print, '     nodes:',nodes
    print, 'dimensions:', gn
    print, '      time:', time
  end
  free_lun, u
  f = fltarr(gn[0],gn[1],gn[2])
  i = 0L
  for rank=0,nodes-1 do begin
    file = strtrim(datadir)+'/'+strtrim(name)+'-'+string(rank,format='(i4.4)')+'.dmp'
    if verbose gt 1 then print, file
    openr, u, /get, file, /f77
    void = lonarr(2)
    irec = 0
    while (1) do begin
      readu, u, void, time, nlab
      label = bytarr(nlab)
      readu, u, label
      if strtrim(label) eq strtrim(search) then begin
        i = i + 1
        if i eq count then break
      end
      if verbose gt 2 then begin
        n = lonarr(3) & gn=n & o1=n
        readu, u, n, gn, o1
        o = o1-o0
        chunk = fltarr(n[0],n[1],n[2])
        readu, u, chunk
        f[o[0]:o[0]+n[0]-1, $
          o[1]:o[1]+n[1]-1, $
          o[2]:o[2]+n[2]-1] = chunk
        print, rank, irec, time, min(f,max=max), max, ' '+strtrim(label)
      end else begin
        readu, u
        readu, u
      end
      if eof(u) then begin
        print, search+' not found'
        return, 0
      end
      irec = irec+1
    end
    n = lonarr(3) & gn=n & o1=n
    readu, u, n, gn, o1
    o = o1-o0
    chunk = fltarr(n[0],n[1],n[2])
    readu, u, chunk
    f[o[0]:o[0]+n[0]-1, $
      o[1]:o[1]+n[1]-1, $
      o[2]:o[2]+n[2]-1] = chunk
    print, rank, irec, time, min(f,max=max), max, ' '+strtrim(label)
    free_lun,u
  end
  return, f
END