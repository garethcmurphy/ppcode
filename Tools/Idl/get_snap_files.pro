; $Id: get_snap_files.pro,v 1.5 2012/04/11 21:51:10 aake Exp $
;-----------------------------------------------------------------------
function get_snap_files, type, snapnr, datadir=datadir, merge=merge, $
                         thread=thread, verbose=verbose, int16=int16
  default, datadir, 'Data'
  default, verbose, 0
  stime= string(format='(i6.6)',snapnr)
  fhead= datadir+path_sep()+type+'-'+stime
  fext = '.dat'
  if (keyword_set(int16)) then fext='.i16'
  if verbose gt 1 then print,'get_snap_files: looking for ',fhead+fext
  names = ''
  ; check if we have a mergeable mpi output, threads set or the wrong filename:
  if (fexists(fhead+fext) eq 1) then begin 
    names = fhead+fext
  ; we have mpi output
  endif else if (fexists(fhead+'-0000.dat') eq 1) then begin   
    merge = 1b
  endif
;-----------------------------------------------------------------------
  ; are we going to merge files?
  if keyword_set(merge) then names = file_search(fhead+'-*'+fext)
  ; check for a specific thread
  if n_elements(thread) gt 0 then begin
    filen = fhead+'-'+string(format='(i4.4)',thread)+fext
    if (fexists(filen)) then begin
      names = filen
    endif else begin
      names = ''
    endelse
  endif
  ; return the names
  pos=strpos(names[0],path_sep(),/reverse_search)
  if verbose gt 1 then print,'get_snap_files: found ',strmid(names,pos)
  return, strmid(names,pos)
end
