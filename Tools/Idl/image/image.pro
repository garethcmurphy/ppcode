; $Id: image.pro,v 1.40 2011/11/20 01:11:01 aake Exp $

;FUNCTION imsym,im
;  im1=reform(im)
;  rms1=rms(im)
;  s=size(im)
;  w=where(abs(im1-im1(s[1]/4,s[2]/4) lt 1e-5*rms1,nw)
;END

pro image,im0,pos,posy,zero=zero,erase=erase,max=max,minimum=min,xoffset=xoffset,$
  grid=grid,stat=stat,rdpixf=rdpixf,color=color,zrange=zrange,title=title,$
  profile=profile,gamma=gamma,symmetric=symmetric,cubic=cubic,bilinear=bilinear,$
  tcolor=tcolor,bold=bold,tsize=tsize,wait_time=wait_time,cm=cm,minmax=minmax,$
  xsize=xsize,ysize=ysize,dryrun=dryrun,xmesh=xmesh,ymesh=ymesh,wshow=wshow,$
  zlog=zlog,mark=mark,pic=pic,periodic=periodic,signexp=signexp,margin=margin,$
  rotate=rotatep,format=format,destretched=destretched,contour=contour,$
  byteimage=im1,$
  _extra=extra

  COMMON cpssz, psx, psy
  COMMON cimsz, imx,imy

  on_error, 2

  if n_params() eq 0 then begin
    print,'image,im0,pos,posy,zero=zero,erase=erase,max=max,min=min,xoffset=xoffset,$'
    print,' grid=grid,stat=stat,rdpixf=rdpixf,color=color,zrange=zrange,title=title,$'
    print,' profile=profile,gamma=gamma,symmetric=symmetric,cubic=cubic,bilinear=bilinear,$'
    print,' tcolor=tcolor,bold=bold,tsize=tsize,wait_time=wait_time,cm=cm,minmax=minmax,$
    print,' xsize=xsize,ysize=ysize,dryrun=dryrun,xmesh=xmesh,ymesh=ymesh'
    print,' $Id: image.pro,v 1.40 2011/11/20 01:11:01 aake Exp $'
    return
  end

  if n_elements(imx) eq 0 then imx=256
  if n_elements(imy) eq 0 then imy=256
  if n_elements(pos) eq 0 then pos=-1
  if n_elements(erase) ne 0 then erase
  if n_elements(xoffset) eq 0 then xoffset=0
  if n_elements(gamma) eq 0 then gamma=1.0
  if n_elements(margin) eq 0 then margin=0.05
  if n_elements(symmetric) ne 0 then begin
    max=max(im0,min=min)
    max=max > (-min) & min=-max
  end
  if n_elements(zrange) ge 2 then begin
    min=zrange(0) & max=zrange(1)
  end else begin
    if n_elements(min) eq 0 then min=0
    if n_elements(max) eq 0 then max=0
  end
  if n_elements(tcolor) eq 0 then tcolor=!d.TABLE_SIZE-1
  if n_elements(wshow) ne 0 then ws
  if n_elements(format) eq 0 then format='(2f8.2)'

  im=reform(im0)
  if n_elements(rotatep) eq 1 then im=rotate(im,rotatep)
  if n_elements(signexp) gt 0 then im=sign(im)*abs(im)^signexp
  s=size(im)
  if keyword_set(cm) then im=im(4:s[1]-5,*)
  s=size(im)
  if keyword_set(zlog) then im=alog10(im)
  if keyword_set(pic) then im=im[2:s[1]-2,2:s[2]-2]
  s=size(im)
  if keyword_set(periodic) then begin
    im = [[im],[im]]
    im = transpose(im)
    im = [[im],[im]]
    im = transpose(im)
  end
  s=size(im)

  if n_elements(contour) gt 0 then begin
    if keyword_set(stat) gt 0 then stat,im

    imax=max(im) & imin=min(im)
    if n_elements(max) eq 0 or max eq min then imax=max(im) else imax=max
    if n_elements(min) eq 0 or max eq min then imin=min(im) else imin=min
    if keyword_set(zero) then begin
      iabs=max([abs(imin),abs(imax)])
      imin=-iabs
      imax=iabs
    end
    levels=imin+(imax-imin)*findgen(64)/63.

    aspect=float(!d.y_size)/float(!d.x_size)
    xticklen=0.03/(aspect > 1)
    yticklen=0.03/(1./aspect > 1)
    if n_elements(xmesh) gt 0 then begin
      if n_elements(ymesh) gt 0 then begin
        contour,/fill,levels=levels,xst=1,yst=1,reform(im),xmesh,ymesh,_extra=extra $
	  ,xticklen=xticklen,yticklen=yticklen,title=title
      end else begin
        contour,/fill,levels=levels,xst=1,yst=1,reform(im),xmesh,_extra=extra $
	  ,xticklen=xticklen,yticklen=yticklen,title=title
      end
    end else begin
      contour,/fill,levels=levels,xst=1,yst=1,reform(im),_extra=extra $
        ,xticklen=xticklen,yticklen=yticklen,title=title
    end
    return
  end

;  Interpolate from uneven to even mesh

  if keyword_set(xmesh) then begin
    xeven=xmesh(0)+(xmesh(s[1]-1)-xmesh(0))*findgen(s[1])/(s[1]-1.)
    im=xinterp(xmesh,xeven,im)
    destretched=im
  endif
  if keyword_set(ymesh) then begin
    yeven=ymesh(0)+(ymesh(s[2]-1)-ymesh(0))*findgen(s[2])/(s[2]-1.)
    im=yinterp(ymesh,yeven,im)
    destretched=im
  endif

  if !d.name eq "PS" then begin
    if n_elements(psx) eq 0 then psx=6.
    if n_elements(psy) eq 0 then psy=6.
    xs=psx & ys=psy
  end else if keyword_set(cubic) then begin
    xs=imx
    ys=imy
  end else begin
    xf=imx/float(s[1])
    yf=imy/float(s[2])
    if xf ge 1.0 then xs=long(xf+0.5)*s[1] else begin
      ix=2
      while (s[1] mod (s[1]/ix)) ne 0 or s[1]/(ix+.2) gt imx do ix=ix+1
      xs=s[1]/ix
    end
    if yf ge 1.0 then ys=long(yf+0.5)*s[2] else begin
      iy=2
      while (s[2] mod (s[2]/iy)) ne 0 or s[2]/(iy+.2) gt imy do iy=iy+1
      ys=s[2]/iy
    end
    imx=xs
    imy=ys
  end
  xsize=imx
  ysize=imy
  if keyword_set(dryrun) then return

  if keyword_set(stat) gt 0 then stat,im

  if !d.name eq "TEK" then begin
    ;if keyword_set(cubic) then amscl,congrid(im,xs,ys),min=min,max=max else if keyword_set(bilinear) then amscl,congrid(im,xs,ys,/inter),min=min,max=max else amscl,rebin(im,xs,ys),min=min,max=max
;------------------------------------------------------------------------
  end else if !d.name eq "PS" then begin
    w=!d.x_size/!d.x_px_cm
    h=!d.y_size/!d.y_px_cm
    x=(pos > 0)*(xs+margin)
    y=h-(ys+margin)
    ;stop
    if x+xs gt w then begin
      nrow=fix(w/xs)
      ny=fix(x/(xs*nrow))
      y=h-(ys+margin)*(ny+1)
      x=((pos > 0)-ny*nrow)*(xs+margin)
    end
    print,'x,y:', x,y

    top=!d.TABLE_SIZE-1

    imax=max(im) & imin=min(im)
    if n_elements(max) eq 0 or max eq min then imax=max(im) else imax=max
    if n_elements(min) eq 0 or max eq min then imin=min(im) else imin=min
    if keyword_set(zero) then begin
      iabs=max([abs(imin),abs(imax)])
      imin=-iabs
      imax=iabs
    end

    if gamma ne 1.0 then $
      im1=bytscl((im-imin)^gamma,max=(imax-imin)^gamma,min=0,top=top-3) $
    else $
      im1=bytscl(im,max=imax,min=imin,top=top-3)

    if s[1] lt 512 then begin
      m=512/s[1]
      im1=rebin(im1,s[1]*m,s[2]*m)
      s[1]=s[1]*m
      s[2]=s[2]*m
    end

    if n_elements(grid) gt 0 then begin
      lg=indgen(s[1]/grid)*fix(grid*xs/s[1])
      mg=indgen(s[2]/grid)*fix(grid*ys/s[2])
      if keyword_set(xmesh) then lg=(s[1]-1)*(xmesh-min(xmesh))/(max(xmesh)-min(xmesh))
      if keyword_set(ymesh) then mg=(s[2]-1)*(ymesh-min(ymesh))/(max(ymesh)-min(ymesh))
      top=255
      im1(lg,*)=top
      im1(*,mg)=top
    end

    ;print,x,y
    device,bits=8
    eps=0.03
    if (pos ge 0) then $
      tv,im1,/centimeter,x+xoffset,y,xsize=xs-eps,ysize=ys-eps $
    else $
      tv,im1,/centimeter,xsize=xs-eps,ysize=ys-eps

    if n_elements(title) ne 0 then begin
      if pos ge 0 then begin
        nx=fix(!d.x_size*1e-3/xs)
        ix=pos mod nx
        iy=pos/nx
        xt=(ix+0.5)*(xs+margin)*1e3
        yt=!d.y_size-!d.y_ch_size*1.2-iy*(ys+margin)*1e3
      end else begin
        xt=0.5*xs*1e3
        yt=ys*1e3-!d.y_ch_size*1.2
      end
      xyouts,xt,yt,/dev,title,align=0.5,charsize=1,color=tcolor
    end
;------------------------------------------------------------------------
  end else begin                ; image devices: 'X', 'MAC', and 'WIN'
    ;tvlct,/get,rr,gg,bb
    ;top=n_elements(rr)-2
    ;if top le 1 then top=255
    top=!d.TABLE_SIZE-1
    imax=max(im) & imin=min(im)
    if n_elements(max) eq 0 or max eq min then imax=max(im) else imax=max
    if n_elements(min) eq 0 or max eq min then imin=min(im) else imin=min
    if keyword_set(zero) then begin
      iabs=max([abs(imin),abs(imax)])
      imin=-iabs
      imax=iabs
    end

    ;im1=rebin(1+bytscl(im,max=imax,min=imin,top=top),xs,ys)
    ;im1=rebin(bytscl(im,max=imax,min=imin,top=top),xs,ys)
    if keyword_set(cubic) then $
      im1=bytscl(congrid(im,xs,ys,/minus),max=imax,min=imin,top=top-5)+2 $
    else if keyword_set(bilinear) then $
    ;if keyword_set(bilinear) then $
      im1=bytscl(congrid(im,xs,ys,/inter),max=imax,min=imin,top=top-5)+2 $
    else begin
      imsz=size(im)
      if imsz[3] eq 1 then im1=im else $
        im1=rebin(bytscl(im,max=imax,min=imin,top=top-3),xs,ys)+2
    end

    if n_elements(grid) gt 0 then begin
      lg=indgen(s[1]/grid)*fix(grid*xs/s[1])
      mg=indgen(s[2]/grid)*fix(grid*ys/s[2])
      if keyword_set(xmesh) then lg=(s[1]-1)*fix(xs/s[1])*(xmesh-min(xmesh))/(max(xmesh)-min(xmesh))
      if keyword_set(ymesh) then mg=(s[2]-1)*fix(ys/s[2])*(ymesh-min(ymesh))/(max(ymesh)-min(ymesh))
      top=255
      im1(lg,*)=top
      im1(*,mg)=top
    end

    if N_elements(mark) gt 0 then begin
      maxim=255
      minim=0
      s0=size(im) & s1=size(im1)
      mksz=size(mark)

      m0=s1[1]-1 & m2=(m0/50)>1
      if mksz[0] gt 2 then m1=mark[2]/2
      m1=2*m2

      mark[0]=mark[0]*s1[1]/s0[1]
      mark[1]=mark[1]*s1[2]/s0[2]
      im1[(mark[0]-m1)>0 :(mark[0]-m2)>0 ,mark[1]]=maxim
      im1[(mark[0]+m2)<m0:(mark[0]+m1)<m0,mark[1]]=maxim

      m0=s1[2]-1 & m1=m0/15 & m2=m0/30
      if mksz[0] gt 2 then begin m1=mark[2] & m2=mark[2]/2 & end

      m0=s1[2]-1 & m2=(m0/50)>1
      if mksz[0] gt 2 then m2=mark[2]/2
      m1=2*m2

      m0=s1[1]-1
      m3=s1[2]-1
      im1[mark[0],(mark[1]-m1)>0 :(mark[1]-m2)>0 ]=maxim
      im1[mark[0],(mark[1]+m2)<m3:(mark[1]+m1)<m3]=maxim
      im1[(mark[0]-1)>0:(mark[0]+1)<m0,(mark[1]-1)>0:(mark[1]+1)<m3]=minim
    end

    if n_elements(tsize) eq 0 then tsize=1.
    ytxtmarg=25
    if n_elements(posy) gt 0 then begin
      tv,im1,(pos > 0),posy
      xt=pos+0.5*xs
      yt=posy+ys-ytxtmarg*tsize
      yt=(yt>posy)
    end else begin
      if pos ge 0 then begin
        tv,im1,pos
        nx=fix(!d.x_size/xs)
        ix=pos mod nx
        iy=pos/nx
        xt=(ix+0.5)*xs
        yt=!d.y_size-ytxtmarg*tsize-iy*ys
        ;print,ix,iy,xt,yt
        rdx=ix*xs
        rdy=!d.y_size-(iy+1)*ys
      end else begin
        tv,im1
        xt=0.5*xs
        yt=ys-ytxtmarg*tsize
        rdx=0
        rdy=0
      end
    end
    if n_elements(color) ne 0 then color,im0,/bar
    sz = size(im)
    if n_elements(profile) ne 0 then aaprofiles,rebin(im,xs,ys),size=sz[1:2]
    if n_elements(title) ne 0 then begin
      xyouts,xt,yt,/dev,title,color=tcolor,align=0.5,charsize=1.5*tsize,_extra=extra
      if keyword_set(bold) then begin
        xyouts,xt+1,yt,/dev,title,color=tcolor,align=0.5,charsize=1.5*tsize
        xyouts,xt,yt+1,/dev,title,color=tcolor,align=0.5,charsize=1.5*tsize
        xyouts,xt+1,yt+1,/dev,title,color=tcolor,align=0.5,charsize=1.5*tsize
      endif
    end
    if keyword_set(minmax) then begin
      ;sym=imsym(im)
      xyouts,xt,yt-ys+25*tsize,/dev,str([min(im,max=max),max],format=format) $
            ,color=tcolor,align=0.5,chars=1.2*tsize
    end
    if n_elements(rdpixf) ne 0 then begin
      ws                ; make sure window is shown
      aardpix,rebin(im,xs,ys),rdx,rdy,rebin=[xs/s[1],ys/s[2]]
      ws,/icon & ws     ; force window on top
    endif
    if keyword_set(wait_time) then begin
       if wait_time ge 0 then wait,wait_time
     end else empty
  end
end
